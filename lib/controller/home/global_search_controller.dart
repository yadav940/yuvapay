import 'package:flutter/cupertino.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';

class GlobalSearchController extends GetxController {
  TextEditingController globalSearchController = TextEditingController();
  RxBool isPay = false.obs;
  RxBool isUnknownSearch = false.obs;

  List<String> recharge = [
    TranslationKeys.walletTopUp.tr,
    TranslationKeys.mobileRecharge.tr,
    TranslationKeys.electricityBill.tr,
    TranslationKeys.dthRecharge.tr,
    TranslationKeys.fastTag.tr,
    TranslationKeys.rent.tr,
    TranslationKeys.gasCylinder.tr,
    TranslationKeys.more.tr
  ];
  List<String> searchResults = [
    TranslationKeys.makePayment.tr,
    TranslationKeys.sendMoney.tr,
    TranslationKeys.rechargesAndBillPayments.tr
  ];

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void emptySearch() {
    globalSearchController.text = '';
    isPay(false);
    isUnknownSearch(false);
  }

  void checkSearchString() {
    List<String> dummySearchArray = ['p', 'pa', 'pay'];
    if (globalSearchController.text.toLowerCase() == ''.toLowerCase()) {
      isPay(false);
      isUnknownSearch(false);
    } else if (dummySearchArray
        .contains(globalSearchController.text.toLowerCase())) {
      isPay(true);
      isUnknownSearch(false);
    } else {
      isPay(false);
      isUnknownSearch(true);
    }
  }
}
