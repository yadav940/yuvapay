import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/utils/network_status.dart';

class NotificationController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }
}
