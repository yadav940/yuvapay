import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/onboarding/upi/transfer_to_bank/bank_search_page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/transfer_to_upi/upi_search_page.dart';
import 'package:yuvapay/ui/views/onboarding/wallete/wallete_activate_page.dart';
import 'package:yuvapay/utils/network_status.dart';

import '../../ui/views/onboarding/upi/transfer_to_self/transfer_to_self.dart';
import '../../ui/views/prepaid/mobile_recharge_prepaid_page.dart';
import '../../ui/views/wallet/wallete_prepaid_card_page.dart';

class HomeController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  late double screenHeight, screenWidth;
  String sendMoneyIconBase = AssetConstants.sendMoneyIconBase;
  String rechargeIconBase = AssetConstants.rechargeIconBase;
  String financialIconBase = AssetConstants.financialIconBase;
  String investmentsIconBase = AssetConstants.investmentsIconBase;
  String rewardsIconBase = AssetConstants.rewardsIconBase;

  List<String> sendVia = [
    TranslationKeys.bhimUpi.tr,
    TranslationKeys.bankTransfer.tr,
    TranslationKeys.toMobileNumber.tr,
    TranslationKeys.selfTransfer.tr
  ];
  List<String> financial = [
    TranslationKeys.buyInsurance.tr,
    TranslationKeys.checkScore.tr,
    TranslationKeys.applyCard.tr
  ];
  List<String> investments = [
    TranslationKeys.goldLocker.tr,
    TranslationKeys.goldAndSilver.tr
  ];
  List<String> rewards = [
    TranslationKeys.myRewards.tr,
    TranslationKeys.spinToWIn.tr,
    TranslationKeys.referAndEarn.tr,
    TranslationKeys.spinToWIn.tr
  ];
  List<String> recharge = [
    TranslationKeys.walletTopUp.tr,
    TranslationKeys.mobileRecharge.tr,
    TranslationKeys.electricityBill.tr,
    TranslationKeys.dthRecharge.tr,
    TranslationKeys.fastTag.tr,
    TranslationKeys.rent.tr,
    TranslationKeys.gasCylinder.tr,
    TranslationKeys.more.tr
  ];

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }



  void selectTab(String text) {
    debugPrint('-----------$text');
    switch (text) {
      case 'BHIM UPI':
        UpiTransferSearchPage.start();
        //UpiActivatePage.start();
        break;
      case 'Bank Transfer':
        BankTransferSearchPage.start();
        break;
      case 'Wallet Topup':
        WalleteActivatePage.start();
        break;
      case 'Self Transfer':
        TransferToSelfPage.start();
        break;
      case 'To Mobile Number':
        MobileRechargePrepaidPage.start(page: TranslationKeys.sendViaPhoneNumber.tr);
        //WalletePrepaidCardPage.start();
        //Get.to(MobileRechargePrepaidPage.)
        break;
      case 'Wallet Topup?':
        WalletePrepaidCardPage.start();
        break;
      case 'Mobile Recharge':
        MobileRechargePrepaidPage.start();
        ///Get.to(MobileRechargePrepaidPage.);

        break;
    }
  }
}
