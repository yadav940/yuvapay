import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/utils/network_status.dart';

import '../../ui/views/home/home_page.dart';

class LoadingController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;

  @override
  void onInit() {
    waitAndNavigate();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void waitAndNavigate() {
    Future.delayed(const Duration(milliseconds: 4000), () {
      Get.toNamed(HomePage.routeName);
      // Get.toNamed(LanguagePage.routeName);
    });
  }
}
