import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base.dart';

class WalletPrepaidCardController extends GetxController {

  RxBool cardVisible=false.obs;

  void setCardVisible(){
    cardVisible.value=!cardVisible.value;
  }

  RxBool cardFreeze=false.obs;

  void setCardFreeze(){
    cardFreeze.value=!cardFreeze.value;
  }

}
