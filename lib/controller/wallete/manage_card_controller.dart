
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/views/base/base.dart';

class ManageCardController extends GetxController {

  RxDouble _transactionLimit=0.4.obs;

  double get transactionLimit => _transactionLimit.value;

  void transactionLimitSet(double value) {
    _transactionLimit.value = value;
  }
  RxBool enableOnlineTransaction=false.obs;

  void setEnableOnlineTransaction(){
    enableOnlineTransaction.value=!enableOnlineTransaction.value;
  }
}