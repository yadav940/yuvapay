import 'package:get/get.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/onboarding/phone_number_and_email_otp_page.dart';
import 'package:yuvapay/utils/enums.dart';
import 'package:yuvapay/utils/network_status.dart';

class PhoneNumberAndEmailController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  TextEditingController textController = TextEditingController();
  RxInt selectedItemIndex = 0.obs;
  //PhoneNumberEmail page = Get.arguments;
  RxString errorMessage = ''.obs;
  RxBool isError = false.obs;

  @override
  void onInit() {
    super.onInit();
    //debugPrint('-------------------$page');
  }

  @override
  void onReady() {
    super.onReady();
  }

  bool validateMobile() {
    String value = textController.text;


    if(Get.arguments==PhoneNumberEmail.phoneNumber){

      String patternNumber = r'(^[0-9])';
      RegExp regExp = RegExp(patternNumber);
      if (value.isEmpty || !regExp.hasMatch(value) || value.length != 10) {
        errorMessage.value = 'Kindly check if the number entered is valid';
        isError.value = true;
        return false;
      }

    }else{
      String patternNumber = Get==PhoneNumberEmail.phoneNumber?r'(^(?:[+0]9)?[0-9]{10,12}$)':r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$';
      RegExp regExp = RegExp(patternNumber);
      if (value.isEmpty || !regExp.hasMatch(value) ) {
        errorMessage.value = 'Kindly enter valid email ID';
        isError.value = true;
        return false;
      }

    }

    errorMessage.value='';
    isError.value = false;
    return true;
  }

  void onChangeText(String text) {
    debugPrint('----');
  }

  void next() {
    if (validateMobile()) {
      errorMessage.value='';
      isError.value = false;
      textController.text='';
      PhoneNumberAndEmailOTPPage.start(Get.arguments);
    }
  }
  @override
  void dispose() {
    //textController.dispose();
  }
}
