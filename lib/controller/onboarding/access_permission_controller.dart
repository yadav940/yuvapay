import 'package:permission_handler/permission_handler.dart';
import 'package:yuvapay/model/onboarding/permission_model.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/onboarding/phone_number_and_email_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/popup_utils.dart';
import 'package:yuvapay/utils/enums.dart';
import 'package:yuvapay/utils/network_status.dart';

class AccessPermissionController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  List<PermissionModel> items = [
    PermissionModel(
        asset: 'assets/images/svg/ic_contact.svg',
        title: 'Contacts',
        bodyMessage: 'To help you connect with your Yuvapay connections'),
    PermissionModel(
        asset: 'assets/images/svg/ic_location.svg',
        title: 'Location',
        bodyMessage: 'To prevent location based frauds'),
    PermissionModel(
        asset: 'assets/images/svg/ic_sms.svg',
        title: 'SMS',
        bodyMessage: 'To serve you better by autofilling OTP'),
    PermissionModel(
        asset: 'assets/images/svg/ic_camera.svg',
        title: 'Camera',
        bodyMessage: 'To scan QRs'),
    PermissionModel(
        asset: 'assets/images/svg/ic_install_apps.svg',
        title: 'Installed Apps',
        bodyMessage:
            'To help you integrate seamlessly with other required apps'),
  ];
  RxInt selectedItemIndex = 0.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future<void> getPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.contacts,
      Permission.location,
      Permission.sms,
      Permission.camera,
      //Permission.requestInstallPackages,
    ].request();

    var statusCamera = await Permission.camera.status;
    var statusContacts = await Permission.contacts.status;
    var statusLocation = await Permission.location.status;
    var statusSms = await Permission.sms.status;
    //var statusRequestInstallPackages = await Permission.requestInstallPackages.status;

    if ((statusCamera.isDenied ||
            statusCamera.isRestricted ||
            statusCamera.isPermanentlyDenied) ||
        (statusContacts.isDenied ||
            statusContacts.isRestricted ||
            statusContacts.isPermanentlyDenied) ||
        (statusLocation.isDenied ||
            statusLocation.isRestricted ||
            statusLocation.isPermanentlyDenied) ||
        (statusSms.isDenied ||
            statusSms.isRestricted ||
            statusSms.isPermanentlyDenied) /*||
        (statusRequestInstallPackages.isDenied ||
            statusRequestInstallPackages.isRestricted ||
            statusRequestInstallPackages.isPermanentlyDenied)*/) {
      debugPrint('------------showAppBottomSheet');
      showAppBottomSheet(
          child: permissionPopup,
          dragPickColor: Colors.transparent,
          initialChildSize: 0.35);
    }else{
      PhoneNumberAndEmailPage.start(PhoneNumberEmail.phoneNumber);
    }

    // You can can also directly ask the permission about its status.
    /*if (await Permission.location.isRestricted) {
      debugPrint('=========isRestricted');
      // The OS restricts access, for example because of parental controls.
    }*/
  }

  Widget get permissionPopup {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        children: [
          Text(
            'Uh-oh! We will need all the  permisssions to serve you',
            style: TextStyles.pageTitle(),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 40,
          ),
          AppButton(
            text: 'Back to permissions',
            onPressed: () async {
              await  openAppSettings();
            }
      ,
          ),
          const SizedBox(
            height: 16,
          ),
          InkWell(
            onTap: () {
              PhoneNumberAndEmailPage.start(PhoneNumberEmail.phoneNumber);
              // Get.toNamed(WelcomePage.routeName);
            },
            child: Text(
              'Deny',
              style: TextStyles.sp16(color: Palette.greyScaleDark6),
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
