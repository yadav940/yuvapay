import 'dart:async';

import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/onboarding/onboarding_home.dart';
import 'package:yuvapay/ui/views/onboarding/phone_number_and_email_page.dart';
import 'package:yuvapay/utils/enums.dart';
import 'package:yuvapay/utils/network_status.dart';

class PhoneNumberAndEmailOTPController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  TextEditingController textController = TextEditingController();
  RxInt selectedItemIndex = 0.obs;
  RxString errorMessage = ''.obs;
  RxBool isError = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }
  bool validateOtp() {
    String value = textController.text;

    String patternNumber = r'(^[0-9])';
    RegExp regExp = RegExp(patternNumber);
    if (value.isEmpty || !regExp.hasMatch(value) || value.length != 6) {
      errorMessage.value = TranslationKeys.kindly_recheck_the_otp_entered.tr;
      isError.value = true;
      return false;
    }
    errorMessage.value='';

    isError.value = false;
    return true;
  }

  void onChangeText(String text) {
    debugPrint('----');
  }

  late Timer _timer;
  RxInt initTimer = 30.obs;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) {
        if (initTimer.value == 0) {
          timer.cancel();
        } else {
          initTimer.value--;
        }
      },
    );
  }

  void next() {
    if (validateOtp()) {
      if(Get.arguments==PhoneNumberEmail.phoneNumber){
        debugPrint('--email--');
        textController.text='';
        if(_timer!=null){
          _timer.cancel();
        }
        PhoneNumberAndEmailPage.start(PhoneNumberEmail.email);
      }else{
        OnboardingHome.start();
      }
    }
  }
}
