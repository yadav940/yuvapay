
import 'package:carousel_slider/carousel_options.dart';
import 'package:get/get.dart';
import 'package:yuvapay/model/onboarding/welcome_model.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/utils/network_status.dart';

class WelcomeController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  List<WelcomeModel> items = [
    WelcomeModel(
        asset: 'assets/images/png/image_welcome1.png',
        assetBackgroundColor: Colors.amber,
        title: 'Welcome to YuvaPay',
        bodyMessage: ''),
    WelcomeModel(
        asset: 'assets/images/png/image_welcome2.png',
        assetBackgroundColor: Palette.buttonBackground,
        title: 'Banking for everyone',
        bodyMessage: 'Join the digital revolution of payments & transactions'),
    WelcomeModel(
        asset: 'assets/images/png/image_welcome3.png',
        assetBackgroundColor: Palette.greenBackground,
        title: 'Easy Digital Finance',
        bodyMessage: 'Join the digital revolution of payments & transactions'),
  ];
  RxInt selectedItemIndex = 0.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void changeSlider(int index, CarouselPageChangedReason reason) {
    selectedItemIndex.value = index;
  }
}
