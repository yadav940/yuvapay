import 'package:get/get.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/onboarding/upi/successfully_activated_page.dart';
import 'package:yuvapay/utils/enums.dart';
import 'package:yuvapay/utils/network_status.dart';

class SelectSimController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  Rx<SelectSim> selectSim = SelectSim.sim1.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void onChangedSim(SelectSim? selectSim) {
    this.selectSim.value = selectSim!;
  }

  void onNext() {
    networkStatus.value = NetworkStatus.LOADING;
    Future.delayed(
      Duration(
        seconds: 3,
      ),
      () {
        UpiSuccessfullyActivatedPage.start();
        networkStatus.value = NetworkStatus.SUCCESS;
      },
    );
  }
}
