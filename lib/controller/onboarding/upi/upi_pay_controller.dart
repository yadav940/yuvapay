import 'package:get/get.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/home/home_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/popup_utils.dart';
import 'package:yuvapay/utils/enums.dart';
import 'package:yuvapay/utils/network_status.dart';

import '../../../ui/resources/translations/translations_key.dart';
import '../../../ui/views/onboarding/upi/upi_transfer_success_page.dart';

class UpiPayController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  Rx<SelectSim> selectSim = SelectSim.sim1.obs;

  RxBool isError = false.obs;
  RxBool isSearching = false.obs;
  RxInt errorState = 1.obs;
  RxString buttonText = TranslationKeys.proceedToPay.tr.obs;
  RxString sendToText = 'Send to Cheteshwari C Rao'.obs;
  RxString sendToUpiText = 'chetishwari@ybl'.obs;

  TextEditingController textController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments == FromUpiPayPage.topUp) {
      buttonText.value = 'Top-Up';
      sendToText.value = 'Yuva Pay Wallet Top-up';
      sendToUpiText.value = 'Current Balance: ₹ 100 ';
    } else if (Get.arguments == FromUpiPayPage.requestToWallet) {
      buttonText.value = 'Request  to Wallet';
      sendToText.value = 'Yuva Pay Wallet Top-up';
      sendToUpiText.value = '9988181818 ';
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  void onSearch(String text) {
    if (text.length == 0) {
      isSearching.value = false;
    } else {
      isSearching.value = true;
    }
  }

  void resetSearch() {
    textController.text = '';
    isSearching.value = false;
  }

  void onNext() {
    networkStatus.value = NetworkStatus.LOADING;
    Future.delayed(
      Duration(
        seconds: 3,
      ),
      () {
        //UpiSuccessfullyActivatedPage.start();
        networkStatus.value = NetworkStatus.SUCCESS;
        if (Get.arguments == FromUpiPayPage.requestToWallet) {
          showAppBottomSheet(child: requestSendPop,initialChildSize: 0.3,dragPickColor: Colors.transparent,);
        } else {
          if (isError.value) {
            switch (errorState.value) {
              case 1:
                errorState.value = 2;
                break;
              case 2:
                errorState.value = 3;
                break;
              case 3:
                UpiTransferSuccessPage.start();
                break;
            }
          } else {
            isError.value = true;
          }
        }
      },
    );
  }

  Widget get requestSendPop {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        children: [
          Text('Request sent',style: TextStyles.sp18(fontWeight: FontWeight.w600),),
          const SizedBox(height: 8,),
          Text('₹70 requested to Prajwala Naik',style: TextStyles.sp18(fontWeight: FontWeight.w600),),
          const SizedBox(height: 16,),
          AppButton(onPressed: (){HomePage.start();},text: TranslationKeys.okay.tr,)
        ],
      ),
    );
  }
}
