import 'package:get/get.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/utils/enums.dart';
import 'package:yuvapay/utils/network_status.dart';

class AddNewBeneficiaryController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  Rx<SelectSim> selectSim = SelectSim.sim1.obs;

  RxBool isError = false.obs;
  RxBool isConfirmAccountError = false.obs;
  RxBool isIfscError = false.obs;

  TextEditingController accountNumberTextController = TextEditingController();
  TextEditingController confirmNumberTextController = TextEditingController();
  TextEditingController ifscTextController = TextEditingController();
  TextEditingController nickNameTextController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void onSearch(String text) {
    /*if(text.contains('@')){
      isError.value=false;
    }else{
      isError.value=true;
    }*/
    if (accountNumberTextController.text.endsWith(
        confirmNumberTextController.text)) {
      isError.value = false;
    } else {
      isError.value = true;
    }
  }


  void onNext() {
    /*networkStatus.value = NetworkStatus.LOADING;
    Future.delayed(
      Duration(
        seconds: 3,
      ),
          () {
        UpiSuccessfullyActivatedPage.start();
        networkStatus.value = NetworkStatus.SUCCESS;
      },
    );
  */
    if (accountNumberTextController.text.length < 9) {
      isError(true);
      isConfirmAccountError(false);
      isIfscError(false);
    }
    else if(accountNumberTextController.text!=confirmNumberTextController.text){
      isError(false);
      isConfirmAccountError(true);
      isIfscError(false);
    }
    else if(ifscTextController.text.isEmpty){
      isError(false);
      isConfirmAccountError(false);
      isIfscError(true);
    }
    else{
      isError(false);
      isConfirmAccountError(false);
      isIfscError(false);
      Get.back();
    }
    }

    }
