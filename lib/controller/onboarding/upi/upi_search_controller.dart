import 'package:get/get.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/utils/enums.dart';
import 'package:yuvapay/utils/network_status.dart';

import '../../../ui/views/onboarding/upi/upi_transfer_success_page.dart';

class UpiSearchController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  Rx<SelectSim> selectSim = SelectSim.sim1.obs;

  RxBool isError = false.obs;
  RxBool isSearching = false.obs;
  RxInt errorState= 1.obs;

  TextEditingController textController= TextEditingController();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void onSearch(String text) {
    if(text.length==0){
      isSearching.value=false;
    }else{
      isSearching.value=true;
    }

  }
  void resetSearch(){
    textController.text='';
    isSearching.value=false;
  }


  void onNext() {
    networkStatus.value = NetworkStatus.LOADING;
    Future.delayed(
      Duration(
        seconds: 3,
      ),
          () {
        //UpiSuccessfullyActivatedPage.start();
        networkStatus.value = NetworkStatus.SUCCESS;
        if(isError.value){
          switch(errorState.value){
            case 1:
              errorState.value=2;
              break;
            case 2:
              errorState.value=3;
              break;
            case 3:
              UpiTransferSuccessPage.start();
              break;
          }
        }else{
          isError.value=true;
        }
      },
    );
  }
}
