import 'package:get/get.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/utils/enums.dart';
import 'package:yuvapay/utils/network_status.dart';

class AddNewUpiController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  Rx<SelectSim> selectSim = SelectSim.sim1.obs;

  RxBool isError = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void onSearch(String text) {
    if(text.contains('@')){
      isError.value=false;
    }else{
      isError.value=true;
    }

  }


  void onNext() {
    /*networkStatus.value = NetworkStatus.LOADING;
    Future.delayed(
      Duration(
        seconds: 3,
      ),
          () {
        UpiSuccessfullyActivatedPage.start();
        networkStatus.value = NetworkStatus.SUCCESS;
      },
    );
  */
  }
}
