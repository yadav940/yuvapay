
import 'package:get/get.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/utils/network_status.dart';

class LanguageController extends GetxController {
  late  Rx<NetworkStatus> networkStatus=NetworkStatus.SUCCESS.obs;

  List<String> languageList= ['English','Hindi', 'Tamil','Telugu','Karnada','Marathi','Punjabi','Malayalam','Gujrati','Bengali','Odiya','Asamiya'];
  List<Locale> languageLocaleList= [Locale('en','US'),Locale('hi','IN'),Locale('pa','ar-ae'),Locale('pa','ar-ae'),Locale('pa','ar-ae'),Locale('pa','ar-ae'),Locale('pa','ar-ae'),Locale('pa','ar-ae'),Locale('pa','ar-ae'),Locale('pa','ar-ae'),Locale('pa','ar-ae'),Locale('pa','ar-ae')];
  Rx<String> selectedLanguage='English'.obs;
  @override
  void onInit() {
    super.onInit();

  }



  @override
  void onReady() {
    super.onReady();
  }
  void selectLanguage(String selectedLanguageText,int index){
    if(selectedLanguage.value==selectedLanguageText){

    }else{
      selectedLanguage.value= selectedLanguageText;
      Get.updateLocale(languageLocaleList[index]);
    }
    debugPrint(selectedLanguage.value);
  }
}
