//
import 'package:get/get.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/onboarding/wallete/wallete_otp_page.dart';
import 'package:yuvapay/utils/network_status.dart';

class WalletePanDetailController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  TextEditingController nameTextController = TextEditingController();
  RxInt selectedItemIndex = 0.obs;
  RxString errorMessage = ''.obs;
  RxBool isError = false.obs;

  @override
  void onInit() {
    super.onInit();
    //debugPrint('-------------------$page');
  }

  @override
  void onReady() {
    super.onReady();
  }

  bool validateMobile() {
    String value = nameTextController.text;

    return true;
  }

  void onChangeText(String text) {
    debugPrint('----');
  }

  void next() => WalleteOtpPage.start;
  @override
  void dispose() {
    //textController.dispose();
  }
}
