import 'package:camera/camera.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/utils/network_status.dart';

class KycCameraController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;

  late CameraController controller;

  @override
  Future<void> onInit() async {
    super.onInit();

    var cameras = await availableCameras();
    controller = CameraController(cameras[0], ResolutionPreset.max);
    controller.initialize().then((_) {
      debugPrint('--------------');
      /*if (!mounted) {
        return;
      }
      setState(() {});*/
    });

  }

  @override
  void onReady() {
    super.onReady();
  }

}
