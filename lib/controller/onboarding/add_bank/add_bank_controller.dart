import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/onboarding/upi/addbank/addbank_no_account.dart';
import 'package:yuvapay/utils/network_status.dart';

class AddBankController extends GetxController {
  final Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  final RxBool isSearch = false.obs;
  final searchController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void onChange(String text) {
    if (text.isNotEmpty) {
      isSearch.value = true;
    } else {
      isSearch.value = false;
    }
  }

  void searchCancel() {
    searchController.text = '';
    isSearch.value = false;
  }

  void onTabBank() {
    networkStatus.value = NetworkStatus.LOADING;
    Future.delayed(
      Duration(
        seconds: 3,
      ),
      () {
        networkStatus.value = NetworkStatus.SUCCESS;
        AddbankNoAccount.start();
      },
    );
    //.start();
  }
}
