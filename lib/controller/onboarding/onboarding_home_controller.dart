
import 'package:get/get.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/utils/network_status.dart';

class OnboardingHomeController extends GetxController {
  late  Rx<NetworkStatus> networkStatus=NetworkStatus.SUCCESS.obs;
  RxBool isError=false.obs;
  RxBool applyReferral=false.obs;


  @override
  void onInit() {
    super.onInit();

  }

  @override
  void onReady() {
    super.onReady();
  }


  void  getApplyReferral(){
    if(!applyReferral.value){
      applyReferral.value=true;
      Navigator.pop(Get.context!);
    }
  }
}
