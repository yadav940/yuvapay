import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/utils/network_status.dart';

class MyAccountController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  List<String> myAccountOptions = [TranslationKeys.linkedBankAccounts.tr];
  List<String> myAccountExtraOptions = [
    TranslationKeys.security.tr,
    TranslationKeys.policies.tr,
    TranslationKeys.helpAndSupport.tr,
    TranslationKeys.language.tr,
    TranslationKeys.logOut.tr
  ];

  String dummyName = 'Prahlad Kumar';
  String dummyPhone = '+91 9988383838';
  String dummyEmail = 'contactprahlad@gmail.com';
}
