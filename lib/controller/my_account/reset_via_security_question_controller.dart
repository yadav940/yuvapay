import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/my_account/change_pin_page.dart';

import '../../ui/views/my_account/confirm_pin_page.dart';

class ResetViaSecurityQuestionController extends GetxController {
  final RxBool isError = false.obs;
  RxString errorMessage = ''.obs;

  TextEditingController textController = TextEditingController();

  bool validateMobile() {
    String value = textController.text;
    if (value.isEmpty) {
      errorMessage.value = TranslationKeys.thisfieldcanNotBeLeftblank.tr;
      isError.value = true;
      return false;
    } else {
      if (value == 'Don Bosco') {
        return true;
      } else {
        errorMessage.value =
            TranslationKeys.answer.tr+ TranslationKeys.doesNotMatchLine.tr;
        isError.value = true;
        return false;
      }
    }



  }

  void next() {
    if (validateMobile()) {
      isError.value = false;
      Get.to(() => ConfirmPinPage());
    }
  }
}
