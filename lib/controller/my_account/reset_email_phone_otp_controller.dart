import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/views/my_account/change_pin_page.dart';
import 'package:yuvapay/ui/views/my_account/confirm_pin_page.dart';

import '../../ui/views/base/base.dart';

class ResetEmailPhoneController extends GetxController {
  final RxBool isError = false.obs;
  RxString errorMessage = ''.obs;

  TextEditingController textController = TextEditingController();

  bool validateMobile(bool isPhone) {
    String value = textController.text;
    if(value.length<4){
      errorMessage.value =
      'Please enter proper OTP';
      isError.value = true;
      return false;
    }

   else if (value != '0000') {
      errorMessage.value =
      'OTP entered does not match.  You have 2 attempts left. 3 invalid attempts will lock Yuvapay for 24 hours';
      isError.value = true;
      return false;
    } else {
      errorMessage.value = 'Correct';
      isError.value = false;
      return true;
    }
  }


  void next(bool isPhone) {
    if (validateMobile(isPhone)) {
      isError.value = false;
      Get.to(()=>ConfirmPinPage());
    }
  }

}