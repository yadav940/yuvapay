import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base.dart';

class AccountSecurityController extends GetxController {

RxBool isLockSecurity=false.obs;
List<String> accountSecurityOptions=[
TranslationKeys.screenLockSecurity.tr,
TranslationKeys.changeYuvaPayPin.tr,
TranslationKeys.forgotYuvaPayPin.tr,
];


}