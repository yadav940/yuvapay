import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/utils/network_status.dart';

class LinkedAccountsController extends GetxController {
  late Rx<NetworkStatus> networkStatus = NetworkStatus.SUCCESS.obs;
  List<String> linkedAccountsHeader=['HDFC Bank','Axis Bank','Add a new bank account'];
  List<String> linkedAccountsSubHeader=['XX 1188','XX 1188',''];


}