import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/my_account/reset_email_phone_otp_page.dart';

class ResetViaEmailPhoneController extends GetxController {
  final RxBool isError = false.obs;
  RxString errorMessage = ''.obs;

  TextEditingController textController = TextEditingController();

  bool validateMobile(bool isPhone) {
    String value = textController.text;
    String patternNumber = isPhone
        ? r'(^(?:[+0]9)?[0-9]{10,12}$)'
        : r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$';
    RegExp regExp = RegExp(patternNumber);
    if (value.isEmpty || !regExp.hasMatch(value)) {
      errorMessage.value = isPhone
          ? 'Kindly check if the number entered is valid'
          : 'Kindly enter valid email ID';
      isError.value = true;
      return false;
    } else {
      if (value != (isPhone ? '9988776655' : 'bijayan@gmail.com')) {
        String phoneEmail =
            isPhone ? TranslationKeys.mobileNumber.tr : TranslationKeys.email.tr;
        errorMessage.value = phoneEmail + TranslationKeys.doesNotMatchLine.tr;
        isError.value = true;
        return false;
      } else {
        isError.value = false;
        return true;
      }
    }
  }

  void next(bool isPhone) {
    if (validateMobile(isPhone)) {
      isError.value = false;
      Get.to(() => ResetEmailPhoneOtpPage(), arguments: [isPhone]);
    }
  }
}
