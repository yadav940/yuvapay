import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base.dart';

class PolicyController extends GetxController {
  List<String> policyOptions = [
    TranslationKeys.yuvapayPolicies.tr,
    TranslationKeys.npciUpiGrievanceRedressal.tr,
    TranslationKeys.yesBankGrievanceRedressal.tr
  ];
}
