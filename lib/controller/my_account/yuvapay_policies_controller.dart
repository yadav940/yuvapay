import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base.dart';

class YuvapayPoliciesController extends GetxController {
  List<String> yuvapayPoliciesOptions = [
    TranslationKeys.about.tr,
    TranslationKeys.privacyPolicy.tr,
    TranslationKeys.refundPolicy.tr,
    TranslationKeys.termsAndConditions.tr,
    TranslationKeys.faqGeneral.tr,
    TranslationKeys.mostImportantTAndC.tr,
    TranslationKeys.upiTermsAndConditions.tr,
    TranslationKeys.upiFaq.tr,
    TranslationKeys.prepaidCardFaq.tr,
    TranslationKeys.prepaidcardTAndC.tr,
    TranslationKeys.insuranceFaq.tr,
    TranslationKeys.subscriptionOfferfaq.tr,
    TranslationKeys.upiLimits.tr
  ];
}
