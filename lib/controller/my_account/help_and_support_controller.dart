import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base.dart';

class HelpAndSupportController extends GetxController {
  List<String> helpAndSupportOptions = [
    TranslationKeys.yuvapayGuide.tr,
    TranslationKeys.raiseATicket.tr,
    TranslationKeys.contactCustomerCare.tr
  ];
}
