import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:yuvapay/ui/views/base/base.dart';

class AccountDetailsController extends GetxController {
  RxBool isPinExist = false.obs;
  RxBool isDefault = false.obs;
  RxBool isShowBalance = false.obs;
  TextEditingController cardNumController =TextEditingController();
  TextEditingController expiryDateController =TextEditingController();

  String tableSeparator = ':';
  String dummyName = 'Prahlad Kumar';
  String dummyPhone = '948 214 0204';
  String dummyAccountType = 'Savings';
  String dummyBranch = 'HHE, HO, Bangalore';
  String dummyIfsc = 'CNRB6637737';
  String dummyBalance = '₹ 100.00';
  String dummyUpiId =  'wsdhs@ybl';
  String xxxxHintText =  'XXXXXXXX ----';
  String mmyyHinteText =  'MM / YY';
}
