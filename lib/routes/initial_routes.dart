import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:yuvapay/ui/views/home/global_search_page.dart';
import 'package:yuvapay/ui/views/home/home_page.dart';
import 'package:yuvapay/ui/views/home/loading_page.dart';
import 'package:yuvapay/ui/views/home/notification_page.dart';
import 'package:yuvapay/ui/views/my_account/account_details_page.dart';
import 'package:yuvapay/ui/views/my_account/account_security_page.dart';
import 'package:yuvapay/ui/views/my_account/change_pin_page.dart';
import 'package:yuvapay/ui/views/my_account/linked_accounts.dart';
import 'package:yuvapay/ui/views/my_account/my_account_page.dart';
import 'package:yuvapay/ui/views/onboarding/access_permissions_page.dart';
import 'package:yuvapay/ui/views/onboarding/language_page.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/adharcard_detail_page.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/neo_bank_aadhar_otp_page.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/neo_bank_activation.dart';
import 'package:yuvapay/ui/views/onboarding/onboarding_home.dart';
import 'package:yuvapay/ui/views/onboarding/phone_number_and_email_otp_page.dart';
import 'package:yuvapay/ui/views/onboarding/phone_number_and_email_page.dart';
import 'package:yuvapay/ui/views/onboarding/wallete/wallete_activate_page.dart';
import 'package:yuvapay/ui/views/onboarding/wallete/wallete_otp_page.dart';
import 'package:yuvapay/ui/views/onboarding/wallete/wallete_pan_detail_page.dart';
import 'package:yuvapay/ui/views/onboarding/wallete/wallete_success_page.dart';
import 'package:yuvapay/ui/views/onboarding/welcome_page.dart';

class InitialRoutes {
  InitialRoutes._();

  // Onboarding Module
  static List<GetPage> get routes => [
        GetPage(
          name: LanguagePage.routeName,
          page: () => LanguagePage(),
        ),
        GetPage(
          name: WelcomePage.routeName,
          page: () => WelcomePage(),
        ),
        GetPage(
          name: AccessPermissionPage.routeName,
          page: () => AccessPermissionPage(),
        ),
        GetPage(
          name: PhoneNumberAndEmailPage.routeName,
          page: () => PhoneNumberAndEmailPage(),
        ),
        GetPage(
          name: PhoneNumberAndEmailOTPPage.routeName,
          page: () => PhoneNumberAndEmailOTPPage(),
        ),
        // Home Module
        GetPage(
          name: LoadingPage.routeName,
          page: () => LoadingPage(),
        ),
        GetPage(
          name: HomePage.routeName,
          page: () => HomePage(),
        ),
        GetPage(
          name: GlobalSearchPage.routeName,
          page: () => GlobalSearchPage(),
        ),
        GetPage(
          name: NotificationPage.routeName,
          page: () => NotificationPage(),
        ),
        GetPage(
          name: MyAccountPage.routeName,
          page: () => MyAccountPage(),
        ),
        GetPage(
          name: LinkedAccountsPage.routeName,
          page: () => LinkedAccountsPage(),
        ),
        GetPage(
          name: OnboardingHome.routeName,
          page: () => OnboardingHome(),
        ),
        GetPage(
          name: WalleteActivatePage.routeName,
          page: () => WalleteActivatePage(),
        ),
        GetPage(
          name: WalletePanDetailPage.routeName,
          page: () => WalletePanDetailPage(),
        ),
        GetPage(
          name: WalleteOtpPage.routeName,
          page: () => WalleteOtpPage(),
        ),
        GetPage(
          name: WalleteSuccessfullyActivatedPage.routeName,
          page: () => WalleteSuccessfullyActivatedPage(),
        ),
        GetPage(
          name: NeoBankActivatePage.routeName,
          page: () => NeoBankActivatePage(),
        ),
        GetPage(
          name: NeobankAdharCardDetailPage.routeName,
          page: () => NeobankAdharCardDetailPage(),
        ),
        GetPage(
          name: NeoBankAadharOtpPage.routeName,
          page: () => NeoBankAadharOtpPage(),
        ),
    GetPage(
      name: AccountDetailsPage.routeName,
      page: () => AccountDetailsPage(),
    ),
    GetPage(
      name: AccountSecurityPage.routeName,
      page: () => AccountSecurityPage(),
    ),
    GetPage(
      name: ChangePinPage.routeName,
      page: () => ChangePinPage(),
    ),
      ];

}