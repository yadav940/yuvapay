import 'package:get/get_navigation/src/routes/get_route.dart';
import '../ui/views/wallet/manage_card_page.dart';
import '../ui/views/wallet/request_wallet_page.dart';
import '../ui/views/wallet/wallete_prepaid_card_page.dart';

class WalleteRoutes {
  WalleteRoutes._();

  // upi Module
  static List<GetPage> get routes => [
    GetPage(
      name: WalletePrepaidCardPage.routeName,
      page: () => WalletePrepaidCardPage(),
    ),
    GetPage(
      name: RequestWalletPage.routeName,
      page: () => RequestWalletPage(),
    ),
    GetPage(
      name: ManageCardPage.routeName,
      page: () => ManageCardPage(),
    ),
  ];

}