import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/adharcard_detail_page.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/autosend_sms_page.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/kyc_camera_page.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/neo_bank_aadhar_otp_page.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/neo_bank_activation.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/neo_bank_success_page.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/video_kyc_page.dart';

class NeoBankRoutes {
  NeoBankRoutes._();

  static List<GetPage> get routes => [
    GetPage(
      name: NeoBankActivatePage.routeName,
      page: () => NeoBankActivatePage(),
    ),
    GetPage(
      name: NeobankAdharCardDetailPage.routeName,
      page: () => NeobankAdharCardDetailPage(),
    ),
    GetPage(
      name: NeoBankAadharOtpPage.routeName,
      page: () => NeoBankAadharOtpPage(),
    ),
    GetPage(
      name: VideoKycPage.routeName,
      page: () => VideoKycPage(),
    ),
    GetPage(
      name: KycCameraPage.routeName,
      page: () => KycCameraPage(),
    ),
    GetPage(
      name: AutosendSmsPage.routeName,
      page: () => AutosendSmsPage(),
    ),
    GetPage(
      name: NeoBankSuccessPage.routeName,
      page: () => NeoBankSuccessPage(),
    ),
  ];

}