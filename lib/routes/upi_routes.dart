import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:yuvapay/ui/views/onboarding/upi/addbank/addbank_no_account.dart';
import 'package:yuvapay/ui/views/onboarding/upi/addbank/addbank_other_bank.dart';
import 'package:yuvapay/ui/views/onboarding/upi/addbank/addbank_page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/addbank/addbank_success_page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/select_sim_page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/successfully_activated_page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/transfer_to_upi/add_new_upi.dart';
import 'package:yuvapay/ui/views/onboarding/upi/transfer_to_upi/upi_search_page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/upi_activate.dart';

import '../ui/views/onboarding/upi/transfer_to_bank/add_new_beneficiary.dart';
import '../ui/views/onboarding/upi/transfer_to_bank/bank_search_page.dart';
import '../ui/views/onboarding/upi/transfer_to_self/transfer_to_self.dart';
import '../ui/views/onboarding/upi/transfer_to_upi/upi_pay_page.dart';
import '../ui/views/onboarding/upi/transfer_to_upi/upi_transaction_pin_page.dart';
import '../ui/views/onboarding/upi/upi_transfer_success_page.dart';
import '../ui/views/payment/scan_and_pay_page.dart';
import '../ui/views/prepaid/mobile_recharge_prepaid_page.dart';
import '../ui/views/wallet/transaction_details_page.dart';

class UpiRoutes {
  UpiRoutes._();

  // upi Module
  static List<GetPage> get routes => [
    GetPage(
      name: UpiActivatePage.routeName,
      page: () => UpiActivatePage(),
    ),
    GetPage(
      name: SelectSimPage.routeName,
      page: () => SelectSimPage(),
    ),
    GetPage(
      name: UpiSuccessfullyActivatedPage.routeName,
      page: () => UpiSuccessfullyActivatedPage(),
    ),
    GetPage(
      name: AddbankPage.routeName,
      page: () => AddbankPage(),
    ),
    GetPage(
      name: AddBankSuccessPage.routeName,
      page: () => AddBankSuccessPage(),
    ),
    GetPage(
      name: AddbankNoAccount.routeName,
      page: () => AddbankNoAccount(),
    ),
    GetPage(
      name: AddbankOtherBank.routeName,
      page: () => AddbankOtherBank(),
    ),
    GetPage(
      name: UpiTransferSearchPage.routeName,
      page: () => UpiTransferSearchPage(),
    ),
    GetPage(
      name: AddNewUpiPage.routeName,
      page: () => AddNewUpiPage(),
    ),
    GetPage(
      name: UpiPayPage.routeName,
      page: () => UpiPayPage(),
    ),
    GetPage(
      name: UpiTransactionPinPage.routeName,
      page: () => UpiTransactionPinPage(),
    ),
    GetPage(
      name: UpiTransferSuccessPage.routeName,
      page: () => UpiTransferSuccessPage(),
    ),
    GetPage(
      name: BankTransferSearchPage.routeName,
      page: () => BankTransferSearchPage(),
    ),
    GetPage(
      name: AddNewBeneficiaryPage.routeName,
      page: () => AddNewBeneficiaryPage(),
    ),
    GetPage(
      name: TransferToSelfPage.routeName,
      page: () => TransferToSelfPage(),
    ),
    GetPage(
      name: ScanAndPayPage.routeName,
      page: () => ScanAndPayPage(),
    ),
    GetPage(
      name: MobileRechargePrepaidPage.routeName,
      page: () => MobileRechargePrepaidPage(),
    ),
    GetPage(
      name: TransactionDetailsPage.routeName,
      page: () => TransactionDetailsPage(),
    ),
  ];

}