

import 'package:flutter/cupertino.dart';
import 'package:yuvapay/ui/usefull/palette.dart';

class BoxDecorations{

  static  Decoration decorationWiteCard= BoxDecoration(
    borderRadius: const BorderRadius.all(Radius.circular(12)),
    boxShadow: [
      BoxShadow(
          color: Palette.black.withOpacity(0.075),
          offset: const Offset(0, 2),
          blurRadius: 4)
    ],
    color: Palette.colorPageBg,
  );
}

