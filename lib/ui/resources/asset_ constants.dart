
class AssetConstants{
  /*  */
  static const String svgIconExtension = '.svg';
  static const String pngIconExtension = '.png';
  static const String _rootIconsPath = 'assets/images';
  static const String _rootAnimPath = 'assets/anim';
  static const String _rootSvgPath = '$_rootIconsPath/svg';
  static const String _rootPngPath = '$_rootIconsPath/png';
 // static const String _rootPngPath = '$_rootAnimPath/png';

  //onBoarding UPI

  static const String ic_simple_online_payment = '$_rootSvgPath/ic_simple_online_payment.svg';
  static const String icLogoWithYuvapay = '$_rootSvgPath/ic_logo_with_yuvapay.svg';
  static const String icLogo = '$_rootSvgPath/ic_logo.svg';
  static const String ic_secure_outer = '$_rootSvgPath/ic_secure_outer.svg';
  static const String ic_cashless = '$_rootSvgPath/ic_cashless.svg';
  static const String icSim2 = '$_rootSvgPath/ic_sim2.svg';
  static const String icSim1 = '$_rootSvgPath/ic_sim1.svg';
  static const String icErrorAlert = '$_rootSvgPath/ic_error_alert.svg';
  static const String icInstantPayments = '$_rootSvgPath/ic_instant_payments.svg';
  static const String icOfflineTransactions = '$_rootSvgPath/ic_offline_transactions.svg';
  static const String icRewards = '$_rootSvgPath/ic_rewards.svg';
  static const String icFluentMoney = '$_rootSvgPath/ic_fluent_money.svg';
  static const String icHandZero = '$_rootSvgPath/ic_hand_zero.svg';
  static const String icCard = '$_rootSvgPath/ic_card.svg';
  static const String icYuvaBank = '$_rootSvgPath/ic_yuva_bank.svg';
  static const String icDirection = '$_rootSvgPath/ic_direction.svg';
  static const String icSuccess = '$_rootSvgPath/ic_success.svg';
  static const String icSuccessBank = '$_rootSvgPath/ic_success_bank.svg';
  static const String icSearch = '$_rootSvgPath/ic_search.svg';
  static const String icCanaraBank = '$_rootSvgPath/ic_canara_bank.svg';
  static const String icSbiBank = '$_rootSvgPath/ic_sbi.svg';
  static const String icSpeechBubbles = '$_rootSvgPath/ic_speech_bubbles.svg';
  static const String icPoweredByYesbankBhimUpi = '$_rootSvgPath/ic_powered_by_yesbank_bhim_upi.svg';
  static const String icPoweredByYesbank = '$_rootSvgPath/powered_by_yesbank.svg';
  static const String icUpiUser = '$_rootSvgPath/ic_upi_user.svg';
  static const String icFlagIndia = '$_rootSvgPath/ic_flag_india.svg';
  static const String icRupeeSymbol = '$_rootSvgPath/ic_rupee_symbol.svg';
  static const String icShare = '$_rootSvgPath/ic_share.svg';

  //wallete
  //ic_visa.svg
  static const String icVisa = '$_rootSvgPath/ic_visa.svg';
  static const String icEyeClosed = '$_rootSvgPath/ic_eye_closed.svg';
  static const String icEyeOpen = '$_rootSvgPath/ic_eye_open.svg';
  static const String icFreeze = '$_rootSvgPath/icFreeze.svg';
  static const String icCardFreeze = '$_rootSvgPath/ic_card_freeze.svg';
  static const String icSetting = '$_rootSvgPath/ic_setting.svg';
  static const String walletCardBackground = '$_rootPngPath/wallet_card_background.png';
  static const String icArrowLeftDown = '$_rootSvgPath/ic_arrow_left_down.svg';
  static const String icBankTransfer = '$_rootSvgPath/ic_bank_transfer.svg';
  static const String icWallet = '$_rootSvgPath/ic_wallet.svg';
  static const String icTransaction = '$_rootSvgPath/ic_transaction.svg';
  static const String icKeyboardWithAbc = '$_rootSvgPath/ic_keyboard_with_abc.svg';

  //animation file
  static const String animLoading = '$_rootAnimPath/loading.json';
  static const String animSuccessUpiActivate = '$_rootAnimPath/anim_success_upi_activate.json';
  static const String animUpiAddAankSuccess = '$_rootAnimPath/anim_upi_add_bank_success.json';
  static const String animPaymentSuccess = '$_rootAnimPath/anim_payment_success.json';
  static const String animWelcomeToYuvaPay = '$_rootAnimPath/anim_welcome_to_yuvapay.json';

  //Home
  static const String sendMoneyIconBase = 'assets/images/svg/home_module/send_money';
  static const String rechargeIconBase = 'assets/images/svg/home_module/recharge';
  static const String financialIconBase = 'assets/images/svg/home_module/financial';
  static const String investmentsIconBase = 'assets/images/svg/home_module/investment';
  static const String rewardsIconBase = 'assets/images/svg/home_module/rewards';
  static const String youtubeSliderImage = 'assets/images/png/youtube0.png';
  static const String exploreSliderImageBase = 'assets/images/png/explore';
  static const String referBannerImage = 'assets/images/png/refer_banner.png';
  static const String homeBanner = 'assets/images/png/home_frame1.png';

  //My Account
  static const String myAccountIconBase = 'assets/images/svg/my_account_module/myaccount';
  static const String myAccountPartIconBase = 'assets/images/svg/my_account_module/myaccountpart';
  static const String myAccountProfileImage = 'assets/images/png/home_appbar_profile.png';


  //Account Security
  static const String accountSecurityIconBase = 'assets/images/png/security';

  //Account Details
  static const String canaraBankImage = 'assets/images/png/canarabank.png';
  static const String defaultTickImage = 'assets/images/svg/my_account_module/defaultTick.svg';

  //Notifications
  static const String notificationIconBase='assets/images/png/notification';

  //Page
  static const String bottomNavBarIconBase='assets/images/png/bnb';
  static const String appBarReward='assets/images/png/rewards0.png';
  static const String yuvaPayLogo='assets/images/png/yuva_logo.png';
  static const String appBarProfile='assets/images/png/home_appbar_profile.png';

  //Linked Accounts
  static const String addNewBankAccount='assets/images/png/linked2.png';
  static const String linkedBanksImageBase='assets/images/png/linked';

  //Change Pin
  static const String changePinIcon='assets/images/png/changepin0.png';
  static const String errorWardingSign='assets/images/svg/my_account_module/warningSign.svg';
  static const String successSign='assets/images/svg/my_account_module/defaultTick.svg';



  static const String animPinResetSuccess = '$_rootAnimPath/anim_pinreset_success.json';
  static const String animCaution = '$_rootAnimPath/anim_caution.json';

  // Wallet
  static const String kycImage ='$_rootPngPath/kyc_image.png';
  static const String walletTick ='$_rootSvgPath/wallet_tick.svg';
  static const String walletLock ='$_rootSvgPath/wallet_lock.svg';
}