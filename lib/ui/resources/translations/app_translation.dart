import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';

class AppTranslations extends Translations {
  AppTranslations._internal() {
    _translations.addAll(_englishTranslations);
    _translationsHi.addAll(_englishTranslations);
  }

  static final _instance = AppTranslations._internal();

  factory AppTranslations.get() => _instance;

  @override
  Map<String, Map<String, String>> get keys {
    return {
      "en_US": _translations,
      "hi_IN": _translationsHi,
    };
  }

  /*Locale? getLocale(LanguageBundle bundle) {
    switch (bundle.name) {
      case "EN":
        return const Locale('en', 'US');
      case "HI":
        return const Locale('hi', 'IN');
    }
    return null;
  }

  void updateLocale(LanguageBundle bundle, Map<String, String> map) {
    final locale = getLocale(bundle);
    if (locale == null) return;
    var tempTranslations = Map<String, String>.from(_englishTranslations);
    tempTranslations.addAll(map);
    _translations.addAll(tempTranslations);
    Get.updateLocale(locale);
  }*/

  final _translations = <String, String>{};
  final _translationsHi = <String, String>{};

  /// Please add all new string here
  ///
  /// Rule for creating key is,
  /// [module_component_variable] eg. module is leave,
  /// component is dashboard and variable name is [My Team Leaves]
  /// then key is "leave_dashboard_my_team_leaves"
  ///
  /// There are some actionable text which are common eg. [view, see all, hide, read more, read less]
  final _englishTranslations = {
    "powered_by": "Powered by",

    //OnBoarding
    TranslationKeys.select_your_language: "Choose a preferred language",
    TranslationKeys.button_contnue: "Continue",
    TranslationKeys.kindly_recheck_the_otp_entered: "Kindly recheck the OTP entered",
    TranslationKeys.enter_phone_number: "Enter Phone Number",
    TranslationKeys.enter_your_email_id: "Enter your email ID",
    TranslationKeys.to_get_started_enter_your_phone_number: "To get started, enter your phone number",
    TranslationKeys.register_your_email_address: "Register your email address",
    TranslationKeys.one_time_password: "One Time Password (OTP)",
    TranslationKeys.we_have_sent_you_an_code_to_your_number: "We have sent you an OTP code to your number",
    TranslationKeys.edit: "Edit",
    TranslationKeys.mail_verification_code: "Mail Verification Code",
    TranslationKeys.we_have_sent_you_a_verification_code_to_your_email_address: "We have sent you a verification code to your email address",
    TranslationKeys.resend_verification_code: "Resend verification code",
    TranslationKeys.welcome_to_yuvaPay: "Welcome to ",
    TranslationKeys.take_me_to_home: "Take me to Home",
    TranslationKeys.apply: "Apply",
    TranslationKeys.referral_code_applied: "Referral Code Applied",
    TranslationKeys.have_a_referral_code: "Have a referral code?",
    TranslationKeys.add_referral_code: "Add Referral Code",
    TranslationKeys.enter_invite_code_to_reward_referral_bonus_to_your_friends: "Enter invite code to reward referral bonus to your friends",
    TranslationKeys.enter_referral_code: "Enter Referral Code",
    TranslationKeys.continue_with_logged_in_account: "Continue with logged in account",
    TranslationKeys.pleaseSelectLanguage: "Continue with logged in account",
    TranslationKeys.weWillRequireFewPermissionsToServeYouBetter: "We will require a few permissions to serve you better",
    TranslationKeys.yourInformationIsSafeAndSecureUs: "Your information is safe and secure with us",

    //Upi
    TranslationKeys.activateUpiTransactions: "Activate UPI Transasctions",
    TranslationKeys.nowYouCanLinkYourBankAccountToMakePayments: "Now you can link your bank account to make payments for",
    TranslationKeys.activateIn2minutes: "Activate in 2 minutes",
    TranslationKeys.simpleOnlinePayments: "Simple Online Payments",
    TranslationKeys.advancedSecurity: "Advanced Security",
    TranslationKeys.cashlessEconomy: "Cashless Economy",
    TranslationKeys.registerYourPhoneNumber: "Register your phone Number",
    TranslationKeys.registerRBIGuidlinesText: "As per RBI guidlines, we would have to verify your mobile number that is registered with the bank",
    TranslationKeys.byClickingContinueYouAgreeAndoConditions : "By clicking continue, you agree and comply to our  terms & conditions",
    TranslationKeys.sim1 : "Sim 1",
    TranslationKeys.sim2 : "Sim 2",
    TranslationKeys.jio4G : "Jio 4G",
    TranslationKeys.airtel4G : "airtel 4G",
    TranslationKeys.errorOccured : "Error Occured",
    TranslationKeys.pleaseTryAgainLater : "Please try again later",
    TranslationKeys.okay : "Okay",
    TranslationKeys.upiSuccessfullyActivated : "UPI successfully activated",
    TranslationKeys.linkYourBankAccount : "Link your Bank Account",
    TranslationKeys.upi : "UPI",
    TranslationKeys.wallete : "Wallet",
    TranslationKeys.activateYourWallet : "Activate your Wallet",
    TranslationKeys.yourWalletIsCurrentlyInactive : "Your wallet is currently inactive. You canreactivate your YuvaPay wallet for seamlesstransactions.",
    TranslationKeys.enterPANCardDetails : "Enter PAN card details",
    TranslationKeys.instantPayments : "Instant Payments",
    TranslationKeys.offlineTransactions : "Offline Transactions! (Coming soon)",
    TranslationKeys.rewardsBenifits : "Rewards & Benifits",
    TranslationKeys.enterName : "Enter Your Name",
    TranslationKeys.namePANcard : "Name on PAN card",
    TranslationKeys.panNumber : "PAN Number",
    TranslationKeys.enterYourPANNumber : "Enter your PAN Number",
    TranslationKeys.resendOTP  : "Resend OTP ",
    TranslationKeys.walletSuccessfullyActivated  : "Wallet successfully activated",
    TranslationKeys.yourWalletTisSuccessfullyActivatedTransactions  : "Your wallet is successfully activated. Enjoy your YuvaPay wallet for seamless transactions.",
    TranslationKeys.done : "Done",
    TranslationKeys.backToHome : "Back to Home",
    TranslationKeys.sendViaPhoneNumber : "Send via Phone Number",

    //Neo Bank
    TranslationKeys.neoBank : "Neo Bank",
    TranslationKeys.simplifiedBankAccountEveryone: "Simplified Bank Account for Everyone- It’s Free!",
    TranslationKeys.openYourBankAccountMinutes: "Open your Bank account in 10 minutes",
    TranslationKeys.sbAccoun7Interest: "S/B account @ 7% interest",
    TranslationKeys.zeroMinimumBalance: "Zero minimum balance",
    TranslationKeys.freeOnlineDebitCard: "Free online Debit Card",
    TranslationKeys.letYourMoneyMakeMoney: "Let your money make money",
    TranslationKeys.wedonWantYouWorryAboutLowCalanceCharges: "We don’t want you to worry about low balance charges",
    TranslationKeys.forEasyyShoppingExperience: "For easyy shopping experience",
    TranslationKeys.aadharCardDetails: "Aadhar Card Details",
    TranslationKeys.nameonAadharCard: "Name on Aadhar Cards",
    TranslationKeys.enterYourName : "Enter Your Name ",
    TranslationKeys.aadharCardNumber : "Aadhar Card Number",
    TranslationKeys.enteryourAadharNumber : "Enter your Aadhar Number",
    TranslationKeys.aadharCardOTP : "Aadhar Card OTP",
    TranslationKeys.weHaveSentOTPYourNumber : "We have sent you an OTP code to your registered mobile number",
    TranslationKeys.nextVideoKYC : "Next: Video KYC",
    TranslationKeys.makeSureYouHaveTheFollowingReady : "Make sure you have the following ready:",
    TranslationKeys.physicalPANCard : "Physical PAN Card",
    TranslationKeys.goodInternetConnectivity : "Good Internet Connectivity",
    TranslationKeys.wellLitEnvironment : "Well lit environment",
    TranslationKeys.autosendSMSToCreateUPI : "Autosend SMS to create UPI",
    TranslationKeys.weHaveRecievedYourApplication : "We have recieved your application",
    TranslationKeys.yourApplicationHasBeenSubmittedYouWillRecieveAmail : "Your application has been submitted. You will recieve a mail as soon as your application has been processed. This may take upto 2-3 business days.",

    //Home
    TranslationKeys.contactCustomerCare: "Contact Customer Care",
    TranslationKeys.availableOnWorking: "Available on working days from 9 AM to 6 PM",
    TranslationKeys.helpAndSupport: "Help & Support",
    TranslationKeys.yuvapayVideos: "YuvaPay Videos",
    TranslationKeys.youtube: "Youtube ->",
    TranslationKeys.financial_education: "Financial Education",
    TranslationKeys.explore: "Explore ->",
    TranslationKeys.rewardsAndBonus: "Rewards & Bonus",
    TranslationKeys.investments: "Investments",
    TranslationKeys.financialServices: "Financial Services",
    TranslationKeys.rechargesBillsAndPayments: "Recharges, Bills & Payments",
    TranslationKeys.sendMoneyVia: "Send Money Via",
    TranslationKeys.requestMoney: "Request Money",
    TranslationKeys.myQrCode: "My QR Code",
    TranslationKeys.checkBalance: "Check Balance",
    TranslationKeys.upiIdTransfer: "UPI ID Transfer",
    TranslationKeys.dummyUpiId: "UPI ID:contactprahlad@okaxis",
    TranslationKeys.newVersionAvailable: "A new version is available",
    TranslationKeys.download: "Download",
    TranslationKeys.remindMeLater: "Remind me later",
    TranslationKeys.howToUseYuvaPay: "How to use YuvaPay?",
    TranslationKeys.bhimUpi: "BHIM UPI",
    TranslationKeys.bankTransfer: "Bank Transfer",
    TranslationKeys.toMobileNumber: "To Mobile Number",
    TranslationKeys.selfTransfer: "Self Transfer",
    TranslationKeys.buyInsurance: "Buy Insurance",
    TranslationKeys.checkScore: "Check Sore",
    TranslationKeys.applyCard: "Apply Card",
    TranslationKeys.goldLocker: "Gold Locker",
    TranslationKeys.goldAndSilver: "Gold & Silver",
    TranslationKeys.myRewards: "My Rewards",
    TranslationKeys.spinToWIn: "Spin to Win",
    TranslationKeys.referAndEarn: "Refer & Earn",
    TranslationKeys.walletTopUp: "Wallet Topup?",
    TranslationKeys.mobileRecharge: "Mobile Recharge",
    TranslationKeys.electricityBill: "Electricity Bill",
    TranslationKeys.dthRecharge: "DTH Recharge",
    TranslationKeys.fastTag: "Fastag",
    TranslationKeys.rent: "Rent",
    TranslationKeys.gasCylinder: "Gas Cylinder",
    TranslationKeys.more: "More",

    //Search
    TranslationKeys.searchAnything: "Search anything",
    TranslationKeys.noSearchResults: "No results found. These are the popular services you might be interested in.",
    TranslationKeys.makePayment: "Make Payment",
    TranslationKeys.sendMoney: "Send Money",
    TranslationKeys.rechargesAndBillPayments: "Recharges And Bill Payments",

    //Base Page
    TranslationKeys.yuvapayTitle: "YuvaPay",

    //add bank
    TranslationKeys.addBank: "Add Bank",
    TranslationKeys.popularBanks: "Popular Banks",
    TranslationKeys.searchBank: "Search Bank",
    TranslationKeys.allBank: "All Bank",
    TranslationKeys.bankLinkedSuccessfully: "Bank Linked Successfully",
    TranslationKeys.setUpUpiPin: "Set up UPI PIN",
    TranslationKeys.addMoney: "Add Money",
    TranslationKeys.addbankNoNumberRegisterWithBank: "The phone number “+91 98273838738” has no accounts linked to Abyudaya bank",
    TranslationKeys.tryOtherBank: "Try other Bank",
    TranslationKeys.setPin: "Set Pin",
    TranslationKeys.debitCreditCardDetails: "Debit Card Details",
    TranslationKeys.lastDigitsOfYourCardNumber: "Last 6 digits of your card number",
    TranslationKeys.expiryDate: "Expiry Date",
    TranslationKeys.proceed: "Proceed",
    TranslationKeys.upiPinSuccessfullySet: "UPI PIN successfully set",
    TranslationKeys.somethingWentWrong: "Something went wrong",
    TranslationKeys.retry: "Retry",
    TranslationKeys.back: "Back",

    //UPI transfer
    TranslationKeys.upiIDTransfer: "UPI ID Transfer",
    TranslationKeys.addNewUpiId: "Add New UPI ID",
    TranslationKeys.searchUpiIDs: "Search UPI IDs",
    TranslationKeys.savedUpiIDs: "Saved UPI IDs",
    TranslationKeys.proceedToPay: "Proceed to Pay",
    TranslationKeys.enterUpiID: "Enter UPI ID",
    TranslationKeys.invalidUpiID: "Invalid UPI ID",
    TranslationKeys.invalidAccountNumber: "Invalid Account Number",
    TranslationKeys.accountNumberDoesNotMatch: "Account Number Does Not Match",
    TranslationKeys.invalidIfscCode: "Invalid IFSC Code",
    TranslationKeys.enterAmount: "Enter amount",
    TranslationKeys.addNoteOptional: "Add a note (optional)",
    TranslationKeys.payFrom: "Pay From",
    TranslationKeys.insufficientFunds: "Insufficient Funds",
    TranslationKeys.thePaymentWasDeclinedBecauseYouNotHaveEnouthBalanace: "The payment was declined because you don’t have enough balance in your account",
    TranslationKeys.thePaymentWasDeclinedBecauseYouBankServersWereTooBusy: "The payment was declined because yourbank servers were too busy",
    TranslationKeys.thePinInvalidKindlyRetryOrResetPin: "The pin you entered is invalid. Kindly retry with a valid pin or reset pin",
    TranslationKeys.successfullySent: "Successfully sent ₹",
    TranslationKeys.viewDetails: "View Details",
    TranslationKeys.share: "Share",
    TranslationKeys.addNewBeneficiary: "Add New Beneficiary",
    TranslationKeys.searchSavedBankAccounts: "Search saved bank accounts",
    TranslationKeys.accountNumber: "Account Number",
    TranslationKeys.confirmAccountNumber: "Confirm Account Number",
    TranslationKeys.nameOptional: "Name (Optional)",
    TranslationKeys.forgotPin: "Forgot PIN",
    TranslationKeys.bankServersBusy: "Bank Servers Busy",
    TranslationKeys.invalidPin: "Invalid pin",
    TranslationKeys.selfTransferTo: "Self Transfer To",
    TranslationKeys.addNewBankAccount: "Add a new bank account",
    TranslationKeys.scanQrCodeToPay: "Scan QR code to pay",
    TranslationKeys.flashlight: "Flashlight",
    TranslationKeys.qrFromGallery: "QR from Gallery",


    //My Account
    TranslationKeys.myAccount: "My Account",
    TranslationKeys.policies: "Policies",
    TranslationKeys.language: "Language",
    TranslationKeys.logOut: "Log Out",

    //My Profile
    TranslationKeys.emailAddress: "Email Address",
    TranslationKeys.finishYourKyc: "Finish your KYC",
    TranslationKeys.yuvapayWalletActivated: "YuvaPay Wallet Activated",

    //Linked Bank Accounts
    TranslationKeys.linkedBankAccounts: "Linked Bank Accounts",
    TranslationKeys.defaultAccount: "Default",

    //Account Security
    TranslationKeys.security: "Security",
    TranslationKeys.changeYuvaPayPin: "Change YuvaPay Pin",
    TranslationKeys.forgotYuvaPayPin: "Forgot YuvaPay Pin?",
    TranslationKeys.recommendedToBeOn: "Recommended to be ON",
    TranslationKeys.recoverPinUsing: "Recover Pin Using",
    TranslationKeys.securityQuestions: "Security Questions",
    TranslationKeys.alternatePhoneNumber: "Alternate Phone Number",
    TranslationKeys.emailId: "Email Id",

    // Reset Via Security Question
    TranslationKeys.resetPinViaSecurityQuestions: "Reset Pin Via Security Question",
    TranslationKeys.yourAnswerCaps: "YOUR ANSWER",
    TranslationKeys.yourAnswer: "Your answer",

    //Help And Support
    TranslationKeys.yuvapayGuide: "Yuvapay Guide",
    TranslationKeys.raiseATicket: "Raise a Ticket",

    //Policies
    TranslationKeys.yuvapayPolicies: "Yuvapay Policies",
    TranslationKeys.npciUpiGrievanceRedressal: "NPCI UPI Grievance Redressal",
    TranslationKeys.yesBankGrievanceRedressal: "Yes Bank Grievance Redressal",

    // YuvaPay Policies
    TranslationKeys.about: "About",
    TranslationKeys.privacyPolicy: "Privacy Policy",
    TranslationKeys.refundPolicy: "Refund Policy",
    TranslationKeys.termsAndConditions: "Terms & Conditions",
    TranslationKeys.faqGeneral: "FAQ - General",
    TranslationKeys.mostImportantTAndC: "Most Important T&C",
    TranslationKeys.upiTermsAndConditions: "UPI Terms & Conditions",
    TranslationKeys.upiFaq: "UPI FAQ",
    TranslationKeys.prepaidCardFaq: "Prepaid Card FAQ",
    TranslationKeys.prepaidcardTAndC: "Prepaid Card T&C",
    TranslationKeys.insuranceFaq: "Insurance FAQ",
    TranslationKeys.subscriptionOfferfaq: "Subscription Offer FAQ",
    TranslationKeys.upiLimits: "UPI Limits",


    //Account Details
    TranslationKeys.accountDetails: "Account Details",
    TranslationKeys.unlinkBankAccount: "Unlink Bank Account",
    TranslationKeys.canaraBank: "Canara Bank",
    TranslationKeys.xx1188: "XX 1188",
    TranslationKeys.setAsDefault: "Set As Default",
    TranslationKeys.viewQrCode: "View QR Code",
    TranslationKeys.accountInformation: "Account Information",
    TranslationKeys.bankingName: "Banking Name",
    TranslationKeys.phoneNumber: "Phone Number",
    TranslationKeys.accountType: "Account Type",
    TranslationKeys.branch: "Branch",
    TranslationKeys.ifsc: "IFSC",
    TranslationKeys.upiPin: "UPI PIN",
    TranslationKeys.upiPinExists: "UPI PIN exists",
    TranslationKeys.reset: "Reset",
    TranslationKeys.change: "Change",
    TranslationKeys.balance: "Balance:",
    TranslationKeys.balanceWithOutColon: "Balance",
    TranslationKeys.upiId: "UPI ID",
    TranslationKeys.active: "ACTIVE:",

    //Notification
    TranslationKeys.notifications: "Notifications",

    //Page
    TranslationKeys.home: "Home",
    TranslationKeys.wallet: "Wallet",
    TranslationKeys.transfer: "Transfer",

    //Change Pin
    TranslationKeys.confirmYuvaPayPin: "Confirm YuvaPay Pin",
    TranslationKeys.toResetPasswordConfirmYuvaPayPin: "To reset password, confirm your pin",
    TranslationKeys.wrongPassword: "Wrong Password",
    TranslationKeys.youHaveTwoAttemptsLeft: "You have 2 attempts left. 3 invalid attempts will lock Yuvapay for 24 hours.",
    TranslationKeys.tooManyUnsuccessfulAttempts: "Too many unsuccessful attempts. Yuvapay will be locked for 24 hours.",
    TranslationKeys.closingAppInFiveSeconds: "Closing app in 5 seconds.",

    //Confirm Pin
    TranslationKeys.confirmPin: "Confirm Pin",
    TranslationKeys.confirm: "Confirm",
    TranslationKeys.newPin: "New Pin",
    TranslationKeys.passwordsDoNotMatch: "Pin Did Not Match",
    TranslationKeys.passwordsSuccessfullyReset: "Passwords Successfully Reset",

    // Field Checking
    TranslationKeys.email: "Email",
    TranslationKeys.mobileNumber: "Mobile Number",
    TranslationKeys.answer: "Answer",
    TranslationKeys.oneTimePasswordOtp: "One Time Password (OTP)",
    TranslationKeys.doesNotMatchLine:" entered does not match. You have 2 attempts left. 3 invalid attempts will lock Yuvapay for 24 hours.",
    TranslationKeys.thisfieldcanNotBeLeftblank: "This field can not be left blank",

    //Wallet Onboarding
    TranslationKeys.yuvaPayWallet: "Yuva Pay Wallet (Prepaid Card)",
    TranslationKeys.completeYourKyc: "Complete Your KYC",
    TranslationKeys.asPerRbiText: "As per RBI guideline, complete e-KYC to use Yuva Pay wallet",
    TranslationKeys.getMonthlyLimitText: "Get monthly limit of Rs 1.00.000 & withdraw to bank account",
    TranslationKeys.useYourEKycText: "Use your e-KYC for other regulatory requirements to access products and services",
    TranslationKeys.byClickingText: "By clicking verify you are accepting to YuvaPay’s ",
    TranslationKeys.verifyWithAadhaar: "Verify with AADHAR",

    //Complete Your KYC
    TranslationKeys.enterYourEmailId: "Enter your Email ID ",
    TranslationKeys.enterAlternatePhoneNumber: "Enter Alternate Mobile Number (Optional)",
    TranslationKeys.selectSecurityQuestion: "Select Security Question",
    TranslationKeys.selectAnswer: "Secret Answer",
    TranslationKeys.enterAlternatePhoneNumberOtp: "Enter Alternate Mobile Number OTP",
    TranslationKeys.enterAadhaarNUmber: "Enter AADHAR Number",
    TranslationKeys.byClickingContinueYouAreText: "By clicking continue you are allowing your consent to open a wallet account subjected to one year expiry",

    //One Time Password
    TranslationKeys.oneTimePassword: "One Time Password ( OTP)",
    TranslationKeys.enterTheOtpWhichHasText: "Enter the OTP which has been sent to your registered mobile number for the purpose of AADHAR verification",
    TranslationKeys.byClickingContinueText: "By clicking continue I declare that I was born in India, I am an Indian citizen, Indian resident and tax paying resident of India and of no other country.",
    TranslationKeys.continueB: "Continue",
    TranslationKeys.congratulations: "Congratulations",
    TranslationKeys.kycUpdatedSuccessfully: "KYC Updated Successfully",
    TranslationKeys.createYuvaPayPin: "Create Yuva Pay PIN",
    TranslationKeys.otpEnteredIsInvalid: "OTP entered is invalid. Kindly enter correct OTP.",

    //wallete

    TranslationKeys.yuvaPayWalletPrepaidCard: "Yuva Pay Wallet (Prepaid Card)",
    TranslationKeys.prepaidCard: "Prepaid Card",
    TranslationKeys.cardDetailsOff: "Card Details Off",
    TranslationKeys.cardDetailsOn: "Card Details On",
    TranslationKeys.freezeCardOff: "Freeze Card Off",
    TranslationKeys.manageCard: "Manage Card",
    TranslationKeys.walletBalance: "Wallet Balance",
    TranslationKeys.topUp: "Top-Up",
    TranslationKeys.requestToWallet: "Request to wallet",
    TranslationKeys.transferToBank: "Transfer to Bank",
    TranslationKeys.sendToWallet: "Send to Wallet",
    TranslationKeys.walletTransactions: "Wallet Transactions",
    TranslationKeys.enterPhoneNumberName: "Enter phone number or name",
    TranslationKeys.yuvaPayContacts: "YuvaPay Contacts",
    TranslationKeys.transactionLimit: "Transaction Limit",
    TranslationKeys.enableOnlineTransactions: "Enable Online Transactions",
    TranslationKeys.disablingWillBlockOnlineTransactions: "Disabling will block online transactions",

    /// Errors messages -start

    /// Set Biometric bottomsheet -end
  }; final _hindiTranslations = {
    "powered_by": "Powered by",
    TranslationKeys.select_your_language: "अपनी भाषा का चयन करें",
    TranslationKeys.button_contnue: "जारी रखें",

    /// Errors messages -start

    /// Set Biometric bottomsheet -end
  };
}