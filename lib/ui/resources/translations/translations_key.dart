class TranslationKeys {
  TranslationKeys._();

  //onBoarding
  static const String privacy_policy = "privacy_policy";
  static const String input_error = "input_error";
  static const String select_your_language = "select_your_language";
  static const String button_contnue = "continue";
  static const String kindly_recheck_the_otp_entered =
      "kindly_recheck_the_otp_entered";
  static const String enter_phone_number = "enter_phone_number";
  static const String enter_your_email_id = "enter_your_email_id";
  static const String to_get_started_enter_your_phone_number =
      "to_get_started_enter_your_phone_number";
  static const String register_your_email_address =
      "to_get_started_enter_your_email_address";
  static const String one_time_password = "one_time_password";
  static const String we_have_sent_you_an_code_to_your_number =
      "we_have_sent_you_an_code_to_your_number";
  static const String edit = "edit";
  static const String mail_verification_code = "mail_verification_code";
  static const String
      we_have_sent_you_a_verification_code_to_your_email_address =
      "we_have_sent_you_a_verification_code_to_your_email_address";
  static const String resend_verification_code = "resend_verification_code";
  static const String welcome_to_yuvaPay = "welcome_to_yuvaPay";
  static const String take_me_to_home = "take_me_to_home";
  static const String apply = "apply";
  static const String have_a_referral_code = "have_a_referral_code";
  static const String referral_code_applied = "referral_code_applied";
  static const String add_referral_code = "add_referral_code";
  static const String
      enter_invite_code_to_reward_referral_bonus_to_your_friends =
      "enter_invite_code_to_reward_referral_bonus_to_your_friends";
  static const String enter_referral_code = "enter_referral_code";
  static const String continue_with_logged_in_account =
      "continue_with_logged_in_account";
  static const String pleaseSelectLanguage = "pleaseSelectLanguage";
  static const String weWillRequireFewPermissionsToServeYouBetter =
      "weWillRequireFewPermissionsToServeYouBetter";
  static const String yourInformationIsSafeAndSecureUs =
      "yourInformationIsSafeAndSecureUs";

  //UPI
  static const String activateUpiTransactions = "activateUpiTransasctions";
  static const String nowYouCanLinkYourBankAccountToMakePayments =
      "nowYouCanLinkYourBankAccountToMakePayments";
  static const String activateIn2minutes = "activateIn2minutes";
  static const String simpleOnlinePayments = "simpleOnlinePayments";
  static const String advancedSecurity = "advancedSecurity";
  static const String cashlessEconomy = "cashlessEconomy";
  static const String registerYourPhoneNumber = "registerYourPhoneNumber";
  static const String registerRBIGuidlinesText = "registerRBIGuidlinesText";
  static const String byClickingContinueYouAgreeAndoConditions =
      "byClickingContinueYouAgreeAndoConditions";
  static const String sim1 = "sim1";
  static const String sim2 = "sim2";
  static const String jio4G = "jio4G";
  static const String airtel4G = "airtel4G";
  static const String errorOccured = "errorOccured";
  static const String pleaseTryAgainLater = "pleaseTryAgainLater";
  static const String okay = "okay";
  static const String upiSuccessfullyActivated = "upi_successfully_activated";
  static const String linkYourBankAccount = "linkYourBankAccount";
  static const String upi = "upi";
  static const String wallete = "wallete";
  static const String activateYourWallet = "activateYourWallet";
  static const String yourWalletIsCurrentlyInactive =
      "yourWalletIsCurrentlyInactive";
  static const String enterPANCardDetails = "enterPANCardDetails";
  static const String instantPayments = "instantPayments";
  static const String offlineTransactions = "offlineTransactions";
  static const String rewardsBenifits = "rewardsBenifits";
  static const String enterName = "enterName";
  static const String namePANcard = "namePANcard";
  static const String panNumber = "panNumber";
  static const String enterYourPANNumber = "enterYourPANNumber";
  static const String resendOTP = "resendOTP";
  static const String walletSuccessfullyActivated =
      "walletSuccessfullyActivated";
  static const String yourWalletTisSuccessfullyActivatedTransactions =
      "yourWalletTisSuccessfullyActivatedTransactions";
  static const String done = "done";
  static const String backToHome = "backToHome";
  static const String sendViaPhoneNumber = "sendViaPhoneNumber";

  //neo bank
  static const String neoBank = "neoBank";
  static const String simplifiedBankAccountEveryone =
      "simplifiedBankAccountEveryone";
  static const String openYourBankAccountMinutes = "openYourBankAccountMinutes";
  static const String sbAccoun7Interest = "sbAccoun7Interest";
  static const String zeroMinimumBalance = "zeroMinimumBalance";
  static const String freeOnlineDebitCard = "freeOnlineDebitCard";
  static const String letYourMoneyMakeMoney = "letYourMoneyMakeMoney";
  static const String wedonWantYouWorryAboutLowCalanceCharges =
      "wedonWantYouWorryAboutLowCalanceCharges";
  static const String forEasyyShoppingExperience = "forEasyyShoppingExperience";
  static const String aadharCardDetails = "aadharCardDetails";
  static const String nameonAadharCard = "nameonAadharCard";
  static const String enterYourName = "enterYourName";
  static const String aadharCardNumber = "aadharCardNumber";
  static const String enteryourAadharNumber = "enteryourAadharNumber";
  static const String aadharCardOTP = "aadharCardOTP";
  static const String weHaveSentOTPYourNumber = "weHaveSentOTPYourNumber";
  static const String nextVideoKYC = "nextVideoKYC";
  static const String makeSureYouHaveTheFollowingReady =
      "makeSureYouHaveTheFollowingReady";
  static const String physicalPANCard = "physicalPANCard";
  static const String goodInternetConnectivity = "goodInternetConnectivity";
  static const String wellLitEnvironment = "wellLitEnvironment";
  static const String autosendSMSToCreateUPI = "autosendSMSToCreateUPI";
  static const String weHaveRecievedYourApplication =
      "weHaveRecievedYourApplication";
  static const String yourApplicationHasBeenSubmittedYouWillRecieveAmail =
      "yourApplicationHasBeenSubmittedYouWillRecieveAmail";

  //Home
  static const String contactCustomerCare = "contact_customer_care";
  static const String availableOnWorking = "available_on_working";
  static const String helpAndSupport = "help_and_support";
  static const String yuvapayVideos = "yuvapay_videos";
  static const String youtube = "youtube";
  static const String financial_education = "financial_education";
  static const String explore = "explore";
  static const String rewardsAndBonus = "rewards_and_bonus";
  static const String investments = "investments";
  static const String financialServices = "financial_services";
  static const String rechargesBillsAndPayments =
      "recharges_bills_and_payments";
  static const String sendMoneyVia = "send_money_via";
  static const String requestMoney = "request_money";
  static const String myQrCode = "my_qr_code";
  static const String checkBalance = "check_balance";
  static const String upiIdTransfer = "upi_id_transfer";
  static const String dummyUpiId = "dummy_upi_id";
  static const String newVersionAvailable = "new_version_available";
  static const String download = "download";
  static const String remindMeLater = "remind_me_later";
  static const String howToUseYuvaPay = "how_to_use_yuva_pay";
  static const String bhimUpi = "bhimUpi";
  static const String bankTransfer = "bankTransfer";
  static const String toMobileNumber = "toMobileNumber";
  static const String selfTransfer = "selfTransfer";
  static const String buyInsurance = "buyInsurance";
  static const String checkScore = "checkScore";
  static const String applyCard = "applyCard";
  static const String goldLocker = "goldLocker";
  static const String goldAndSilver = "goldAndSilver";
  static const String myRewards = "myRewards";
  static const String spinToWIn = "spinToWIn";
  static const String referAndEarn = "referAndEarn";
  static const String walletTopUp = "walletTopUp";
  static const String mobileRecharge = "mobileRecharge";
  static const String electricityBill = "electricityBill";
  static const String dthRecharge = "dthRecharge";
  static const String fastTag = "fastTag";
  static const String rent = "rent";
  static const String gasCylinder = "gasCylinder";
  static const String more = "more";

  //Search
  static const String searchAnything = "search_anything";
  static const String noSearchResults = "no_search_results";
  static const String makePayment = "makePayment";
  static const String sendMoney = "sendMoney";
  static const String rechargesAndBillPayments = "rechargesAndBillPayments";

  //Base Page
  static const String yuvapayTitle = "yuva_pay_title";

  //add bank
  static const String addBank = "addBank";
  static const String popularBanks = "popularBanks";
  static const String searchBank = "searchBank";
  static const String allBank = "allBank";
  static const String bankLinkedSuccessfully = "bankLinkedSuccessfully";
  static const String setUpUpiPin = "setUpUpiPin";
  static const String addMoney = "addMoney";
  static const String addbankNoNumberRegisterWithBank =
      "addbankNoNumberRegisterWithBank";
  static const String tryOtherBank = "tryOtherBank";
  static const String setPin = "setPin";
  static const String debitCreditCardDetails = "debitCreditCardDetails";
  static const String lastDigitsOfYourCardNumber = "lastDigitsOfYourCardNumber";
  static const String expiryDate = "expiryDate";
  static const String proceed = "proceed";
  static const String upiPinSuccessfullySet = "upiPinSuccessfullySet";
  static const String somethingWentWrong = "somethingWentWrong";
  static const String retry = "retry";
  static const String back = "Back";

  //UPI transfer
  static const String upiIDTransfer = "upiIDTransfer";
  static const String addNewUpiId = "addNewUpiId";
  static const String searchUpiIDs = "searchUpiIDs";
  static const String savedUpiIDs = "savedUpiIDs";
  static const String proceedToPay = "proceedToPay";
  static const String enterUpiID = "enterUpiID";
  static const String invalidUpiID = "invalidUpiID";
  static const String invalidAccountNumber = "invalidAccountNumber";
  static const String accountNumberDoesNotMatch = "accountNumberDoesNotMatch";
  static const String invalidIfscCode = "invalidIfscCode";
  static const String enterAmount = "enterAmount";
  static const String addNoteOptional = "addNoteOptional";
  static const String payFrom = "payFrom";
  static const String insufficientFunds = "insufficientFunds";
  static const String thePaymentWasDeclinedBecauseYouNotHaveEnouthBalanace =
      "thePaymentWasDeclinedBecauseYouNotHaveEnouthBalanace";
  static const String thePaymentWasDeclinedBecauseYouBankServersWereTooBusy =
      "thePaymentWasDeclinedBecauseYouBankServersWereTooBusy";
  static const String thePinInvalidKindlyRetryOrResetPin =
      "thePinInvalidKindlyRetryOrResetPin";
  static const String successfullySent = "successfullySent₹";
  static const String viewDetails = "viewDetails";
  static const String share = "share";
  static const String addNewBeneficiary = "addNewBeneficiary";
  static const String searchSavedBankAccounts = "searchSavedBankAccounts";
  static const String accountNumber = "accountNumber";
  static const String confirmAccountNumber = "confirmAccountNumber";
  static const String nameOptional = "nameOptional";
  static const String forgotPin = "forgotPin";
  static const String bankServersBusy = "bankServersBusy";
  static const String invalidPin = "invalidPin";
  static const String selfTransferTo = "selfTransferTo";
  static const String addNewBankAccount = "addNewBankAccount";
  static const String scanQrCodeToPay = "scanQrCodeToPay";
  static const String flashlight = "flashlight";
  static const String qrFromGallery = "qrFromGallery";

  //My Account
  static const String myAccount = "myAccount";
  static const String policies = "policies";
  static const String language = "language";
  static const String logOut = "logOut";

  //My Profile
  static const String myProfile = "myProfile";
  static const String emailAddress = "emailAddress";
  static const String finishYourKyc = "finishYouKyc";
  static const String yuvapayWalletActivated = "yuvapayWalletActivated";

  //Linked Accounts
  static const String linkedBankAccounts = "linkedBankAccounts";
  static const String defaultAccount = "defaultAccount";

  //Account Security
  static const String security = "security";
  static const String screenLockSecurity = "screenLockSecurity";
  static const String changeYuvaPayPin = "changeYuvaPayPin";
  static const String forgotYuvaPayPin = "forgotYuvaPayPin";
  static const String recommendedToBeOn = "recommendedToBeOn";
  static const String recoverPinUsing = "recoverPinUsing";
  static const String securityQuestions = "securityQuestions";
  static const String alternatePhoneNumber = "alternatePhoneNumber";
  static const String emailId = "emailId";

  // Reset Via Security Questions
  static const String resetPinViaSecurityQuestions =
      "resetPinViaSecurityQuestions";
  static const String yourAnswerCaps = "yourAnswerCaps";
  static const String yourAnswer = "yourAnswer";

  // Help And Support
  static const String yuvapayGuide = "yuvapayGuide";
  static const String raiseATicket = "raiseATicket";

  // Policies
  static const String yuvapayPolicies = "yuvapayPolicies";
  static const String npciUpiGrievanceRedressal = "upciNpiGrievanceRedressal";
  static const String yesBankGrievanceRedressal = "yesBankGrievanceRedressal";

  // YuvaPay Policies
  static const String about = "about";
  static const String privacyPolicy = "privacyPolicy";
  static const String refundPolicy = "refundPolicy";
  static const String termsAndConditions = "termsAndConditions";
  static const String faqGeneral = "faqGeneral";
  static const String mostImportantTAndC = "mostImportantTAndC";
  static const String upiTermsAndConditions = "upiTermsAndConditions";
  static const String upiFaq = "upiFaq";
  static const String prepaidCardFaq = "prepaidCardFaq";
  static const String prepaidcardTAndC = "prepaidcardTAndC";
  static const String insuranceFaq = "insuranceFaq";
  static const String subscriptionOfferfaq = "subscriptionOfferfaq";
  static const String upiLimits = "upiLimits";

  //Account Details
  static const String accountDetails = "accountDetails";
  static const String unlinkBankAccount = "unlinkBankAccount";
  static const String canaraBank = "canaraBank";
  static const String xx1188 = "xx1188";
  static const String setAsDefault = "setAsDefault";
  static const String viewQrCode = "viewQrCode";
  static const String accountInformation = "accountInformation";
  static const String bankingName = "bankingName";
  static const String phoneNumber = "phoneNumber";
  static const String accountType = "accountType";
  static const String branch = "branch";
  static const String ifsc = "ifsc";
  static const String upiPin = "upiPin";
  static const String upiPinExists = "upiPinExists";
  static const String reset = "reset";
  static const String change = "change";
  static const String balance = "balance";
  static const String balanceWithOutColon = "balanceWithOutColon";
  static const String upiId = "upiId";
  static const String active = "active";

  //Notification
  static const String notifications = "notifications";

  //Page
  static const String home = "home";
  static const String wallet = "wallet";
  static const String transfer = "transfer";

  //Chnage Pin
  static const String confirmYuvaPayPin = "confirmYuvaPayPin";
  static const String toResetPasswordConfirmYuvaPayPin =
      "toResetPasswordConfirmYuvaPayPin";
  static const String wrongPassword = "wrongPassword";
  static const String youHaveTwoAttemptsLeft = "youHaveTwoAttemptsLeft";
  static const String tooManyUnsuccessfulAttempts =
      "tooManyUnsuccessfulAttempts";
  static const String closingAppInFiveSeconds =
      "tclosingAppInFiveSecondsransfer";

  //Confirm Pin
  static const String confirmPin = "confirmPin";
  static const String confirm = "Confirm";
  static const String newPin = "newPin";
  static const String passwordsDoNotMatch = "passwordsDoNotMatch";
  static const String passwordsSuccessfullyReset = "passwordsSuccessfullyReset";

  //Field Checking
  static const String email = "email";
  static const String mobileNumber = "mobileNumber";
  static const String answer = "answer";
  static const String oneTimePasswordOtp = "oneTimePasswordOtp";
  static const String doesNotMatchLine = "doesNotMatchLine";
  static const String thisfieldcanNotBeLeftblank = "thisfieldcanNotBeLeftblank";

//Wallet Onboarding
  static const String yuvaPayWallet="yuvaPayWallet";
  static const String completeYourKyc="completeYourKyc";
  static const String asPerRbiText="asPerRbiText";
  static const String getMonthlyLimitText="getMonthlyLimitText";
  static const String useYourEKycText="useYourEKycText";
  static const String byClickingText="byClickingText";
  static const String verifyWithAadhaar="verifyWithAadhaar";

  //Complete Your KYC
  static const String enterYourEmailId="enterYourEmailId";
  static const String enterAlternatePhoneNumber="enterAlternatePhoneNumber";
  static const String selectSecurityQuestion="selectSecurityQuestion";
  static const String selectAnswer="selectAnswer";
  static const String enterAlternatePhoneNumberOtp="enterAlternatePhoneNumberOtp";
  static const String enterAadhaarNUmber="enterAadhaarNUmber";
  static const String byClickingContinueYouAreText="byClickingContinueYouAreText";

  //One Time Password
  static const String oneTimePassword="oneTimePassword";
  static const String enterTheOtpWhichHasText="enterTheOtpWhichHasText";
  static const String byClickingContinueText="byClickingContinueText";
  static const String continueB="continueB";
  static const String congratulations="congratulations";
  static const String kycUpdatedSuccessfully="kycUpdatedSuccessfully";
  static const String createYuvaPayPin="createYuvaPayPin";
  static const String otpEnteredIsInvalid="otpEnteredIsInvalid";




//wallete
  static const String yuvaPayWalletPrepaidCard = "yuvaPayWalletPrepaidCard";
  static const String prepaidCard = "prepaidCard";
  static const String cardDetailsOff = "cardDetailsOff";
  static const String cardDetailsOn = "cardDetailsOn";
  static const String freezeCardOff = "freezeCardOff";
  static const String manageCard = "manageCard";
  static const String walletBalance = "walletBalance";
  static const String topUp = "topUp";
  static const String requestToWallet = "requestToWallet";
  static const String transferToBank = "transferToBank";
  static const String sendToWallet = "sendToWallet";
  static const String walletTransactions = "walletTransactions";
  static const String enterPhoneNumberName = "enterPhoneNumberName";
  static const String yuvaPayContacts = "yuvaPayContacts";
  static const String transactionLimit = "transactionLimit";
  static const String enableOnlineTransactions = "enableOnlineTransactions";
  static const String disablingWillBlockOnlineTransactions = "disablingWillBlockOnlineTransactions";

  //Mobile RechargePrepaid Card



}
