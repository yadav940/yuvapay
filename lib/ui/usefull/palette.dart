import 'dart:ui';

import 'package:flutter/material.dart';

class Palette {
  Palette._();

   static const Color colorPrimary = Color(0xff2348AD);
   static const Color colorPageBg = Colors.white;
   //static const Color colorError = Colors.red;
   static const Color colorCardBg = Color(0xFFF7F7FA); // rgb: 247, 247, 250
   static const Color colorPageBgG05 = Color(0xFFF7F7F8);
   static const Color colorPageBgG07 = Color(0xFFF2F2F3);
   static const Color colorPrimaryText = Color(0xff1C1C28);
   static const Color colorPrimaryTextG90 = Color(0xff191A1B);
   static const Color colorPrimaryTextG80 = Color(0xff303236);
   static const Color colorPrimaryTextG70 = Color(0xff494D51);
   static const Color colorPrimaryTextG50 = Color(0xff7A7D86);
   static const Color colorTextLabel = Color(0xff8F90A6);
   static const Color colorBottomNavBar = Color(0xFF031842);
   static const Color black = Color(0xFF000000);
   static const Color colorCardText = Colors.white;

   static const Color colorGoalRed = Color.fromRGBO(229, 53, 53, 1.0);

   static const Color colorError = Color(0xFFC91D1D);
   static const Color colorWarning = Color(0xFFE8A530);

   static const Color dividerColor = Color(0xffE5E5E5);
   static const Color colorUserBackgroundPink =
       Color.fromRGBO(238, 109, 116, .67);

   static const Color colorBorders = Color(0xffE4E4EB);
  static const Color colorBorders1 = Color(0xFFE9F0F7);
   static const Color colorBorders2 = Color.fromRGBO(60, 145, 54, 1);
   static const Color greyScaleDark0 = Color(0xFF1C1C28);
   static const Color greyScaleDark2 = Color(0xff555770);
   static const Color greyScaleDark3 = Color(0xFF8F90A6);
   static const Color greyScaleDark4 = Color(0xffC7C9D9);
   static const Color greyScaleDark5 = Color(0xffC4C4C4);
   static const Color greyScaleDark6 = Color(0xff7A7D86);
   static const Color greyScaleDark7 = Color(0xffC9CCCF);
   static const Color greyScaleDark8 = Color(0xffF6F7F8);
   static const Color greyScaleLight0 = Color(0xFFE4E4EB);
   static const Color greyScaleLight1 = Color(0xFFEBEBF0);
   static const Color greyScaleLight3 = Color(0xFFF7F7FA);
   static const Color psMain = Color(0xFF2A5EE3);
   static const Color colorBorderG10 = Color(0xFFE5E5E7);
   static const Color buttonBackground = Color(0xFF2348AD);
   static const Color greenBackground = Color(0xFF35C298);
   static const Color messageSuccess0 = Color(0xFF4BB543);


}

