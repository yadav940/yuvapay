import 'package:flutter_svg/flutter_svg.dart';
import 'package:yuvapay/controller/home/home_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/home/phone_ui.dart';
import 'package:yuvapay/ui/views/onboarding/upi/upi_activate.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';

import '../payment/details_screen.dart';
import '../prepaid/select_state_page.dart';

class HomePage extends AppPageWithAppAndBottomBar {
  static const routeName = "/home";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(HomeController());

  @override
  Widget get body {
    return SafeArea(
      child: SingleChildScrollView(
        child: _showBodyContent(),
      ),
    );
  }

  Widget _showBodyContent() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: Column(
        children: [
          const SizedBox(height: 8),
          InkWell(
            onTap: (){
              Get.to(DetailsScreen());
            },
            child: SizedBox(
              height: 160,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  _showBanner(),
                  _showBanner(),
                ],
              ),
            ),
          ),
          const SizedBox(height: 16),
          InkWell(
              onTap: (){
Get.to(PhoneUiPage());
              },

              child: _showUpiIdSection()),
          const SizedBox(height: 12),
          _showSendMoneySection(),
          const SizedBox(height: 12),
          _showRechargeSection(),
          const SizedBox(height: 12),
          _showReferBannerView(),
          const SizedBox(height: 12),
          _showFinancialSection(),
          const SizedBox(height: 12),
          _showInvestmentSection(),
          const SizedBox(height: 12),
          _showFinancialSlider(),
          const SizedBox(height: 12),
          _showYoutubeSlider(),
          const SizedBox(height: 20),
          const PoweredByYesBankBhim(),
          const SizedBox(height: 48),
        ],
      ),
    );
  }






  Widget _showYoutubeSlider() => _showCardSlider(
      heading: TranslationKeys.yuvapayVideos.tr,
      subHeading: TranslationKeys.youtube.tr,
      isExplore: false);

  Widget _showFinancialSlider() => _showCardSlider(
      heading: TranslationKeys.financial_education.tr,
      subHeading: TranslationKeys.explore.tr,
      isExplore: true);

  Widget _showRewardsSection() {
    return _showCardItem(
        iconTextList: controller.rewards,
        heading: TranslationKeys.rewardsAndBonus.tr,
        itemCount: 3,
        iconBase: controller.rewardsIconBase,
        isCircular: true,
        backgroundColor: Palette.colorCardBg,
        scale: 0.5);
  }

  Widget _showInvestmentSection() {
    return _showCardItem(
        iconTextList: controller.investments,
        heading: TranslationKeys.investments.tr,
        itemCount: 2,
        iconBase: controller.investmentsIconBase,
        isCircular: false,
        backgroundColor: Palette.colorCardBg,
        scale: 0.5);
  }

  Widget _showFinancialSection() {
    return _showCardItem(
        iconTextList: controller.financial,
        heading: TranslationKeys.financialServices.tr,
        itemCount: 3,
        iconBase: controller.financialIconBase,
        isCircular: true,
        backgroundColor: Palette.colorCardBg,
        scale: 0.5);
  }

  Widget _showReferBannerView() {
    return SizedBox(
      child: Center(
        child: SizedBox(
          height: 140,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
              _showReferBannerItem(),
              _showReferBannerItem(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _showReferBannerItem() {
    return Padding(
      padding: const EdgeInsets.only(right: 8),
      child: SizedBox(child: Image.asset(AssetConstants.referBannerImage)),
    );
  }

  Widget _showRechargeSection() {
    return _showCardItem(
        iconTextList: controller.recharge,
        heading: TranslationKeys.rechargesBillsAndPayments.tr,
        itemCount: 8,
        iconBase: controller.rechargeIconBase,
        isCircular: true,
        backgroundColor: Palette.colorCardBg,
        scale: 0.5);
  }

  Widget _showSendMoneySection() {
    return _showCardItem(
        iconTextList: controller.sendVia,
        heading: TranslationKeys.sendMoneyVia.tr,
        itemCount: 4,
        iconBase: controller.sendMoneyIconBase,
        isCircular: true,
        backgroundColor: Colors.transparent,
        scale: 1);
  }

  Widget _showCardItem(
      {required List<String> iconTextList,

      required String heading,
      required int itemCount,
      required String iconBase,
      required bool isCircular,
      required Color backgroundColor,
      required double scale}) {
    return AppCard(
      padding: const EdgeInsets.all(16),
      child: _showCardContent(iconTextList, heading, itemCount, iconBase,
          isCircular, backgroundColor, scale),
    );
  }

  Widget _showCardContent(
      List<String> iconTextList,
      String heading,
      int itemCount,
      String iconBase,
      bool isCircular,
      Color backgroundColor,
      double scale) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(heading, style: TextStyles.sp18(fontWeight: FontWeight.w500)),
        const SizedBox(height: 20),
        _showCommonGridIcons(
            iconTextList: iconTextList,
            itemCount: itemCount,
            iconBase: iconBase,
            isCircular: isCircular,
            backgroundColor: backgroundColor,
            scale: scale),
      ],
    );
  }

  Widget _showUpiIdSection() {
    return AppCard(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 14),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _showUpiId(),
          const SizedBox(height: 12),
          _showUpiIdOptions(),
        ],
      ),
    );
  }

  Widget _showUpiIdOptions() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(
          onTap: (){
            UpiActivatePage.start();
          },
          child: Text(
            TranslationKeys.requestMoney.tr,
            style: TextStyles.sp14(color: Palette.buttonBackground),
          ),
        ),
        _showSeparator(),
        Text(TranslationKeys.myQrCode.tr,
            style: TextStyles.sp14(color: Palette.buttonBackground)),
        _showSeparator(),
        Text(TranslationKeys.checkBalance.tr,
            style: TextStyles.sp14(color: Palette.buttonBackground)),
        const SizedBox(height: 0),
      ],
    );
  }

  Widget _showSeparator() {
    return Container(width: 0.8, height: 16, color: Colors.black);
  }

  Widget _showUpiId() {
    return Row(
      children: [
        Text(TranslationKeys.dummyUpiId.tr, style: TextStyles.sp14()),
        const SizedBox(width: 10),
        const Icon(Icons.copy, color: Palette.colorPrimaryText, size: 17),
      ],
    );
  }

  BoxDecoration _tileDecoration() {
    return const BoxDecoration(
        color: Palette.colorPageBg,
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }

  Widget _showBanner() {
    return SizedBox(
      child: Image.asset(AssetConstants.homeBanner),
    );
  }

  Widget _showCommonGridIcons(
      {required List<String> iconTextList,
      required int itemCount,
      required String iconBase,
      required bool isCircular,
      required Color backgroundColor,
      required double scale}) {
    return GridView.count(
      crossAxisCount: 4,
      crossAxisSpacing: 0.0,
      mainAxisSpacing: 0.0,
      shrinkWrap: true,
      childAspectRatio: 0.7,
      physics: const ClampingScrollPhysics(),
      children: List.generate(
        itemCount,
        (index) {
          return _gridViewItem(iconTextList, index, backgroundColor, isCircular,
              iconBase, scale);
        },
      ),
    );
  }

  Widget _gridViewItem(List<String> iconTextList, int index,
      Color backgroundColor, bool isCircular, String iconBase, double scale) {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: InkWell(
        onTap: () {
          print('$iconBase$index${AssetConstants.svgIconExtension}');
          controller.selectTab(iconTextList[index]);
        },
        child: Column(
          children: [
            Container(
              height: 56,
              width: 56,
              decoration: BoxDecoration(
                  color: index == 7 ? Colors.transparent : backgroundColor,
                  borderRadius:
                      BorderRadius.all(Radius.circular(isCircular ? 56 : 12))),
              child: Transform.scale(
                scale: scale,
                child: SvgPicture.asset(
                    '$iconBase$index${AssetConstants.svgIconExtension}'),
              ),
            ),
            const SizedBox(height: 8),
            Text(
              iconTextList[index],
              style: TextStyles.sp14(fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  Widget _showCardSlider(
      {required String heading,
      required String subHeading,
      required bool isExplore}) {
    return AppCard(
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _showSliderTopSection(heading, subHeading),
          const SizedBox(height: 20),
          SizedBox(
            child: Center(
              child: SizedBox(
                height: 140,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    isExplore ? _showExploreImage(0) : _showYoutubeImage(),
                    isExplore ? _showExploreImage(1) : _showYoutubeImage(),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _showYoutubeImage() {
    return Padding(
      padding: const EdgeInsets.only(right: 8),
      child: Container(
        width: 200,
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AssetConstants.youtubeSliderImage),
                fit: BoxFit.cover),
            borderRadius: BorderRadius.all(Radius.circular(12))),
        child: Stack(
          children: [
            Positioned(
                left: 12,
                bottom: 12,
                child: Text(
                  TranslationKeys.howToUseYuvaPay.tr,
                  style: TextStyles.sp14(color: Palette.colorPageBg),
                ))
          ],
        ),
      ),
    );
  }

  Widget _showExploreImage(int imageId) {
    return Padding(
      padding: const EdgeInsets.only(right: 8),
      child: SizedBox(
          child: Image.asset(
              '${AssetConstants.exploreSliderImageBase}$imageId${AssetConstants.pngIconExtension}')),
    );
  }

  Widget _showSliderTopSection(String heading, String subHeading) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(heading, style: TextStyles.sp18(fontWeight: FontWeight.w500)),
        Text(subHeading, style: TextStyles.sp12(fontWeight: FontWeight.normal)),
      ],
    );
  }
}
