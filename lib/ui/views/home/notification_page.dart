import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:yuvapay/controller/home/notification_controller.dart';
import 'package:yuvapay/services/navigation.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';

class NotificationPage extends AppPageWithAppAndBottomBar {
  static const routeName = "/notification";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(NotificationController());

  @override
  double? get toolbarHeight => 0;

  @override
  ConvexAppBar? get bottomNavigationBar => null;

  @override
  // TODO: implement appBar
  PreferredSizeWidget? get appBar => AppBar(
      backgroundColor: Palette.buttonBackground,
      leading: IconButton(
        visualDensity: VisualDensity.compact,
        splashRadius: 24,
        onPressed: Get.back,
        icon: const Icon(Icons.arrow_back_outlined),
      ),
      title: const Text(TranslationKeys.notifications));

  @override
  Widget get body {
    return SafeArea(
      child: SingleChildScrollView(
        child: _showContent(),
      ),
    );
  }

  Widget _showContent() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 16.0),
      child: ListView.builder(
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          itemCount: 2,
          itemBuilder: (BuildContext context, int index) {
            return _showNotificationItem(index);
          }),
    );
  }

  Widget _showNotificationItem(int index) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: AppCard(
        // decoration: _tileDecoration(),
        padding: const EdgeInsets.all(25),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 25,
              width: 25,
              child: Image.asset(
                  '${AssetConstants.notificationIconBase}$index${AssetConstants.pngIconExtension}'),
            ),
            const SizedBox(
              width: 25,
            ),
            Text('${TranslationKeys.notifications} ${index + 1}',
                style: TextStyles.sp14()),
          ],
        ),
      ),
    );
  }

  BoxDecoration _tileDecoration() {
    return const BoxDecoration(
        color: Palette.colorPageBg,
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }
}
