import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:yuvapay/controller/home/global_search_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';

class GlobalSearchPage extends AppPageWithAppAndBottomBar {
  static const routeName = "/global-search";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(GlobalSearchController());

  @override
  double? get toolbarHeight => 0;

  @override
  ConvexAppBar? get bottomNavigationBar => null;

  @override
  // TODO: implement appBar
  PreferredSizeWidget? get appBar => _searchAppBar();

  AppBar _searchAppBar() {
    return AppBar(
      backgroundColor: Palette.buttonBackground,
      leading: _backButton(),
      title: _searchBar(),
    );
  }

  Widget _searchBar() {
    return Container(
      height: 45,
      padding: const EdgeInsets.only(left: 0, right: 0),
      decoration: _searchBarDecoration(),
      child: Center(
        child: _searchBarTextField(),
      ),
    );
  }

  BoxDecoration _searchBarDecoration() {
    return BoxDecoration(
      color: Palette.colorPageBg,
      borderRadius: const BorderRadius.all(Radius.circular(8)),
      border: Border.all(
        color: Colors.transparent,
        width: 1,
      ),
    );
  }

  Widget _searchBarTextField() {
    return TextField(
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.text,
      style: TextStyles.sp16(fontWeight: FontWeight.w400),
      controller: controller.globalSearchController,
      textAlign: TextAlign.left,
      decoration: _searchTextFieldDecoration(),
      onChanged: (searchString) => controller.checkSearchString(),
    );
  }

  InputDecoration _searchTextFieldDecoration() {
    return InputDecoration(
        prefixIcon: const Icon(Icons.search, size: 24),
        suffixIcon: InkWell(
          onTap: () => controller.emptySearch(),
          child: const Icon(Icons.close, size: 24),
        ),
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        hintText: TranslationKeys.searchAnything.tr,
        hintStyle: TextStyles.sp16(
            fontWeight: FontWeight.w400, color: Palette.greyScaleDark6));
  }

  Widget _backButton() {
    return IconButton(
      visualDensity: VisualDensity.compact,
      splashRadius: 24,
      onPressed: Get.back,
      icon: const Icon(Icons.arrow_back_outlined),
    );
  }

  @override
  Widget get body {
    return SafeArea(
      child: SingleChildScrollView(
        child: _showContent(),
      ),
    );
  }

  Widget _showContent() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 16),
      child: Column(
        children: [
          Obx(() {
            return controller.isUnknownSearch.value
                ? _showNoSearchResultsText()
                : const SizedBox(height: 0);
          }),
          Obx(() {
            return controller.isPay.value
                ? _showSearchResultSection()
                : _showRechargeSection();
          }),
        ],
      ),
    );
  }

  Widget _showNoSearchResultsText() {
    return Padding(
      padding: const EdgeInsets.only(top: 4, left: 12, right: 12, bottom: 12),
      child: Text(TranslationKeys.noSearchResults.tr, style: TextStyles.sp16()),
    );
  }

  Widget _showSearchResultSection() {
    return AppCard(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 0),
      child: ListView.builder(
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          itemCount: 3,
          itemBuilder: (BuildContext context, int index) {
            return _showSearchResultItem(index);
          }),
    );
  }

  Widget _showSearchResultItem(int index) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(controller.searchResults[index], style: TextStyles.sp16()),
              Transform.rotate(
                  angle: 90,
                  child: const Icon(Icons.arrow_back_outlined,
                      size: 20, color: Palette.black)),
            ],
          ),
        ),
        index != 2 ? const Divider() : const SizedBox(height: 0),
      ],
    );
  }

  Widget _showRechargeSection() {
    return _showCardItem(
        heading: TranslationKeys.rechargesBillsAndPayments.tr,
        itemCount: 8,
        iconBase: AssetConstants.rechargeIconBase,
        isCircular: true,
        backgroundColor: Palette.colorCardBg,
        scale: 3);
  }

  Widget _showCardItem(
      {required String heading,
      required int itemCount,
      required String iconBase,
      required bool isCircular,
      required Color backgroundColor,
      required double scale}) {
    return Container(
      decoration: _tileDecoration(),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: _showCardContent(
            heading, itemCount, iconBase, isCircular, backgroundColor, scale),
      ),
    );
  }

  BoxDecoration _tileDecoration() {
    return const BoxDecoration(
        color: Palette.colorPageBg,
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }

  Widget _showCardContent(String heading, int itemCount, String iconBase,
      bool isCircular, Color backgroundColor, double scale) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _showCommonGridIcons(
            itemCount: itemCount,
            iconBase: iconBase,
            isCircular: isCircular,
            backgroundColor: backgroundColor,
            scale: scale),
      ],
    );
  }

  Widget _showCommonGridIcons(
      {required int itemCount,
      required String iconBase,
      required bool isCircular,
      required Color backgroundColor,
      required double scale}) {
    return GridView.count(
      crossAxisCount: 4,
      crossAxisSpacing: 0.0,
      mainAxisSpacing: 0.0,
      shrinkWrap: true,
      childAspectRatio: 0.7,
      physics: const ClampingScrollPhysics(),
      children: List.generate(
        itemCount,
        (index) {
          return _gridViewItem(
              index, backgroundColor, isCircular, iconBase, scale);
        },
      ),
    );
  }

  Widget _gridViewItem(int index, Color backgroundColor, bool isCircular,
      String iconBase, double scale) {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: Column(
        children: [
          _showGridIcons(index, backgroundColor, isCircular, iconBase, scale),
          const SizedBox(height: 8),
          Text(
            controller.recharge[index],
            style: TextStyles.sp14(fontWeight: FontWeight.normal),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  Widget _showGridIcons(int index, Color backgroundColor, bool isCircular,
      String iconBase, double scale) {
    return Container(
      height: 56,
      width: 56,
      decoration: BoxDecoration(
          color: index == 7 ? Colors.transparent : backgroundColor,
          borderRadius:
              BorderRadius.all(Radius.circular(isCircular ? 56 : 12))),
      child: SizedBox(
          height: 32,
          width: 32,
          child: Transform.scale(
            scale: 0.5,
            child: SvgPicture.asset(
              '$iconBase$index${AssetConstants.svgIconExtension}'
            ),
          )),
    );
  }
}
