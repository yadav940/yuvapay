import 'package:yuvapay/controller/home/loading_controller.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';

class PhoneUiPage extends AppPageWithAppBar {
  // static const routeName = "/loading";

  // static void start() {
  //   navigateOffAll(routeName);
  // }
@override
  // TODO: implement appBar
  PreferredSizeWidget? get appBar => null;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(height: 0),
Column(
  children: [
    Text('Successfully Recharged ₹599',style: TextStyles.sp18(fontWeight: FontWeight.w600),),
   const SizedBox(height: 16,),
    Container(
      padding: const EdgeInsets.all(20),
      decoration:  BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(25)),
        border: Border.all(),
      ),
      child: const Text('View Details'),
    ),
  ],
),
AppButton(onPressed: (){},text: 'Done',),
          ],
        ),
      ),
    );
  }


}