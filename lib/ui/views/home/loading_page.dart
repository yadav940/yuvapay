import 'package:yuvapay/controller/home/loading_controller.dart';
import 'package:yuvapay/ui/views/base/base.dart';

class LoadingPage extends AppPageWithAppBar {
  static const routeName = "/loading";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(LoadingController());

  @override
  double? get toolbarHeight => 0;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
       padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Stack(
          children: [
            _showLogoAndName(),
            _showBottomText(),
          ],
        ),
      ),
    );
  }

  Widget _showLogoAndName() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 132,
            width: 144,
            child: Image.asset('assets/images/png/yuvapay_logo.png'),
          ),
          SizedBox(
            height: 22,
            width: 96,
            child: Image.asset('assets/images/png/yuvapay_name.png'),
          ),
        ],
      ),
    );
  }

  Widget _showBottomText(){
    return Positioned(
      left: 0,
      right: 0,
      bottom: 0,
      child: Column(
        children: [
          SizedBox(
            height: 16,
            // width: 80,
            child: Image.asset('assets/images/png/powered_by_image.png'),
          ),
          const SizedBox(height: 20),
          SizedBox(
            height: 16,
            // width: 50,
            child: Image.asset('assets/images/png/loading_partners_image.png'),
          ),
        ],
      ),
    );
  }


}
