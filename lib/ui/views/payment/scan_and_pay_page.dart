import 'dart:io';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';
import 'package:file_picker/file_picker.dart';

import '../../../../../controller/onboarding/upi/upi_search_controller.dart';
import '../onboarding/upi/transfer_to_upi/upi_pay_page.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class ScanAndPayPage extends AppPageWithAppBar {
  static const routeName = "/scan_and_pay_page";

  static void start() {
    Get.toNamed(ScanAndPayPage.routeName);
  }

  @override
  Color get actionBarbackgroundColor => Palette.black.withOpacity(0.75);

  @override
  Color get pageBackgroundColor => Palette.black.withOpacity(0.75);

  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  QRViewController? controller;

  @override
  void reassemble() {
    //super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget get body {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          cameraView,
          Text(
            TranslationKeys.scanQrCodeToPay.tr,
            style: TextStyles.pageTitle(color: Palette.colorPageBg),
          ),
          bottomButton
        ],
      ),
    );
  }

  Widget get cameraView {
    return SizedBox(
          width: (Get.size.width - 55),
          height: (Get.size.width - 55),
          child: QRView(
            key: qrKey,
            onQRViewCreated: _onQRViewCreated,
            overlay: QrScannerOverlayShape(
                borderColor: Palette.colorPageBg,
                borderWidth: 10,
                borderRadius: 8),
          ),
        );
  }

  Widget get bottomButton {
    return AppCard(
      bgColor: Palette.colorPrimaryTextG90,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          InkWell(
            onTap: (){
              controller?.toggleFlash();
            },
            child: getItem(true, TranslationKeys.flashlight.tr),
          ),
          InkWell(
            onTap: () async {
              FilePickerResult? result =
                  await FilePicker.platform.pickFiles(type: FileType.image);
              if (result != null) {
                debugPrint('---------QR from Gallery');
                UpiPayPage.start();
              } else {}
            },
            child: getItem(false, TranslationKeys.qrFromGallery.tr),
          ),
        ],
      ),
    );
  }

  Widget getItem(bool isLight, String text) {
    return Row(
      children: [
        Icon(
          isLight ? Icons.highlight_sharp : Icons.photo_album_outlined,
          color: Palette.colorPageBg,
        ),
        const SizedBox(
          width: 8,
        ),
        Text(
          text,
          style: TextStyles.sp16(color: Palette.colorPageBg),
        )
      ],
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      debugPrint('------------${scanData.code!}');
      UpiPayPage.start();
      /*setState(() {
        result = scanData;
      });*/
    });
  }

  Widget get sizedBox {
    return const SizedBox(
      height: 12,
    );
  }
}
