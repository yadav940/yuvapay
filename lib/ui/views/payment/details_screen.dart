import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';
import '../base/page.dart';
import '../widgets/app_card.dart';

class DetailsScreen extends AppPageWithAppBar {
  static const routeName = "Postpaid";
  TextEditingController globalSearchController = TextEditingController();

  static void start() {
    Get.toNamed(DetailsScreen.routeName);
  }

  @override
  Color get actionBarbackgroundColor => Palette.colorPrimaryTextG80;

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  @override
  Widget get body {
    return SafeArea(
      child: Column(
        children: [
          getUserDetail,
          Padding(
            padding: const EdgeInsets.all(10),
            child: AppCard(
              child: Column(
                children: [
                  _showDropDownMenu(),
                  const SizedBox(height: 9),
                  _showDropDownMenu(),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5),
            child: AppCard(
              child: Column(
                children: [
                  _searchBar(),
                  SizedBox(height: 16),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Recent'),
                      Text('Popular'),
                      Text('Special Package'),
                      Text('Date'),
                    ],
                  ),
                  const SizedBox(height: 16),
                  Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        color: Palette.colorCardBg,
                        borderRadius: BorderRadius.all(Radius.circular(25))),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Text(
                                  '299',
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 4),
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(25))),
                                  child: Text(
                                    'Best Seller',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 8),
                              decoration: BoxDecoration(
                                  color: Colors.blueAccent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(25))),
                              child: Text(
                                'Recharge',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 16),
                        Divider(),
                        SizedBox(height: 16),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Text('Validity'),
                                SizedBox(
                                  height: 4,
                                ),
                                Text('84 Days'),
                              ],
                            ),
                            Column(
                              children: [
                                Text('Data'),
                                SizedBox(
                                  height: 4,
                                ),
                                Text('2GB /day'),
                              ],
                            ),
                            Column(
                              children: [
                                Text('More'),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),

          ),
        ],
      ),
    );
  }

  Widget get getUserDetail {
    return Container(
      height: 200,
      color: Palette.colorPrimaryTextG80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              SvgPicture.asset(
                AssetConstants.icUpiUser,
                height: 50,
              ),
              const SizedBox(
                height: 24,
              ),
              Text(
                'Send to Cheteshwari C Rao',
                style: TextStyles.sp18(color: Palette.colorPageBg),
              ),
              const SizedBox(
                height: 6,
              ),
              Text(
                'chetishwari@ybl',
                style: TextStyles.sp14(color: Palette.colorPrimaryTextG50),
              ),
              const SizedBox(
                height: 24,
              ),
            ],
          )
        ],
      ),
    );
  }

  DropdownButtonFormField<String> _showDropDownMenu() {
    return DropdownButtonFormField(
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Select Operator',
        hintStyle: TextStyles.sp16(color: Palette.greyScaleDark7),
        // suffixIcon: Icon(Icons.arrow_drop_down),
      ),
      dropdownColor: Colors.white,
      items: [
        // HARDCODED OPTIONS.
        // ACTUAL OPTIONS WILL COME FROM APIs.
        DropdownMenuItem(
            child: Text(
              "Airtel",
              style: TextStyles.sp16(color: Palette.greyScaleDark7),
            ),
            value: "USA"),
        DropdownMenuItem(
            child: Text(
              "BSNL",
              style: TextStyles.sp16(color: Palette.greyScaleDark7),
            ),
            value: "Canada"),
        DropdownMenuItem(
            child: Text(
              "JIO",
              style: TextStyles.sp16(color: Palette.greyScaleDark7),
            ),
            value: "Brazil"),
      ],
      onChanged: (Object? value) {
        print(value);
      },
    );
  }

  Widget _searchBar() {
    return Container(
      height: 45,
      padding: const EdgeInsets.only(left: 0, right: 0),
      decoration: _searchBarDecoration(),
      child: Center(
        child: _searchBarTextField(),
      ),
    );
  }

  BoxDecoration _searchBarDecoration() {
    return BoxDecoration(
      color: Palette.colorPageBg,
      borderRadius: const BorderRadius.all(Radius.circular(8)),
      border: Border.all(
        color: Colors.transparent,
        width: 1,
      ),
    );
  }

  Widget _searchBarTextField() {
    return TextField(
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.text,
      style: TextStyles.sp16(fontWeight: FontWeight.w400),
      controller: globalSearchController,
      textAlign: TextAlign.left,
      decoration: _searchTextFieldDecoration(),
      onChanged: (searchString) {},
    );
  }

  InputDecoration _searchTextFieldDecoration() {
    return InputDecoration(
        prefixIcon: const Icon(Icons.search, size: 24),
        suffixIcon: const Icon(Icons.close, size: 24),
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        hintText: TranslationKeys.searchAnything.tr,
        hintStyle: TextStyles.sp16(
            fontWeight: FontWeight.w400, color: Palette.greyScaleDark6));
  }
}
