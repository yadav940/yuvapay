import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/services.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/home/global_search_page.dart';
import 'package:yuvapay/ui/views/home/notification_page.dart';
import 'package:yuvapay/ui/views/my_account/my_account_page.dart';
import 'package:yuvapay/ui/views/payment/scan_and_pay_page.dart';
import 'package:yuvapay/ui/views/prepaid/mobile_recharge_prepaid_page.dart';
import 'package:yuvapay/ui/views/prepaid/select_state_page.dart';
import 'package:yuvapay/ui/views/wallet/wallet_onboarding.dart';

import '../wallet/to_bank_page.dart';
import 'base_stateless_widget.dart';

export 'package:yuvapay/services/navigation.dart';

abstract class AppPage extends BaseStatelessWidget {
  dynamic get arguments {
    try {
      return Get.arguments['arguments'];
    } catch (e) {
      return null;
    }
  }

  Widget? get leadingWidget {
    return IconButton(
      visualDensity: VisualDensity.compact,
      splashRadius: 24,
      onPressed: Get.back,
      icon: const Icon(Icons.arrow_back_outlined),
      color: Colors.white,
      /*icon: SvgPicture.asset(AssetConstants.ic_toolbar_back,
            height: 16.8, width: 8.4, fit: BoxFit.scaleDown)*/
    );
  }

  String get title {
    try {
      return Get.arguments['title'];
    } catch (e) {
      return '';
    }
  }

  Widget? get titleWidget => null;

  List<Widget>? get actions => null;

  bool get centerTitle => false;

  Widget get defaultTitleWidget => Text(
        title,
        style: TextStyles.sp20(fontWeight: FontWeight.w700,color: Palette.colorPageBg),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      );

  //List<Widget>? get actions => modulePageActions;

  /*final List<Widget> moduleDashboardActions = [
    IconButton(
      onPressed: () {
        */ /*navigateTo(Constants.profile,
            currentPageTitle: TranslationKeys.home.tr);*/ /*
      },
      icon: SvgPicture.asset(
        AssetConstants.ic_user,
        key: HomePageController.userTabKey,
      ),
      splashRadius: 24,
    ),
    Badge(
      text: '2',
      child: IconButton(
        onPressed: () {
          navigateTo(Constants.announcementNotification,
              currentPageTitle: TranslationKeys.today.tr);
        },
        icon: SvgPicture.asset(
          AssetConstants.ic_announcement,
          key: HomePageController.announcementTabKey,
        ),
        splashRadius: 24,
      ),
    ),
  ];*/

  final List<Widget> modulePageActions = [
    IconButton(
      onPressed: () {},
      icon: Container() /*SvgPicture.asset(AssetConstants.ic_cross)*/,
      splashRadius: 24,
    )
  ];

  double? get toolbarHeight => null;

  bool get automaticallyImplyLeading => true;

  Color get actionBarContentColor => Palette.colorPageBg;
  Color get actionBarbackgroundColor => Palette.colorPrimary;

  Widget? get floatingActionButton => null;

  String? get heading => null;

  Widget? get headerAction => null;

  Widget simpleAddHeaderAction(String text, VoidCallback? onTap) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.translucent,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Icon(Icons.add, color: Palette.psMain),
            Text(text, style: TextStyles.sp14(color: Palette.psMain)),
          ],
        ),
      ),
    );
  }

  Widget get header {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
            child: Text(
              heading ?? '',
              style: TextStyles.pageTitle(color: Palette.greyScaleDark0),
            ),
          ),
        ),
        if (headerAction != null) headerAction!,
      ],
    );
  }


  Widget? get bottomNavBar => null;

  FloatingActionButtonLocation? get floatingActionButtonLocation => null;

  Future<bool> Function()? get onWillPop => null;
}

enum WidgetVisibility {
  Visible,
  Invisible,
}

class AppPageWithAppBar extends AppPage {
  PreferredSizeWidget? get appBar {
    return AppBar(
      leading: leadingWidget,
      automaticallyImplyLeading: automaticallyImplyLeading,
      iconTheme: IconThemeData(color: actionBarContentColor),
      elevation: 0,
      titleSpacing: 0,
      centerTitle: centerTitle,
      titleTextStyle: TextStyles.sp20(fontWeight: FontWeight.bold),
      backgroundColor:actionBarbackgroundColor,
      toolbarHeight: toolbarHeight,
      systemOverlayStyle: SystemUiOverlayStyle.dark
          .copyWith(statusBarColor: Colors.transparent),
      title: titleWidget ?? defaultTitleWidget,
      /*actions: actions
          ?.map((e) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4), child: e))
          .toList(),*/
    );
  }

  bool get resizeToAvoidBottomInset => true;

  bool get observeVisiblityChnages => false;

  Color get pageBackgroundColor => Palette.colorPageBg;

  void onResume() {}

  void onPause() {}

  Widget get body => Container();

  @override
  Widget build(BuildContext context) {
    Widget content = Scaffold(
      appBar: appBar,
      body: body,
      backgroundColor: pageBackgroundColor,
      floatingActionButton: floatingActionButton,
      floatingActionButtonLocation: floatingActionButtonLocation,
      bottomNavigationBar: bottomNavBar,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
    );

    return WillPopScope(
        child: observeVisiblityChnages
            ? FocusDetector(
                onFocusGained: () {
                  onResume();
                },
                onFocusLost: () {
                  onPause();
                },
                onVisibilityLost: () {
                  onPause();
                },
                child: content,
              )
            : content,
        onWillPop: onWillPop);
  }
}

class AppPageWithSliverAppBar extends AppPage {
  SliverAppBar get sliverAppBar {
    return SliverAppBar(
      snap: true,
      floating: true,
      leading: leadingWidget,
      iconTheme: IconThemeData(color: actionBarContentColor),
      backgroundColor: Colors.transparent,
      systemOverlayStyle: SystemUiOverlayStyle.dark
          .copyWith(statusBarColor: Colors.transparent),
      elevation: 0,
      titleSpacing: 0,
      centerTitle: centerTitle,
      toolbarHeight: toolbarHeight ?? kToolbarHeight,
      title: titleWidget ?? defaultTitleWidget,
      /*actions: actions
          ?.map((e) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4), child: e))
          .toList(),*/
    );
  }

  List<Widget> get slivers => [];

  @override
  Widget build(BuildContext context) {
    Widget content = Scaffold(
      body: CustomScrollView(slivers: [sliverAppBar, ...slivers]),
      floatingActionButton: floatingActionButton,
    );

    return WillPopScope(child: content, onWillPop: onWillPop);
  }
}

class AppPageWithAppAndBottomBar extends AppPage {
  PreferredSizeWidget? get appBar {
    return AppBar(
      leading: null,
      automaticallyImplyLeading: false,
      iconTheme: IconThemeData(color: actionBarContentColor),
      elevation: 0,
      titleSpacing: 0,
      centerTitle: centerTitle,
      backgroundColor: Palette.buttonBackground,
      systemOverlayStyle: SystemUiOverlayStyle.dark
          .copyWith(statusBarColor: Colors.transparent),
      title: _showAppTitle(),
      actions: [
        _showRewardsIcon(),
        const SizedBox(width: 12),
        _showNotificationsIcon(),
        const SizedBox(width: 12),
        _showSearchIcon(),
        const SizedBox(width: 12),
        _showMyAccountIcon(),
        const SizedBox(width: 20),
      ],
    );
  }

  Widget _showMyAccountIcon() {
    return Padding(
      padding: const EdgeInsets.only(left: 0),
      child: InkWell(
        onTap: () => Get.toNamed(MyAccountPage.routeName),
        child: SizedBox(
            height: 32,
            width: 32,
            child: Image.asset(AssetConstants.appBarProfile)),
      ),
    );
  }

  InkWell _showSearchIcon() {
    return InkWell(
      onTap: () => Get.toNamed(GlobalSearchPage.routeName),
      child: const SizedBox(
          child: Icon(Icons.search, size: 22, color: Palette.colorPageBg)),
    );
  }

  InkWell _showNotificationsIcon() {
    return InkWell(
      onTap: () => Get.toNamed(NotificationPage.routeName),
      child: const SizedBox(
        child: Icon(
          Icons.notifications_none,
          size: 22,
          color: Palette.colorPageBg,
        ),
      ),
    );
  }

  SizedBox _showRewardsIcon() {
    return SizedBox(
        height: 24,
        width: 24,
        child: Image.asset(AssetConstants.appBarReward));
  }

  Padding _showAppTitle() {
    return Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: SizedBox(height: 32,
        width: 44,
        child: Image.asset(AssetConstants.yuvaPayLogo),
        )

    );
  }

  ConvexAppBar? get bottomNavigationBar {
    return ConvexAppBar(
      backgroundColor: Palette.buttonBackground,
      height: 70,
      color: Colors.white,
      style: TabStyle.fixedCircle,
      curveSize: 110,
      activeColor: Palette.colorPageBg,
      items: [
        _bottomNavBarItem(TranslationKeys.home.tr, 0),
        _bottomNavBarItem(TranslationKeys.neoBank.tr, 1),
        const TabItem(icon: Icon(Icons.qr_code)),
        _bottomNavBarItem(TranslationKeys.wallet.tr, 2),
        _bottomNavBarItem(TranslationKeys.transfer.tr, 3),
      ],
      onTap: (index){
        debugPrint('nav tab $index');
        switch(index){
          case 0: Get.to(MobileRechargePrepaidPage());
            break;
          case 1:
            // Get.to(SelectStatePage());
            break;
          case 2:
            ScanAndPayPage.start();
            break;
          case 3: Get.to(WalletOnboardingPage());

            break;
          case 4:Get.to(ToBankPage());
            break;
        }

      },
      initialActiveIndex: 1,
    );
  }

  TabItem<SizedBox> _bottomNavBarItem(String title, int index) {
    return TabItem(
        icon: SizedBox(
          height: 20,
          width: 20,
          child: Image.asset('${AssetConstants.bottomNavBarIconBase}$index${AssetConstants.pngIconExtension}'),
        ),
        title: title);
  }

  bool get resizeToAvoidBottomInset => true;

  bool get observeVisiblityChnages => false;

  void onResume() {}

  void onPause() {}

  Widget get body => Container();

  @override
  Widget build(BuildContext context) {
    Widget content = Scaffold(
      appBar: appBar,
      body: body,
      backgroundColor: Palette.colorCardBg,
      floatingActionButton: floatingActionButton,
      floatingActionButtonLocation: floatingActionButtonLocation,
      bottomNavigationBar: bottomNavigationBar,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
    );

    return WillPopScope(
        child: observeVisiblityChnages
            ? FocusDetector(
                onFocusGained: () {
                  onResume();
                },
                onFocusLost: () {
                  onPause();
                },
                onVisibilityLost: () {
                  onPause();
                },
                child: content,
              )
            : content,
        onWillPop: onWillPop);
  }
}
