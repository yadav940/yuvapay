import 'package:yuvapay/ui/views/prepaid/select_operator_page.dart';
import 'package:yuvapay/ui/views/wallet/wallet_onboarding_otp_page.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';

class SelectStatePage extends AppPageWithAppBar {
  // static const routeName = "/wallet-onboarding";
  final TextEditingController _emailController = TextEditingController();

  @override
  // TODO: implement title
  String get title => TranslationKeys.completeYourKyc.tr;

  @override
  Widget get body {
    return SafeArea(
      child: _showBodyContent(),
    );
  }

  Widget _showBodyContent(){
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: AppCard(child: ListView.builder(
          itemCount: 8,
          itemBuilder: (BuildContext context, int index){
            return InkWell(
              onTap: (){
                Get.to(SelectOperatorPage());
              },
              child: const Padding(
                padding: EdgeInsets.only(bottom: 32),
                child: Text('Bengaluru'),
              ),
            );
          })),
    );
  }

}