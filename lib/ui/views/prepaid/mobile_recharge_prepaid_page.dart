import 'package:flutter_svg/svg.dart';
import 'package:yuvapay/ui/views/prepaid/mobile_prepaid_search_page.dart';
import 'package:yuvapay/ui/views/wallet/transfer_amount_page.dart';
import 'package:yuvapay/ui/views/wallet/wallet_onboarding_otp_page.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import '../../resources/asset_ constants.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';
import '../onboarding/upi/transfer_to_upi/upi_pay_page.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';

class MobileRechargePrepaidPage extends AppPageWithAppBar {
  // static const routeName = "/wallet-onboarding";
  final TextEditingController _searchController = TextEditingController();
  RxBool _isSearch = false.obs;
  static const routeName = "/MobileRechargePrepaidPage";

  static void start({String? page}) {
    Get.toNamed(MobileRechargePrepaidPage.routeName,arguments: page);
    //Get.to(MobileRechargePrepaidPage());
  }

  @override
  //String get title => TranslationKeys.sendViaPhoneNumber.tr;
  String get title => Get.arguments??'Mobile Recharge - Prepaid';


  @override
  Widget get body {
    return SafeArea(
      child: _showBodyContent(),
    );
  }

  Widget _showBodyContent() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          children: [
            AppCard(
              child: AppTextField(
                textController: _searchController,
                prefixIcon: InkWell(
                    onTap: (){
                      Get.to(MobilePrepaidSearchPage());
                    },
                    child: const Icon(Icons.search)),
                hintText: TranslationKeys.searchSavedBankAccounts.tr,
                onChange: (String s) {
                  print(s);
                  print(s.length);
                  if (s.length > 0) {
                    _isSearch = true.obs;
                  } else {
                    _isSearch = false.obs;
                  }
                  print(_isSearch.value);
                },
                suffixIcon: Obx(() => _isSearch.value
                    ? InkWell(onTap: () {}, child: const Icon(Icons.close))
                    : const SizedBox(height: 0)),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            AppCard(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Obx(() {
                    return _isSearch.value
                        ? const SizedBox(height: 0)
                        : const Text('RECENT SEARCHES');
                  }),
                  sizedBox,
                  InkWell(
                    onTap: () {},
                    // onTap: UpiPayPage.start,
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          AssetConstants.icSbiBank,
                          height: 40,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Mira Jahan',
                              style:
                                  TextStyles.sp16(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              '9999999999',
                              style:
                                  TextStyles.sp14(fontWeight: FontWeight.w400),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  sizedBox,
                  InkWell(
                    onTap: UpiPayPage.start,
                    child: Row(
                      children: [
                        SvgPicture.asset(AssetConstants.icUpiUser),
                        const SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Cheteshwari C Rao',
                              style:
                                  TextStyles.sp16(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              '9999999999',
                              style:
                                  TextStyles.sp14(fontWeight: FontWeight.w400),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            sizedBox,
            AppCard(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('ALL CONTACTS'),
                  sizedBox,
                  InkWell(
                    onTap: UpiPayPage.start,
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          AssetConstants.icSbiBank,
                          height: 40,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Mira Jahan',
                              style:
                                  TextStyles.sp16(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              '9999999999',
                              style:
                                  TextStyles.sp14(fontWeight: FontWeight.w400),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  sizedBox,
                  InkWell(
                    onTap: UpiPayPage.start,
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          AssetConstants.icSbiBank,
                          height: 40,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Mira Jahan',
                              style:
                                  TextStyles.sp16(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              '9999999999',
                              style:
                                  TextStyles.sp14(fontWeight: FontWeight.w400),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  sizedBox,
                  InkWell(
                    onTap: UpiPayPage.start,
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          AssetConstants.icSbiBank,
                          height: 40,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Mira Jahan',
                              style:
                                  TextStyles.sp16(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              '9999999999',
                              style:
                                  TextStyles.sp14(fontWeight: FontWeight.w400),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  sizedBox,
                  InkWell(
                    onTap: UpiPayPage.start,
                    child: Row(
                      children: [
                        SvgPicture.asset(AssetConstants.icUpiUser),
                        const SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Cheteshwari C Rao',
                              style:
                                  TextStyles.sp16(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              '9999999999',
                              style:
                                  TextStyles.sp14(fontWeight: FontWeight.w400),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget get sizedBox {
    return const SizedBox(
      height: 16,
    );
  }
}
