import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yuvapay/controller/onboarding/language_controller.dart';
import 'package:yuvapay/services/navigation.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/onboarding/welcome_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/utils/network_status.dart';

import '../base/base.dart';

class LanguagePage extends AppPageWithAppBar {
  static const routeName = "/language";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(LanguageController());

  @override
  double? get toolbarHeight => 0;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: ListView(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //appTitle,
            SvgPicture.asset(AssetConstants.icSpeechBubbles),
            appTitle,
            const SizedBox(
              height: 26,
            ),
            Obx(() => getLanguageList),
            const SizedBox(
              height: 26,
            ),
            appButton
          ],
        ),
      ),
    );
  }

  Widget get appTitle {
    return Text(
      TranslationKeys.select_your_language.tr,
      style: TextStyles.pageTitle(
          fontWeight: FontWeight.bold, color: Palette.colorPrimaryTextG90),
      textAlign: TextAlign.center,
    );
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.button_contnue.tr,
      onPressed: () {
        if (controller.selectedLanguage.value.isNotEmpty) {
          Get.toNamed(WelcomePage.routeName);
        } else {
          ScaffoldMessenger.of(Get.context!).showSnackBar(
            SnackBar(
              content: Text(TranslationKeys.pleaseSelectLanguage.tr),
            ),
          );

          // Fluttertoast.showToast(msg: 'Please select language',);
        }
      },
    );
  }

  Widget get getLanguageList {
    Widget widget;
    switch (controller.networkStatus.value) {
      case NetworkStatus.LOADING:
        widget = const Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.red,
            strokeWidth: 2,
          ),
        );
        break;
      case NetworkStatus.ERROR:
        widget = const Center(child: Text('Error ....'));
        break;
      case NetworkStatus.SUCCESS:
        widget = GridView.builder(
          shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: (MediaQuery.of(Get.context!).orientation == Orientation.portrait) ? 3 : 5),
          itemCount: controller.languageList.length,
          itemBuilder: (context, i) {
            return getLanguage(i, controller.languageList[i]);
          },
        );
        break;
      default:
        widget = Container();
    }

    return widget;
  }

  Widget getLanguage(int index, String languageText) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 6),
      child: InkWell(
        onTap: () {
          controller.selectLanguage(languageText, index);
        },
        child: Obx(() => Container(
            decoration: BoxDecoration(
              borderRadius : const BorderRadius.all(Radius.circular(12) ),
              color : (languageText == controller.selectedLanguage.value)?Color.fromRGBO(237, 248, 236, 1):Colors.transparent,
              border : Border.all(
                color: Palette.colorBorders2,
                width: 1,
              ),
            ),
              child: Column(
                children: [
                  languageText == controller.selectedLanguage.value?Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0,right: 14),
                          child: SvgPicture.asset(
                            'assets/images/svg/ic_Icon_selected_trick.svg',
                            semanticsLabel: 'icon',
                            color: Palette.colorBorders2,
                          ),
                        )
                      ],
                    ):const SizedBox(height: 28,),
                  const SizedBox(height: 8,),
                  Text(
                    languageText,
                    style:  TextStyles.sp16(
                            fontWeight: FontWeight.w500)

                  ),
                  const SizedBox(height: 8,),
                  Text(
                    languageText,
                    style: TextStyles.sp14(
                            color: Palette.colorPrimaryTextG70,),
                  ),

                ],
              ),
            )),
      ),
    );
  }
}
