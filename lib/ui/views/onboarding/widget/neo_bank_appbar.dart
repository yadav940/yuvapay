import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/page.dart';

import 'neo_bank_title.dart';

class NeoBankAppBar extends AppPageWithAppBar {
  @override
  String get title => TranslationKeys.neoBank.tr;

  @override
  bool get centerTitle => true;

  Widget get defaultTitleWidget => NeoBankTitle();

  @override
  List<Widget>? get actions => [
        SvgPicture.asset(AssetConstants.icDirection),
        const SizedBox(
          width: 16,
        )
      ];
}
