import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';

class NeoBankInfoList extends StatelessWidget {
  const NeoBankInfoList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return infoList;
  }

  Widget get infoList {
    return Column(
      children: [
        infoItem(
          TranslationKeys.sbAccoun7Interest.tr,
          TranslationKeys.letYourMoneyMakeMoney.tr,
          AssetConstants.icFluentMoney,
        ),
        infoItem(
          TranslationKeys.zeroMinimumBalance.tr,
          TranslationKeys.wedonWantYouWorryAboutLowCalanceCharges.tr,
          AssetConstants.icHandZero,
        ),
        infoItem(
          TranslationKeys.freeOnlineDebitCard.tr,
          TranslationKeys.forEasyyShoppingExperience.tr,
          AssetConstants.icCard,
        ),
      ],
    );
  }

  Widget infoItem(String titleText, String detail, String imgPath) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: Row(
        children: [
          SvgPicture.asset(
            imgPath,
            semanticsLabel: 'icon',
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  titleText,
                  style: TextStyles.sp16(),
                ),
                Text(
                  detail,
                  style: TextStyles.sp14(
                    color: Palette.colorPrimaryTextG70,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
