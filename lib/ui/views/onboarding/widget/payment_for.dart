import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';

class UpiPaymentFor extends StatelessWidget {
  const UpiPaymentFor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return paymentFor;
  }

  Widget get paymentFor {
    return Column(
      children: [
        paymentItem(TranslationKeys.simpleOnlinePayments,
            AssetConstants.ic_simple_online_payment),
        paymentItem(TranslationKeys.advancedSecurity.tr,
            AssetConstants.ic_secure_outer),
        paymentItem(
            TranslationKeys.cashlessEconomy.tr, AssetConstants.ic_cashless),
      ],
    );
  }

  Widget paymentItem(String text, String imgPath) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: Row(
        children: [
          SvgPicture.asset(
            imgPath,
            semanticsLabel: 'icon',
          ),
          Text(
            text,
            style: TextStyles.sp16(
                color: Palette.colorPrimaryTextG90,
                fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  }
}
