import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';

class WalletInfoList extends StatelessWidget {
  const WalletInfoList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return walletInfoList;
  }

  Widget get walletInfoList {
    return Column(
      children: [
        walletInfoListItem(TranslationKeys.instantPayments.tr,
            AssetConstants.icInstantPayments),
        walletInfoListItem(TranslationKeys.offlineTransactions.tr,
            AssetConstants.icOfflineTransactions),
        walletInfoListItem(
            TranslationKeys.rewardsBenifits.tr, AssetConstants.icRewards),
      ],
    );
  }

  Widget walletInfoListItem(String text, String imgPath) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: Row(
        children: [
          SvgPicture.asset(
            imgPath,
            semanticsLabel: 'icon',
          ),
          Text(
            text,
            style: TextStyles.sp16(
                color: Palette.colorPrimaryTextG90,
                fontWeight: FontWeight.normal),
          ),
        ],
      ),
    );
  }
}
