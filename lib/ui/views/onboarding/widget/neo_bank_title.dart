
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';

class NeoBankTitle extends StatefulWidget {
  const NeoBankTitle({Key? key}) : super(key: key);

  @override
  _NeoBankTitleState createState() => _NeoBankTitleState();
}

class _NeoBankTitleState extends State<NeoBankTitle> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(AssetConstants.icYuvaBank),
        const SizedBox(width: 8,),
        Text(TranslationKeys.neoBank.tr,style: TextStyles.sp20(fontWeight: FontWeight.w700,color: Palette.colorPageBg),)
      ],
    );
  }
}
