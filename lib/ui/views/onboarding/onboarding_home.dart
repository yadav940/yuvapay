import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:yuvapay/controller/onboarding/onboarding_home_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/home/home_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_text_field.dart';
import 'package:yuvapay/ui/views/widgets/popup_utils.dart';

import '../base/base.dart';

class OnboardingHome extends AppPageWithAppBar {
  static const routeName = "/onboarding_home";

  static void start() {
    //navigateOffAll(routeName);
    Get.toNamed(OnboardingHome.routeName);
  }

  final controller = Get.put(OnboardingHomeController());
  final referralController = TextEditingController();

  @override
  double? get toolbarHeight => 0;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(
              height: 14,
            ),
            Lottie.asset(AssetConstants.animWelcomeToYuvaPay,
                height: 240, repeat: false),
            SvgPicture.asset(AssetConstants.icLogoWithYuvapay),
            const SizedBox(
              width: 4,
            ),
            Obx(
              () => InkWell(
                onTap: referralPopup,
                child: Padding(
                    padding: const EdgeInsets.all(26.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        if (controller.applyReferral.value)
                          SvgPicture.asset(
                            'assets/images/svg/ic_Icon_selected_trick.svg',
                            semanticsLabel: 'icon',
                            color: Palette.buttonBackground,
                          ),
                        const SizedBox(
                          width: 4,
                        ),
                        Text(
                          controller.applyReferral.value
                              ? TranslationKeys.referral_code_applied.tr
                              : TranslationKeys.have_a_referral_code.tr,
                          style: TextStyles.sp14(
                              fontWeight: FontWeight.w500,
                              color: Palette.buttonBackground),
                        ),
                      ],
                    )),
              ),
            ),
            AppButton(
              text: TranslationKeys.take_me_to_home.tr,
              onPressed: HomePage.start,
            )
          ],
        ),
      ),
    );
  }

  void referralPopup() {
    debugPrint('-----');
    showAppBottomSheet(
        child: getReferralPopup,
        dragPickColor: Colors.transparent,
        initialChildSize: 0.85);
    referralController.notifyListeners();
  }

  Widget get getReferralPopup {
    return Padding(
      padding: const EdgeInsets.only(left: 12.0, right: 12.0),
      child: Column(
        children: [
          SvgPicture.asset('assets/images/svg/ic_referral.svg'),
          const SizedBox(
            height: 24,
          ),
          Text(
            TranslationKeys.add_referral_code.tr,
            style: TextStyles.pageTitle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            TranslationKeys
                .enter_invite_code_to_reward_referral_bonus_to_your_friends.tr,
            style: TextStyles.sp14(),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 40,
          ),
          Row(
            children: [
              Flexible(
                flex: 3,
                child: AppTextField(
                  hintText: TranslationKeys.enter_referral_code.tr,
                  textController: referralController,
                  autofocus: true,
                  onChange: (text) {},
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(6),
                  ],
                  borderColor: controller.isError.value
                      ? Palette.colorError
                      : Palette.greyScaleDark7,
                ),
              ),
              const SizedBox(
                width: 12,
              ),
              Flexible(
                flex: 2,
                child: AppButton(
                    text: TranslationKeys.apply.tr,
                    onPressed: controller.getApplyReferral),
              )
            ],
          )
        ],
      ),
    );
  }
}
