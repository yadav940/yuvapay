import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/onboarding/wallete/wallete_pan_detail_page.dart';
import 'package:yuvapay/ui/views/onboarding/widget/wallet_info_list.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';

class WalleteActivatePage extends AppPageWithAppBar {
  static const routeName = "/wallete_activate_page";

  static void start() {
    //navigateOffAll(routeName);
    Get.toNamed(WalleteActivatePage.routeName);
  }

  //final controller = Get.put(PhoneNumberAndEmailOTPController());

  @override
  String get title => TranslationKeys.wallete.tr;


  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTitle(
                  titleText: TranslationKeys.activateYourWallet.tr,
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  TranslationKeys.yourWalletIsCurrentlyInactive.tr,
                  style: TextStyles.sp16(fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 32,
                ),
                const WalletInfoList(),
                const SizedBox(
                  height: 26,
                ), //
              ],
            ),
            appButton
          ],
        ),
      ),
    );
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.activateIn2minutes.tr,
      onPressed: WalletePanDetailPage.start,
    );
  }
}
