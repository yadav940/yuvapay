import 'package:get/get.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:yuvapay/controller/onboarding/phone_number_and_email_otp_controller.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/onboarding/wallete/wallete_success_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';

class WalleteOtpPage extends AppPageWithAppBar {
  static const routeName = "/wallete_otp_page";

  static void start() {
    //navigateOffAll(routeName);
    debugPrint(routeName);
    Get.toNamed(WalleteOtpPage.routeName);
  }

  final controller = Get.put(PhoneNumberAndEmailOTPController());

  @override
  String get title => TranslationKeys.wallete.tr;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTitle(
                  titleText: TranslationKeys.one_time_password.tr,
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  TranslationKeys.we_have_sent_you_an_code_to_your_number.tr,
                  style: TextStyles.sp16(fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 16,
                ),
                numberWidget,
                const SizedBox(
                  height: 40,
                ),

                textField,

                const SizedBox(
                  height: 8,
                ),
                errorText,

                const SizedBox(
                  height: 24,
                ),
                reSendCode,
                const SizedBox(
                  height: 26,
                ), //
              ],
            ),
            appButton
          ],
        ),
      ),
    );
  }

  Widget get numberWidget {
    return Text(
      '948 *** **04',
      style: TextStyles.sp16(fontWeight: FontWeight.bold),
    );
  }

  Widget get textField {
    return PinCodeTextField(
      maxLength: 6,
      autofocus: true,
      highlightColor: Colors.red,
      defaultBorderColor: Palette.greyScaleDark6,
      hasTextBorderColor: Palette.psMain,
      pinBoxRadius: 12,
      pinBoxHeight: 56,
      pinBoxWidth: 48,
      keyboardType: TextInputType.number,
      controller: controller.textController,
      onTextChanged: (_) {
        debugPrint('-------------$_');
      },
    );
  }

  Widget get errorText {
    return Obx(
      () => Text(
        controller.errorMessage.value,
        style: TextStyles.sp12(color: Palette.colorError),
      ),
    );
  }

  Text get reSendCode => Text(
        TranslationKeys.resendOTP.tr,
        style: TextStyles.sp14(
            fontWeight: FontWeight.w500, color: Palette.buttonBackground),
      );

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.button_contnue.tr,
      onPressed: WalleteSuccessfullyActivatedPage.start,
    );
  }
}
