//
import 'package:get/get.dart';
import 'package:yuvapay/controller/onboarding/wallete/wallete_pan_detail_controller.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/onboarding/wallete/wallete_otp_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_text_field.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';
import 'package:yuvapay/utils/enums.dart';

class WalletePanDetailPage extends AppPageWithAppBar {
  static const routeName = "/wallete_pan_detail_pagel";

  static void start() {
    //Get.offAll(PhoneNumberAndEmailPage(),arguments: page);
    Get.toNamed(
      WalletePanDetailPage.routeName,
    );
  }

  final controller = Get.put(WalletePanDetailController());

  @override
  String get title => TranslationKeys.wallete.tr;

  @override
  Widget get body {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTitle(
                    titleText: TranslationKeys.enterPANCardDetails.tr,
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  textlable(TranslationKeys.namePANcard.tr),
                  const SizedBox(
                    height: 4,
                  ),
                  nameTextField,
                  const SizedBox(
                    height: 40,
                  ),
                  textlable(TranslationKeys.namePANcard.tr),
                  const SizedBox(
                    height: 4,
                  ),
                  panTextField,
                  const SizedBox(
                    height: 8,
                  ),
                  errorText,

                  const SizedBox(
                    height: 26,
                  ), //
                ],
              ),
              appButton
            ],
          ),
        ),
      ),
    );
  }

  Widget textlable(String data) {
    return Text(
      data,
      style: TextStyles.sp12(),
    );
  }

  Widget get nameTextField {
    return Obx(() => AppTextField(
          hintText: TranslationKeys.enterName.tr,
          textController: TextEditingController(),
          onChange: controller.onChangeText,
          autofocus: true,
          inputType: TextInputType.text,
          borderColor: controller.isError.value
              ? Palette.colorError
              : Palette.greyScaleDark7,
        ));
  }

  Widget get panTextField {
    return Obx(() => AppTextField(
          hintText: TranslationKeys.enterYourPANNumber.tr,
          textController: TextEditingController(),
          onChange: controller.onChangeText,
          autofocus: true,
          inputType: TextInputType.text,
          borderColor: controller.isError.value
              ? Palette.colorError
              : Palette.greyScaleDark7,
        ));
  }

  Widget get errorText {
    return Obx(() => Text(
          controller.errorMessage.value,
          style: TextStyles.sp12(color: Palette.colorError),
        ));
  }

  Widget get appButton {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if (Get.arguments == PhoneNumberEmail.email)
          Padding(
            padding: const EdgeInsets.all(28.0),
            child: Text(
              TranslationKeys.continue_with_logged_in_account.tr,
              style: TextStyles.sp14(
                  color: Palette.buttonBackground, fontWeight: FontWeight.w500),
            ),
          ),
        AppButton(
          text: TranslationKeys.button_contnue.tr,
          onPressed: WalleteOtpPage.start,
        )
      ],
    );
  }
}
