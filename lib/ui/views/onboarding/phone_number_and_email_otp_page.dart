import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:yuvapay/controller/onboarding/phone_number_and_email_otp_controller.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';
import 'package:yuvapay/utils/enums.dart';

import '../base/base.dart';

class PhoneNumberAndEmailOTPPage extends AppPageWithAppBar {
  static const routeName = "/phone_number_and_email_otp";

  static void start(PhoneNumberEmail page) {
    //navigateOffAll(routeName);
    Get.toNamed(PhoneNumberAndEmailOTPPage.routeName, arguments: page);
  }

  final controller = Get.put(PhoneNumberAndEmailOTPController());

  @override
  double? get toolbarHeight => 0;

  @override
  Widget get body {
    debugPrint('----------on PhoneNumberAndEmailOTPPage');

    ///controller.initTimer.value=30;
    controller.startTimer();
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTitle(
                    titleText: Get.arguments == PhoneNumberEmail.phoneNumber
                        ? TranslationKeys.one_time_password.tr
                        : TranslationKeys.mail_verification_code.tr,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    Get.arguments == PhoneNumberEmail.phoneNumber
                        ? TranslationKeys
                            .we_have_sent_you_an_code_to_your_number.tr
                        : TranslationKeys
                            .we_have_sent_you_a_verification_code_to_your_email_address
                            .tr,
                    style: TextStyles.sp16(fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  editWidget,
                  const SizedBox(
                    height: 40,
                  ),

                  textField,

                  const SizedBox(
                    height: 8,
                  ),
                  errorText,

                  const SizedBox(
                    height: 24,
                  ),
                  reSendCode,
                  const SizedBox(
                    height: 26,
                  ), //
                ],
              ),
              appButton
            ],
          ),
        ),
      ),
    );
  }

  Widget get editWidget {
    return Row(
      children: [
        Text(
          Get.arguments == PhoneNumberEmail.phoneNumber
              ? '948 214 0204'
              : 'abc@gmail.com',
          style: TextStyles.sp16(fontWeight: FontWeight.bold),
        ),
        const Padding(
          padding: EdgeInsets.only(left: 6.0, right: 6.0),
          child: Divider(
            color: Palette.colorBorderG10,
            thickness: 1,
            height: 20,
          ),
        ),
        InkWell(
          onTap: () {
            Get.back();
          },
          child: Text(
            TranslationKeys.edit.tr,
            style: TextStyles.sp14(
                color: Palette.buttonBackground, fontWeight: FontWeight.w500),
          ),
        )
      ],
    );
  }

  Widget get textField {
    return PinCodeTextField(
      //appContext: Get.context!,
      maxLength: 6,
      //showCursor: false,
      autofocus: true,
      highlightColor: Colors.red,
      defaultBorderColor: Palette.greyScaleDark6,
      hasTextBorderColor: Palette.psMain,
      pinBoxRadius: 12,
      pinBoxHeight: 56,
      pinBoxWidth: 48,
      // pinBoxBorderWidth: 48,
      //textStyle: TextStyles.sp12(),
      //  cursorColor: Palette.greyScaleDark7,
      //backgroundColor: Palette.greyScaleDark6,
      keyboardType: TextInputType.number,
      controller: controller.textController,
      onTextChanged: (_) {
        debugPrint('-------------$_');
      },
    );
  }

  Widget get errorText {
    return Obx(() => Text(
          controller.errorMessage.value,
          style: TextStyles.sp12(color: Palette.colorError),
        ));
  }

  Widget get reSendCode => Obx(() => Text(
        controller.initTimer.value == 0
            ? TranslationKeys.resend_verification_code.tr
            : 'Resend OTP in ${controller.initTimer.value} seconds',
        style: TextStyles.sp14(
          fontWeight: FontWeight.w500,
          color: controller.initTimer.value == 0
              ? Palette.buttonBackground
              : Palette.colorPrimaryTextG50,
        ),
      ));

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.button_contnue.tr,
      onPressed: controller.next,
    );
  }
}
