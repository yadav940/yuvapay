//
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:yuvapay/controller/onboarding/upi/select_sim_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/home/home_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_radio_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';

import 'addbank/addbank_page.dart';

class UpiSuccessfullyActivatedPage extends AppPageWithAppBar {
  static const routeName = "/upi_successfully_activated_page";

  static void start() {
    //navigateOffAll(routeName);
    Get.toNamed(UpiSuccessfullyActivatedPage.routeName);

    //Navigator.push(Get.context!, MaterialPageRoute(builder: (context) => SuccessfullyActivatedPage()));
  }

  final controller = Get.put(SelectSimController());

  @override
  String get title => TranslationKeys.upi.tr;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Lottie.asset(AssetConstants.animSuccessUpiActivate,repeat: false),
            const SizedBox(
              height: 24,
            ),
            AppTitle(
              titleText: TranslationKeys.upiSuccessfullyActivated.tr,
            ),
            const SizedBox(
              height: 46,
            ),
            appButton,
            const SizedBox(
              height: 16,
            ),
            InkWell(
              onTap: HomePage.start,
              child: Text(
                TranslationKeys.backToHome.tr,
                style: TextStyles.sp16(
                    fontWeight: FontWeight.w400,
                    color: Palette.colorPrimaryTextG50),
              ),
            ),
            //const UpiPaymentFor(),
            //
          ],
        ),
      ),
    );
  }

  Widget simContainer(
      {required String textSim,
      required String path,
      required String simName,
      required bool isSelected}) {
    return Column(
      children: [
        Container(
          height: 88,
          width: 88,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(12)),
            color: isSelected ? Palette.colorBorderG10 : Palette.colorPageBg,
            border: Border.all(
              color: Palette.colorBorderG10,
              width: 1,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(path),
              Text(textSim),
            ],
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            AppRadioButton(isSelected),
            const SizedBox(
              width: 6,
            ),
            Text(
              simName,
              style: TextStyles.sp14(),
            )
          ],
        )
      ],
    );
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.linkYourBankAccount.tr,
      onPressed: AddbankPage.start,
    );
  }
}
