//
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/controller/onboarding/add_bank/add_bank_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/box_decorations.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/widgets/app_loading.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/app_text_field.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';
import 'package:yuvapay/utils/network_status.dart';

class AddbankPage extends AppPageWithAppBar {
  static const routeName = "/upi_addbank_page";

  static void start() {
    //navigateOffAll(routeName);
    Get.toNamed(AddbankPage.routeName);

    //Navigator.push(Get.context!, MaterialPageRoute(builder: (context) => SuccessfullyActivatedPage()));
  }

  final controller = Get.put(AddBankController());

  @override
  String get title => TranslationKeys.addBank.tr;

  @override
  Color get pageBackgroundColor => Palette.dividerColor;

  @override
  Widget get body {
    return SafeArea(
      child: Obx(()=>appBody),
    );
  }

  Widget get appBody {
    Widget widget =Container();
    switch(controller.networkStatus.value){
      case NetworkStatus.ERROR:
        widget=Text('Error...');
        break;
      case NetworkStatus.LOADING:
        widget=AppLoading();
        break;
      case NetworkStatus.SUCCESS:
        widget= appBody2;
        break;
    }
    return widget;
  }

  Widget get appBody2 {
    return AppPadding(
    child: ListView(
      children: [
        const SizedBox(
          height: 16,
        ),
        _searchWidget,
        Obx(
          () => SizedBox(
            height: controller.isSearch.value ? 0 : 12,
          ),
        ),
        Obx(() => getBankGrid),
        const SizedBox(
          height: 12,
        ),
        getBankList
      ],
    ),
  );
  }

  Widget get _searchWidget {
    return Container(
      decoration: BoxDecorations.decorationWiteCard,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Obx(
        () => AppTextField(
          textController: controller.searchController,
          prefixIcon: Padding(
            padding: const EdgeInsets.only(top: 12.0, bottom: 12),
            child: SvgPicture.asset(
              AssetConstants.icSearch,
              height: 10,
              width: 10,
            ),
          ),
          suffixIcon: controller.isSearch.value
              ? InkWell(
                  onTap: controller.searchCancel,
                  child: const RotationTransition(
                    turns: AlwaysStoppedAnimation(45 / 360),
                    child: Icon(Icons.add),
                  ),
                )
              : null,
          onChange: controller.onChange,
        ),
      ),
    );
  }

  Widget get getBankGrid {
    return controller.isSearch.value
        ? Container()
        : Container(
            decoration: BoxDecorations.decorationWiteCard,
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (!controller.isSearch.value)
                  AppTitle(titleText: TranslationKeys.popularBanks.tr),
                if (!controller.isSearch.value)
                  const SizedBox(
                    height: 24,
                  ),
                GridView.count(
                  crossAxisCount: 4,
                  crossAxisSpacing: 0.0,
                  mainAxisSpacing: 0.0,
                  shrinkWrap: true,
                  //childAspectRatio: 0.7,
                  physics: const ClampingScrollPhysics(),
                  children: List.generate(
                    8,
                    (index) {
                      return _gridViewItem('SBI', AssetConstants.icSbiBank);
                    },
                  ),
                )
              ],
            ),
          );
  }

  Widget get getBankList {
    return Container(
      decoration: BoxDecorations.decorationWiteCard,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Obx(
            () => controller.isSearch.value
                ? Container()
                : Column(
                    children: [
                      AppTitle(titleText: TranslationKeys.allBank.tr),
                      const SizedBox(
                        height: 24,
                      ),
                    ],
                  ),
          ),
          ListView.builder(
            itemCount: 4,
            shrinkWrap: true,
            //childAspectRatio: 0.7,
            physics: const ClampingScrollPhysics(),
            itemBuilder: (c, i) {
              return _listViewItem('Canara Bank', AssetConstants.icSbiBank);
            },
          )
        ],
      ),
    );
  }

  Widget _gridViewItem(String title, String assetPath) {
    return InkWell(
      onTap: controller.onTabBank,
      child: Padding(
        padding: const EdgeInsets.all(6.0),
        child: Column(
          children: [
            SizedBox(
                height: 32,
                width: 32,
                child: SvgPicture.asset(
                  assetPath,
                  //scale: scale,
                )),
            const SizedBox(height: 8),
            Text(
              title,
              style: TextStyles.sp14(fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  Widget _listViewItem(String title, String assetPath) {
    return InkWell(
      onTap: controller.onTabBank,
      child: Padding(
        padding:
            const EdgeInsets.only(left: 8.0, right: 8.0, top: 12, bottom: 12),
        child: Row(
          children: [
            SizedBox(
                height: 32,
                width: 32,
                child: SvgPicture.asset(
                  assetPath,
                  //scale: scale,
                )),
            const SizedBox(width: 8),
            Text(
              title,
              style: TextStyles.sp16(fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
