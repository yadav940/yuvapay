
import 'package:flutter_svg/svg.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/box_decorations.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/onboarding/upi/addbank/addbank_success_page.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';

class AddbankOtherBank extends AppPageWithAppBar{

  static const routeName = "/upi_addbank_other_bank_page";

  static void start() {
    //navigateOffAll(routeName);
    Get.toNamed(AddbankOtherBank.routeName);

    //Navigator.push(Get.context!, MaterialPageRoute(builder: (context) => SuccessfullyActivatedPage()));
  }

  //final controller = Get.put(AddBankController());

  @override
  String get title => TranslationKeys.addBank.tr;

  @override
  Color get pageBackgroundColor => Palette.dividerColor;

  @override
  Widget get body {
    return SafeArea(
      child: AppPadding(
        child: ListView.builder(
          itemCount: 4,
          itemBuilder: (c,i){
            return Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: _listViewItem('Canara Bank', AssetConstants.icSbiBank),
            );
          },

        ),
      ),
    );
  }


  Widget _listViewItem(String title, String assetPath) {
    return Container(
      decoration: BoxDecorations.decorationWiteCard,
      padding: const EdgeInsets.all(4),
      child: InkWell(
        onTap: AddBankSuccessPage.start,
        child: Padding(
          padding:
          const EdgeInsets.only(left: 8.0, right: 8.0, top: 12, bottom: 12),
          child: Row(
            children: [
              SizedBox(
                  height: 32,
                  width: 32,
                  child: SvgPicture.asset(
                    assetPath,
                    //scale: scale,
                  )),
              const SizedBox(width: 8),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyles.sp16(fontWeight: FontWeight.w500),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 4,),
                  Text(
                    'Saving Account',
                    style: TextStyles.sp14(fontWeight: FontWeight.normal),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    'XXX 4343',
                    style: TextStyles.sp14(fontWeight: FontWeight.normal),
                    textAlign: TextAlign.center,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }


}