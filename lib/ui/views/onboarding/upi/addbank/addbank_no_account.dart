import 'dart:ui';

import 'package:yuvapay/controller/onboarding/add_bank/add_bank_controller.dart';
import 'package:yuvapay/ui/resources/box_decorations.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';

import 'addbank_other_bank.dart';

class AddbankNoAccount extends AppPageWithAppBar {
  static const routeName = "/upi_addbank_no_account_page";

  static void start() {
    //navigateOffAll(routeName);
    debugPrint('-----------AddbankNoAccount');
    Get.toNamed(AddbankNoAccount.routeName);

    //Navigator.push(Get.context!, MaterialPageRoute(builder: (context) => SuccessfullyActivatedPage()));
  }

  final controller = Get.put(AddBankController());

  @override
  String get title => TranslationKeys.addBank.tr;

  @override
  Color get pageBackgroundColor => Palette.dividerColor;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          padding: const EdgeInsets.all(32.0),
          height: 200,
          decoration: BoxDecorations.decorationWiteCard,
          child: Column(
            children: [

              Text(TranslationKeys.addbankNoNumberRegisterWithBank.tr, style: TextStyles.sp16(),textAlign: TextAlign.center,),
              const SizedBox(
                height: 24,
              ),
              AppButton(
                text: TranslationKeys.tryOtherBank.tr,
                onPressed: AddbankOtherBank.start
              )
            ],
          ),
        ),
      ),
    );
  }
}
