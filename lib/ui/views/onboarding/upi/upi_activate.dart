import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/select_sim_page.dart';
import 'package:yuvapay/ui/views/onboarding/widget/payment_for.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';

class UpiActivatePage extends AppPageWithAppBar {
  static const routeName = "/upi_activate_page";

  static void start() {
    //navigateOffAll(routeName);
    Get.toNamed(UpiActivatePage.routeName);
  }

  //final controller = Get.put(PhoneNumberAndEmailOTPController());

  @override
  String get title => TranslationKeys.upi.tr;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTitle(
                  titleText: TranslationKeys.activateUpiTransactions.tr,
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  TranslationKeys.nowYouCanLinkYourBankAccountToMakePayments.tr,
                  style: TextStyles.sp16(fontWeight: FontWeight.w400),
                ),
                const SizedBox(
                  height: 32,
                ),
                const UpiPaymentFor(),
                const SizedBox(
                  height: 26,
                ), //
              ],
            ),
            Column(
              children: [
                const PoweredByYesBankBhim(),
                const SizedBox(height: 16),
                appButton
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.activateIn2minutes.tr,
      onPressed: () {
        SelectSimPage.start();
      },
    );
  }
}
