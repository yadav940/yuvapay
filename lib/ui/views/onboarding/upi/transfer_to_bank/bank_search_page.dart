
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/app_text_field.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';

import '../../../../../controller/onboarding/upi/upi_search_controller.dart';
import '../transfer_to_upi/upi_pay_page.dart';
import 'add_new_beneficiary.dart';

class BankTransferSearchPage extends AppPageWithAppBar {
  static const routeName = "/bank_search_page";

  static void start() {
    Get.toNamed(BankTransferSearchPage.routeName);
  }

  final controller = Get.put(UpiSearchController());

  @override
  String get title => TranslationKeys.bankTransfer.tr;

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  @override
  Widget get body {
    return SafeArea(
      child: AppPadding(
        child: ListView(
          children: [
            //AppTitle(titleText: titleText)
            Obx(
                  () => !controller.isSearching.value
                  ? Column(
                children: [
                  sizedBox,
                  addNewBank,
                ],
              )
                  : Container(),
            ),
            sizedBox,
            AppCard(
              child: AppTextField(
                textController: controller.textController,
                prefixIcon: Icon(Icons.search),
                hintText: TranslationKeys.searchSavedBankAccounts.tr,
                onChange: controller.onSearch,
                suffixIcon: Obx(()=>controller.isSearching.value?InkWell(onTap:controller.resetSearch,child: Icon(Icons.close)):SizedBox(width: 1,)),
              ),
            ),
            sizedBox,
            AppCard(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(TranslationKeys.savedUpiIDs.tr),
                  sizedBox,
                  InkWell(
                    onTap: UpiPayPage.start,
                    child: Row(
                      children: [
                        SvgPicture.asset(AssetConstants.icSbiBank,height: 40,),
                        const SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Mira Jahan',
                              style: TextStyles.sp16(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              'Canara Bank 5182',
                              style: TextStyles.sp14(fontWeight: FontWeight.w400),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  sizedBox,
                  InkWell(
                    onTap: UpiPayPage.start,
                    child: Row(
                      children: [
                        SvgPicture.asset(AssetConstants.icUpiUser),
                        const SizedBox(
                          width: 16,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Cheteshwari C Rao',
                              style: TextStyles.sp16(fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 7,
                            ),
                            Text(
                              '9999999@upi',
                              style: TextStyles.sp14(fontWeight: FontWeight.w400),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 100,
            ),

            const PoweredByYesBankBhim()
          ],
        ),
      ),
    );
  }

  Widget get sizedBox {
    return const SizedBox(
      height: 16,
    );
  }

  Widget get addNewBank {
    return InkWell(
      onTap: AddNewBeneficiaryPage.start,
      child: AppCard(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  height: 40,
                  width: 40,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(40)),
                      color: Palette.colorPageBgG05),
                  child: const Icon(Icons.add),
                ),
                const SizedBox(
                  width: 16,
                ),
                Text(TranslationKeys.addNewBeneficiary.tr),
              ],
            ),
            const Icon(
              Icons.arrow_forward_ios_outlined,
              color: Palette.colorPrimaryTextG50,
              size: 20,
            )
          ],
        ),
      ),
    );
  }
}
