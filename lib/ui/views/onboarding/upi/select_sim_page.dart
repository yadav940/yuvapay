import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/controller/onboarding/upi/select_sim_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_loading.dart';
import 'package:yuvapay/ui/views/widgets/app_radio_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';
import 'package:yuvapay/ui/views/widgets/popup_utils.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';
import 'package:yuvapay/utils/enums.dart';
import 'package:yuvapay/utils/network_status.dart';

class SelectSimPage extends AppPageWithAppBar {
  static const routeName = "/select_sim_page";

  static void start() {
    //navigateOffAll(routeName);
    Get.toNamed(SelectSimPage.routeName);
  }

  final controller = Get.put(SelectSimController());

  @override
  String get title => TranslationKeys.upi.tr;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Obx(() => controller.networkStatus.value == NetworkStatus.LOADING
            ? AppLoading()
            : getPage),
      ),
    );
  }

  Widget get getPage {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppTitle(
              titleText: TranslationKeys.registerYourPhoneNumber.tr,
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              TranslationKeys.registerRBIGuidlinesText.tr,
              style: TextStyles.sp16(fontWeight: FontWeight.w400),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              '9482140204',
              style: TextStyles.sp16(fontWeight: FontWeight.bold),
            ),
            //paymentFor,
            const SizedBox(
              height: 48,
            ),
            Obx(() => Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        controller.onChangedSim(SelectSim.sim1);
                      },
                      child: simContainer(
                          textSim: TranslationKeys.sim1,
                          path: AssetConstants.icSim1,
                          simName: TranslationKeys.jio4G.tr,
                          isSelected:
                              controller.selectSim.value == SelectSim.sim1),
                    ),
                    const SizedBox(
                      width: 86,
                    ),
                    GestureDetector(
                      onTap: () {
                        controller.onChangedSim(SelectSim.sim2);
                      },
                      child: simContainer(
                          textSim: TranslationKeys.sim2.tr.tr,
                          path: AssetConstants.icSim2,
                          simName: TranslationKeys.airtel4G.tr,
                          isSelected:
                              controller.selectSim.value == SelectSim.sim2),
                    ),
                  ],
                ))
            //
          ],
        ),
        Column(
          children: [
            const PoweredByYesBankBhim(),
            const SizedBox(height: 10),
            Text(
              TranslationKeys.byClickingContinueYouAgreeAndoConditions.tr,
              style: TextStyles.sp14(),
            ),
            const SizedBox(
              height: 16,
            ),
            appButton
          ],
        )
      ],
    );
  }

  Widget simContainer(
      {required String textSim,
      required String path,
      required String simName,
      required bool isSelected}) {
    return Column(
      children: [
        Container(
          height: 88,
          width: 88,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(12)),
            color: isSelected ? Palette.colorBorderG10 : Palette.colorPageBg,
            border: Border.all(
              color: Palette.colorBorderG10,
              width: 1,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(path),
              Text(textSim),
            ],
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            AppRadioButton(isSelected),
            const SizedBox(
              width: 6,
            ),
            Text(
              simName,
              style: TextStyles.sp14(),
            )
          ],
        )
      ],
    );
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.button_contnue.tr,
      onPressed: () {
        showAppBottomSheet(
                child: errorWidget,
                initialChildSize: 0.35,
                dragPickColor: Colors.transparent)
            .then((value) => {controller.onNext()});
      },
    );
  }

  Widget get errorWidget {
    return Padding(
      padding: const EdgeInsets.only(left: 24.0, right: 24.0),
      child: Column(
        children: [
          SvgPicture.asset(AssetConstants.icErrorAlert),
          const SizedBox(
            height: 24,
          ),
          Text(
            TranslationKeys.errorOccured.tr,
            style: TextStyles.sp18(),
          ),
          const SizedBox(
            height: 4,
          ),
          Text(
            TranslationKeys.pleaseTryAgainLater.tr,
            style: TextStyles.sp18(),
          ),
          const SizedBox(
            height: 40,
          ),
          AppButton(
            text: TranslationKeys.okay.tr,
            onPressed: () {
              Navigator.pop(Get.context!);
              controller.onNext();
            },
          )
        ],
      ),
    );
  }
}
