
import 'package:get/get.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

import '../../../../usefull/palette.dart';
import '../../../base/base.dart';
import '../../../base/page.dart';

class UpiTransactionPinPage extends AppPageWithAppBar {
  static const routeName = "/upi_pin_transaction_page";

  static void start() {
    Get.toNamed(UpiTransactionPinPage.routeName);
  }

  @override
  Color get actionBarbackgroundColor => Palette.colorPrimaryTextG80;

  //final controller = Get.put(UpiSearchController());

  /*@override
  String get title => TranslationKeys.upiIDTransfer.tr;*/


  @override
  Widget get body {
    return SafeArea(
      child: Column(
        children: [
          Text('Pin'),
          _showPinCodeTextField()
        ],
      ),
    );
  }

  Widget _showPinCodeTextField() {
    return PinCodeTextField(
      hideCharacter: true,
      maxLength: 6,
      autofocus: true,
      highlightColor: Colors.red,
      defaultBorderColor: Palette.greyScaleDark6,
      hasTextBorderColor: Palette.psMain,
      pinBoxRadius: 12,
      pinBoxHeight: 56,
      pinBoxWidth: 48,
      pinBoxDecoration: (
          Color borderColor,
          Color pinBoxColor, {
            double borderWidth = 2.0,
            double radius = 0,
          }) {
        return BoxDecoration(
          color: pinBoxColor,
        );
      },
      keyboardType: TextInputType.number,
      controller: TextEditingController(),
      hasUnderline: true,
      onDone: (_) {
      },
      onTextChanged: (_) {},
    );
  }
}