import 'package:flutter_svg/flutter_svg.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/onboarding/upi/addbank/addbank_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_loading.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/app_text_field.dart';
import 'package:yuvapay/ui/views/widgets/popup_utils.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';
import 'package:yuvapay/utils/network_status.dart';

import '../../../../../controller/onboarding/upi/upi_pay_controller.dart';
import '../../../../../controller/onboarding/upi/upi_search_controller.dart';
import '../../../../../utils/enums.dart';
import '../../../../usefull/palette.dart';

class UpiPayPage extends AppPageWithAppBar {
  static const routeName = "/upi_pay_page";

  static void start({FromUpiPayPage? page}) {
    Get.toNamed(UpiPayPage.routeName, arguments: page);
  }

  @override
  Color get actionBarbackgroundColor => Palette.colorPrimaryTextG80;

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  final controller = Get.put(UpiPayController());

  @override
  Widget get body {
    return SafeArea(
      child: Obx(
        () => controller.networkStatus.value == NetworkStatus.LOADING
            ? AppLoading()
            : getBody,
      ),
    );
  }

  Widget get getBody {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          children: [
            getUserDetail,
            AppPadding(
              child: Column(
                children: [
                  const SizedBox(
                    height: 12,
                  ),
                  appInputCard,
                  const SizedBox(
                    height: 12,
                  ),
                  Get.arguments==FromUpiPayPage.requestToWallet?getWalletDetail:getBankDetail
                ],
              ),
            )
          ],
        ),
        getBottom
      ],
    );
  }

  Widget get appInputCard {
    return AppCard(
      child: Column(
        children: [
          AppTextField(
            textController: TextEditingController(),
            hintText: TranslationKeys.enterAmount.tr,
            inputType: TextInputType.number,
            prefixIcon: SizedBox(
              height: 24,
              width: 10,
              child: Center(
                child: SvgPicture.asset(
                  AssetConstants.icRupeeSymbol,
                ),
              ),
            ),
          ),
          TextField(
            decoration:
                InputDecoration(hintText: TranslationKeys.addNoteOptional.tr),
          )
        ],
      ),
    );
  }

  Widget get getUserDetail {
    return Container(
      color: Palette.colorPrimaryTextG80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              SvgPicture.asset(
                AssetConstants.icUpiUser,
                height: 50,
              ),
              const SizedBox(
                height: 24,
              ),
              Obx(
                () => Text(
                  controller.sendToText.value,
                  style: TextStyles.sp18(color: Palette.colorPageBg),
                ),
              ),
              const SizedBox(
                height: 6,
              ),
              Obx(
                () => Text(
                  controller.sendToUpiText.value,
                  style: TextStyles.sp14(color: Palette.colorPrimaryTextG50),
                ),
              ),
              const SizedBox(
                height: 24,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget get getBankDetail {
    return AppCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            TranslationKeys.payFrom.tr,
            style: TextStyles.sp14(color: Palette.colorPrimaryTextG50),
          ),
          const SizedBox(
            height: 8,
          ),
          InkWell(
            onTap: () {
              showAppBottomSheet(child: getPopup);
            },
            child: _listViewItem(
                "State Bank Of India", AssetConstants.icSbiBank,
                isCorner: true),
          )
        ],
      ),
    );
  }
  Widget get getWalletDetail {
    return AppCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Credit to'.toUpperCase(),
            style: TextStyles.sp14(color: Palette.colorPrimaryTextG50),
          ),
          const SizedBox(
            height: 8,
          ),
          Padding(
            padding:
            const EdgeInsets.only(left: 8.0, right: 8.0, top: 12, bottom: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(
                        height: 32,
                        width: 32,
                        child: SvgPicture.asset(
                          AssetConstants.icWallet,
                          //scale: scale,
                        )),
                    const SizedBox(width: 8),
                    Text(
                      'Yuva Pay Wallet',
                      style: TextStyles.sp16(fontWeight: FontWeight.normal),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                Text('₹ 500.00',style: TextStyles.sp16(),)
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _listViewItem(String title, String assetPath,
      {bool isCorner = false}) {
    return Padding(
      padding:
          const EdgeInsets.only(left: 8.0, right: 8.0, top: 12, bottom: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              SizedBox(
                  height: 32,
                  width: 32,
                  child: SvgPicture.asset(
                    assetPath,
                    //scale: scale,
                  )),
              const SizedBox(width: 8),
              Text(
                title,
                style: TextStyles.sp16(fontWeight: FontWeight.normal),
                textAlign: TextAlign.center,
              )
            ],
          ),
          if (isCorner)
            InkWell(
              onTap: () {
                showAppBottomSheet(child: getPopup);
              },
              child: Icon(
                Icons.arrow_forward_ios_outlined,
                size: 24,
                color: Palette.colorPrimaryTextG50,
              ),
            )
        ],
      ),
    );
  }

  Widget get getBottom {
    return Column(
      children: [
        const PoweredByYesBankBhim(),
        AppCard(
          child: Obx(
            () => controller.isError.value
                ? getError
                : AppButton(
                    onPressed: controller.onNext,
                    text: controller.buttonText.value),
          ),
        )
      ],
    );
  }

  Widget get getError {
    return Column(
      children: [
        errorGetMessage,
        const SizedBox(
          height: 16,
        ),
        errorButton,
      ],
    );
  }

  Widget get errorGetMessage {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        color: controller.errorState.value == 1
            ? Palette.colorWarning.withOpacity(0.1)
            : Palette.colorError.withOpacity(0.1),
      ),
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              SvgPicture.asset(
                AssetConstants.icErrorAlert,
                color: controller.errorState.value == 1
                    ? Palette.colorWarning
                    : Palette.colorError,
                height: 16,
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                controller.errorState.value == 1
                    ? TranslationKeys.bankServersBusy.tr
                    : controller.errorState.value == 2
                        ? TranslationKeys.insufficientFunds.tr
                        : TranslationKeys.invalidPin.tr,
                style: TextStyles.sp16(
                    color: controller.errorState.value == 1
                        ? Palette.colorWarning
                        : Palette.colorError,
                    fontWeight: FontWeight.w500),
              )
            ],
          ),
          const SizedBox(
            height: 6,
          ),
          Text(
            controller.errorState.value == 1
                ? TranslationKeys
                    .thePaymentWasDeclinedBecauseYouBankServersWereTooBusy.tr
                : controller.errorState.value == 2
                    ? TranslationKeys
                        .thePaymentWasDeclinedBecauseYouNotHaveEnouthBalanace.tr
                    : TranslationKeys.thePinInvalidKindlyRetryOrResetPin.tr,
            style: TextStyles.sp14(color: Palette.colorPrimaryTextG70),
          ),
        ],
      ),
    );
  }

  Widget get errorButton {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: AppButtonWithBorder(
            text: controller.errorState != 3
                ? TranslationKeys.back.tr
                : TranslationKeys.forgotPin.tr,
            onPressed: () {
              Get.back();
            },
          ),
        ),
        const SizedBox(width: 8),
        Expanded(
          flex: 1,
          child: AppButton(
            text: TranslationKeys.retry.tr,
            onPressed: () async {
              //Get.back();
              controller.onNext();
            },
          ),
        ),
      ],
    );
  }

  Widget get getPopup {
    return GestureDetector(
      onTap: () {
        Navigator.pop(Get.context!);
      },
      child: Column(
        children: [
          _listViewItem("State Bank Of India", AssetConstants.icSbiBank),
          _listViewItem("State Bank Of India", AssetConstants.icSbiBank),
          _listViewItem("State Bank Of India", AssetConstants.icSbiBank),
          _listViewItem("State Bank Of India", AssetConstants.icSbiBank),
          _listViewItem("State Bank Of India", AssetConstants.icSbiBank),
          InkWell(
            onTap: () {
              AddbankPage.start();
            },
            child: _listViewItem(
              "Add New Bank",
              AssetConstants.icSbiBank,
            ),
          ),
          _listViewItem(
            "Check Balance",
            AssetConstants.icSbiBank,
          ),
          const PoweredByYesBankBhim()
        ],
      ),
    );
  }
}
