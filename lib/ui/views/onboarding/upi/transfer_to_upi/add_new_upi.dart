import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';

import '../../../../../controller/onboarding/upi/add_new_upi_controller.dart';

class AddNewUpiPage extends AppPageWithAppBar {
  static const routeName = "/add_new_upi_page";

  static void start() {
    Get.toNamed(AddNewUpiPage.routeName);
  }

  final controller = Get.put(AddNewUpiController());

  @override
  String get title => TranslationKeys.upiIDTransfer.tr;

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  @override
  Widget get body {
    return SafeArea(
      child: AppPadding(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                const SizedBox(
                  height: 16,
                ),
                AppCard(
                  child: Column(
                    children: [
                      TextField(
                        controller: TextEditingController(),
                        decoration: new InputDecoration(
                            hintText: TranslationKeys.enterUpiID.tr),
                        onChanged: controller.onSearch,
                      ),
                      const SizedBox(
                        height: 7,
                      ),
                      Obx(
                        () => controller.isError.value
                            ? Row(
                                children: [
                                  SvgPicture.asset(
                                    AssetConstants.icErrorAlert,
                                    height: 12,
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    TranslationKeys.invalidUpiID.tr,
                                    style: TextStyles.sp12(
                                        color: Palette.colorError),
                                  )
                                ],
                              )
                            : Container(),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 100,
                ),
              ],
            ),
            Column(
              children: [
                PoweredByYesBankBhim(),
                const SizedBox(
                  height: 6,
                ),
                AppButton(
                  onPressed: () {Get.back();},
                  text: TranslationKeys.proceedToPay.tr,
                ),
                const SizedBox(
                  height: 16,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
