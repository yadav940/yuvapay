
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';

import '../../../../../controller/onboarding/upi/upi_search_controller.dart';
import '../transfer_to_upi/upi_pay_page.dart';

class TransferToSelfPage extends AppPageWithAppBar {
  static const routeName = "/transfer_to_self_page";

  static void start() {
    Get.toNamed(TransferToSelfPage.routeName);
  }

  final controller = Get.put(UpiSearchController());

  @override
  String get title => TranslationKeys.selfTransferTo.tr;

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  @override
  Widget get body {
    return SafeArea(
      child: AppPadding(
        child: ListView(
          children: [
            sizedBox,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                sizedBox,
                listItem(isDefault:true),
                sizedBox,
                listItem(),
                sizedBox,
                listItem(),
                sizedBox,
                addNewBankAccount

              ],
            ),
            const SizedBox(
              height: 100,
            ),

            const PoweredByYesBankBhim()
          ],
        ),
      ),
    );
  }

  Widget listItem({bool isDefault=false}) {
    return AppCard(
      child: InkWell(
                    onTap: UpiPayPage.start,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            SvgPicture.asset(AssetConstants.icSbiBank,height: 40,),
                            const SizedBox(
                              width: 16,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Mira Jahan',
                                  style: TextStyles.sp16(fontWeight: FontWeight.w500),
                                ),
                                const SizedBox(
                                  height: 7,
                                ),
                                Text(
                                  'Canara Bank 5182',
                                  style: TextStyles.sp14(fontWeight: FontWeight.w400),
                                ),
                                const SizedBox(
                                  height: 7,
                                ),
                                if(isDefault)Text(
                                  'Default',
                                  style: TextStyles.sp14(fontWeight: FontWeight.w400),
                                )
                              ],
                            )
                          ],
                        ),
                        arrowForward
                      ],
                    ),
                  ),
    );
  }

  Widget get sizedBox {
    return const SizedBox(
      height: 12,
    );
  }

  Widget get addNewBankAccount {
    return InkWell(
      child: AppCard(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  height: 40,
                  width: 40,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(40)),
                      color: Palette.colorPageBgG05),
                  child: const Icon(Icons.add),
                ),
                const SizedBox(
                  width: 16,
                ),
                Text(TranslationKeys.addNewBankAccount.tr),
              ],
            ),
            arrowForward
          ],
        ),
      ),
    );
  }

  Icon get arrowForward {
    return const Icon(
            Icons.arrow_forward_ios_outlined,
            color: Palette.colorPrimaryTextG50,
            size: 20,
          );
  }
}
