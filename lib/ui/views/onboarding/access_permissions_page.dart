
import 'package:flutter_svg/svg.dart';
import 'package:yuvapay/controller/onboarding/access_permission_controller.dart';
import 'package:yuvapay/model/onboarding/permission_model.dart';
import 'package:yuvapay/services/navigation.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';

import '../base/base.dart';

class AccessPermissionPage extends AppPageWithAppBar {
  static const routeName = "/access_permission";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(AccessPermissionController());

  @override
  double? get toolbarHeight => 0;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppTitle(
              titleText: TranslationKeys
                  .weWillRequireFewPermissionsToServeYouBetter.tr,
            ),
            const SizedBox(
              height: 26,
            ),
            Expanded(child: getPermissionList),
            warning,
            const SizedBox(
              height: 24,
            ),
            appButton
          ],
        ),
      ),
    );
  }

  get getPermissionList => ListView.builder(
        shrinkWrap: true,
        itemCount: controller.items.length,
        itemBuilder: (BuildContext context, int index) {
          return permissionItem(controller.items[index]);
        },
      );

  Widget permissionItem(PermissionModel data) {
    return Row(
      children: [
        SvgPicture.asset(
          data.asset,
          semanticsLabel: 'icon',
        ),
        //26
        Flexible(
          child: Padding(
            padding: const EdgeInsets.only(left: 26.0, top: 12, bottom: 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  data.title,
                  style: TextStyles.sp16(),
                ),
                const SizedBox(
                  height: 7,
                ),
                Text(
                  data.bodyMessage,
                  style: TextStyles.sp14(),
                  overflow: TextOverflow.clip,
                ),
              ],
            ),
          ),
        )
        //assets/images/svg/ic_contact.svg
      ],
    );
  }

  Widget get warning {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(
          'assets/images/svg/ic_secure.svg',
          semanticsLabel: 'icon',
        ),
        const SizedBox(
          width: 7,
        ),
        Expanded(
          child: Text(
            TranslationKeys.yourInformationIsSafeAndSecureUs.tr,
            style: TextStyles.sp16(),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.button_contnue,
      onPressed: controller.getPermission,
    );
  }
}
