
import 'package:carousel_slider/carousel_slider.dart';
import 'package:yuvapay/controller/onboarding/welcome_controller.dart';
import 'package:yuvapay/model/onboarding/welcome_model.dart';
import 'package:yuvapay/services/navigation.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/onboarding/access_permissions_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';

import '../base/base.dart';

class WelcomePage extends AppPageWithAppBar {
  static const routeName = "/welcome";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(WelcomeController());

  @override
  double? get toolbarHeight => 0;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                welcomeBody,
                indicator,
              ],
            ),
            appButton
          ],
        ),
      ),
    );
  }

  Widget get welcomeBody {
    return CarouselSlider(
      options: CarouselOptions(
        height: 560,
        viewportFraction: 1,
        initialPage: 0,
        enableInfiniteScroll: true,
        reverse: false,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 4),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,
        enlargeCenterPage: false,
        onPageChanged: controller.changeSlider,
        scrollDirection: Axis.horizontal,
      ),
      items: controller.items.map((data) {
        return Builder(
          builder: (BuildContext context) {
            return welcomeItem(data);
          },
        );
      }).toList(),
    );
  }

  Widget get indicator {
    return Obx(
      () => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          dot(controller.selectedItemIndex.value == 0),
          const SizedBox(
            width: 7,
          ),
          dot(controller.selectedItemIndex.value == 1),
          const SizedBox(
            width: 7,
          ),
          dot(controller.selectedItemIndex.value == 2),
        ],
      ),
    );
  }

  Widget get appButton {
    return AppButton(
      text: 'Continue',
      onPressed: () {
        //AccessPermissionPage.start();
        Get.toNamed(AccessPermissionPage.routeName);
      },
    );
  }

  Widget welcomeItem(WelcomeModel welcomeModel) {
    return Container(
      //margin: EdgeInsets.symmetric(horizontal: 5.0),
      //decoration: BoxDecoration(color: Colors.amber),
      child: Column(
        children: [
          Container(
              decoration: BoxDecoration(
                color: welcomeModel.assetBackgroundColor,
                borderRadius: BorderRadius.circular(52.0),
              ),
              child: Image.asset(
                welcomeModel.asset,
                height: 400,
                alignment: Alignment.bottomCenter,
                //semanticsLabel: 'icon',
              )),
          const SizedBox(
            height: 42,
          ),
          Text(
            welcomeModel.title,
            style: TextStyles.pageTitle(fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 18,
          ),
          Text(
            welcomeModel.bodyMessage,
            textAlign: TextAlign.center,
            style: TextStyles.sp16(
              fontWeight: FontWeight.normal,
              color: Palette.colorPrimaryTextG90,
            ),
          ),
        ],
      ),
    );
  }

  Widget dot(bool isSelected) {
    return Container(
      width: 8,
      height: 8,
      decoration: BoxDecoration(
        color: isSelected ? Palette.buttonBackground : Palette.greyScaleDark5,
        borderRadius: BorderRadius.all(Radius.elliptical(8, 8)),
      ),
    );
  }
}
