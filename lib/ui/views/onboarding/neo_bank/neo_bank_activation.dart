import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/onboarding/widget/neo_bank_appbar.dart';
import 'package:yuvapay/ui/views/onboarding/widget/neo_bank_info_list.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';

import 'adharcard_detail_page.dart';

class NeoBankActivatePage extends NeoBankAppBar {
  static const routeName = "/neo_bank_activate_page";

  static void start() {
    //navigateOffAll(routeName);
    Get.toNamed(NeoBankActivatePage.routeName);
  }

  //final controller = Get.put(PhoneNumberAndEmailOTPController());



  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTitle(
                  titleText: TranslationKeys.simplifiedBankAccountEveryone.tr,
                ),
                const SizedBox(
                  height: 28,
                ),
                const NeoBankInfoList(),
                const SizedBox(
                  height: 26,
                ), //
              ],
            ),
            appButton
          ],
        ),
      ),
    );
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.openYourBankAccountMinutes.tr,
      onPressed: NeobankAdharCardDetailPage.start,
    );
  }
}
