import 'package:get/get.dart';
import 'package:yuvapay/controller/onboarding/neo_bank/camera_controller.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/onboarding/neo_bank/autosend_sms_page.dart';
import 'package:yuvapay/ui/views/onboarding/widget/neo_bank_appbar.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';

class KycCameraPage extends NeoBankAppBar {
  static const routeName = "/camera_kyc_page";

  static void start() {
    //navigateOffAll(routeName);
    debugPrint(routeName);
    Get.toNamed(KycCameraPage.routeName);
  }

  final controller = Get.put(KycCameraController());

  @override
  double? get toolbarHeight => 0;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [

            Text('Camera page'),
           // CameraPreview(controller.controller)
            appButton
          ],
        ),
      ),
    );
  }

  SizedBox get sizedBox24 {
    return const SizedBox(
      height: 24,
    );
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.button_contnue.tr,
      onPressed: AutosendSmsPage.start,
    );
  }
}
