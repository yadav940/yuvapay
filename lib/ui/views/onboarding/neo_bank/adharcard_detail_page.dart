//
import 'package:get/get.dart';
import 'package:yuvapay/controller/onboarding/wallete/wallete_pan_detail_controller.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/onboarding/widget/neo_bank_appbar.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_text_field.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';

import 'neo_bank_aadhar_otp_page.dart';

class NeobankAdharCardDetailPage extends NeoBankAppBar {
  static const routeName = "/neobank_adharcard_detail_pagel";

  static void start() {
    //Get.offAll(PhoneNumberAndEmailPage(),arguments: page);
    Get.toNamed(
      NeobankAdharCardDetailPage.routeName,
    );
  }

  final controller = Get.put(WalletePanDetailController());


  @override
  Widget get body {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTitle(
                    titleText: TranslationKeys.aadharCardDetails.tr,
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  textlable(TranslationKeys.nameonAadharCard.tr),
                  const SizedBox(
                    height: 4,
                  ),

                  nameTextField,
                  const SizedBox(
                    height: 40,
                  ),
                  textlable(TranslationKeys.aadharCardNumber.tr),
                  const SizedBox(
                    height: 4,
                  ),
                  aadharTextField,
                  const SizedBox(
                    height: 8,
                  ),
                  errorText,

                  const SizedBox(
                    height: 26,
                  ), //

                ],
              ),

              appButton
            ],
          ),
        ),
      ),
    );
  }

  Widget textlable(String data) {
    return Text(
      data,
      style: TextStyles.sp12(),
    );
  }

  Widget get nameTextField {
    return Obx(() => AppTextField(
      hintText: TranslationKeys.enterYourName.tr,
      textController: TextEditingController(),
      onChange: controller.onChangeText,
      autofocus: true,
      inputType: TextInputType.text,
      borderColor: controller.isError.value
          ? Palette.colorError
          : Palette.greyScaleDark7,
    ));
  }

  Widget get aadharTextField {
    return Obx(() => AppTextField(
      hintText: TranslationKeys.enteryourAadharNumber.tr,
      textController: TextEditingController(),
      onChange: controller.onChangeText,
      autofocus: true,
      inputType: TextInputType.number,
      borderColor: controller.isError.value
          ? Palette.colorError
          : Palette.greyScaleDark7,
    ));
  }

  Widget get errorText {
    return Obx(() => Text(
      controller.errorMessage.value,
      style: TextStyles.sp12(color: Palette.colorError),
    ));
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.button_contnue.tr,
      onPressed: NeoBankAadharOtpPage.start,
    );
  }
}
