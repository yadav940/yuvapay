import 'package:get/get.dart';
import 'package:yuvapay/controller/onboarding/phone_number_and_email_otp_controller.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/onboarding/widget/neo_bank_appbar.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';

import 'kyc_camera_page.dart';

class VideoKycPage extends NeoBankAppBar {
  static const routeName = "/video_kyc_page";

  static void start() {
    //navigateOffAll(routeName);
    debugPrint(routeName);
    Get.toNamed(VideoKycPage.routeName);
  }

  final controller = Get.put(PhoneNumberAndEmailOTPController());

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTitle(
                  titleText: TranslationKeys.nextVideoKYC.tr,
                ),
                const SizedBox(
                  height: 8,
                ),
                getText(TranslationKeys.makeSureYouHaveTheFollowingReady.tr),
                sizedBox24,
                getRequirementList,
                const SizedBox(
                  height: 26,
                ), //
              ],
            ),
             appButton
          ],
        ),
      ),
    );
  }

  SizedBox get sizedBox24 {
    return const SizedBox(
      height: 24,
    );
  }

  Widget getText(String text) {
    return Text(
      text,
      style: TextStyles.sp16(fontWeight: FontWeight.w400),
    );
  }

  Widget get getRequirementList {
    return Column(
      children: [
        getRequirementItem(TranslationKeys.physicalPANCard.tr),
        sizedBox24,
        getRequirementItem(TranslationKeys.goodInternetConnectivity.tr),
        sizedBox24,
        getRequirementItem(TranslationKeys.wellLitEnvironment.tr),
      ],
    );
  }

  getRequirementItem(String text) {
    return Row(
      children: [
        Icon(
          Icons.arrow_forward,
          color: Palette.colorPrimaryTextG90,
        ),
        const SizedBox(
          width: 16,
        ),
        getText(text)
      ],
    );
  }

  Widget get appButton {
    return AppButton(
      text: TranslationKeys.button_contnue.tr,
      onPressed: KycCameraPage.start,
    );
  }
}
