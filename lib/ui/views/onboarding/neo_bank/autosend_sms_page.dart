import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/onboarding/widget/neo_bank_appbar.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';

import 'neo_bank_success_page.dart';

class AutosendSmsPage extends NeoBankAppBar {
  static const routeName = "/autosend_sms_page";

  static void start() {
    //navigateOffAll(routeName);
    debugPrint(routeName);
    Get.toNamed(AutosendSmsPage.routeName);
  }

 //// final controller = Get.put(PhoneNumberAndEmailOTPController());

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppTitle(
                  titleText: TranslationKeys.autosendSMSToCreateUPI.tr,
                ),
                const SizedBox(
                  height: 8,
                ),

                const SizedBox(
                  height: 26,
                ), //
              ],
            ),
            appButton
          ],
        ),
      ),
    );
  }


  Widget get appButton {
    return AppButton(
      text: TranslationKeys.button_contnue.tr,
      onPressed: NeoBankSuccessPage.start,
    );
  }
}
