
import 'package:flutter/services.dart';
import 'package:yuvapay/controller/onboarding/phone_number_and_email_controller.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base_stateful_widget.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_text_field.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';
import 'package:yuvapay/utils/enums.dart';

import '../base/base.dart';

class PhoneNumberAndEmailPage extends AppPageWithAppBar {
  static const routeName = "/phone_number_and_email";

  static void start(PhoneNumberEmail page) {
    //Get.offAll(PhoneNumberAndEmailPage(),arguments: page);
    Get.toNamed(PhoneNumberAndEmailPage.routeName, arguments: page);
  }

  final controller = Get.put(PhoneNumberAndEmailController());

  @override
  double? get toolbarHeight => 0;

  @override
  Widget get body {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppTitle(
                    titleText: Get.arguments == PhoneNumberEmail.phoneNumber
                        ? TranslationKeys.enter_phone_number.tr
                        : TranslationKeys.enter_your_email_id.tr,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    Get.arguments == PhoneNumberEmail.phoneNumber
                        ? TranslationKeys
                            .to_get_started_enter_your_phone_number.tr
                        : TranslationKeys.register_your_email_address.tr,
                    style: TextStyles.sp16(fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 40,
                  ),

                  textField,
                  const SizedBox(
                    height: 8,
                  ),
                  errorText,

                  const SizedBox(
                    height: 26,
                  ), //
                ],
              ),
              appButton
            ],
          ),
        ),
      ),
    );
  }

  Widget get textField {
    return Obx(() => AppTextField(
          hintText: Get.arguments == PhoneNumberEmail.phoneNumber
              ? "000 000 0000"
              : "abc@gmail.com",
          textController: controller.textController,
          onChange: controller.onChangeText,
          autofocus: true,
      inputFormatters: Get.arguments == PhoneNumberEmail.phoneNumber?[
        LengthLimitingTextInputFormatter(10),
      ]:null,
          inputType: Get.arguments == PhoneNumberEmail.phoneNumber
              ? TextInputType.phone
              : TextInputType.text,
          borderColor: controller.isError.value
              ? Palette.colorError
              : Palette.greyScaleDark7,
        ));
  }

  Widget get errorText {
    return Obx(() => Text(
          controller.errorMessage.value,
          style: TextStyles.sp12(color: Palette.colorError),
        ));
  }

  Widget get appButton {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if (Get.arguments == PhoneNumberEmail.email)
          Padding(
            padding: const EdgeInsets.all(28.0),
            child: Text(
              TranslationKeys.continue_with_logged_in_account.tr,
              style: TextStyles.sp14(
                  color: Palette.buttonBackground, fontWeight: FontWeight.w500),
            ),
          ),
        AppButton(
          text: TranslationKeys.button_contnue.tr,
          onPressed: controller.next,
        )
      ],
    );
  }
}
