import 'package:flutter_svg/flutter_svg.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/views/wallet/reset_yuvapay_pin_page.dart';
import 'package:yuvapay/ui/views/wallet/wallet_onboarding_aadhaar_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';

class TransactionDetailsPage extends AppPageWithAppBar {
  static const routeName = "/transaction_details_page";


  static void start({String? page}) {
    Get.toNamed(TransactionDetailsPage.routeName,arguments: page);
    //Get.to(MobileRechargePrepaidPage());
  }

  @override
  // TODO: implement title
  String get title => 'Transaction Details';

  @override
  Widget get body {
    return SafeArea(
      child: _showBodyContent(),
    );
  }

  Widget _showBodyContent() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: SingleChildScrollView(
        child: Column(
          children: [
            AppCard(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: 164,
                        width: MediaQuery.of(Get.context!).size.width,
                        decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(12)),
                          border: Border.all(
                            color: Colors.red,
                            width:
                                4, //                   <--- border width here
                          ),
                        ),
                        child: Center(
                            child: Padding(
                          padding: const EdgeInsets.only(bottom: 50),
                          child: Text(
                            '₹ 1,000',
                            style: TextStyles.sp40(),
                          ),
                        )),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: 60,
                          width: MediaQuery.of(Get.context!).size.width,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: const BorderRadius.only(
                                bottomLeft: Radius.circular(12),
                                bottomRight: Radius.circular(12)),
                            border: Border.all(
                              color: Colors.red,
                              width:
                                  4, //                   <--- border width here
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Text('Oct 10, 6:52 AM', style: TextStyles.sp14()),
                  const SizedBox(height: 20),
                  Container(
                      padding: const EdgeInsets.all(12),
                      decoration: const BoxDecoration(
                        color: Palette.colorPageBgG07,
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                      ),
                      child: Text('Note: Sent for the sake of Bla',
                          style: TextStyles.sp16())),
                ],
              ),
            ),
            const SizedBox(height: 12),
            AppCard(
                child: ListView(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              children: [
                const Text('To:'),
                const SizedBox(height: 16),
                ListTile(
                  contentPadding: EdgeInsets.all(0),
                  title: Text('Yuva Pay Wallet',
                      style: TextStyles.sp18(fontWeight: FontWeight.w500)),
                  subtitle: Text(
                    'Prahlad Kumar',
                    style: TextStyles.sp14(
                        fontWeight: FontWeight.w400,
                        color: Palette.colorTextLabel),
                  ),
                  leading: SizedBox(
                    height: 56,
                    width: 56,
                    child: Image.asset(
                      AssetConstants.myAccountProfileImage,
                      scale: 0.5,
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                Divider(),
                const SizedBox(height: 16),
                const Text('From:'),
                const SizedBox(height: 16),
                ListTile(
                  contentPadding: EdgeInsets.all(0),
                  title: Text('Canara bank XXXX 3344',
                      style: TextStyles.sp18(fontWeight: FontWeight.w500)),
                  subtitle: Text(
                    'Prahlad Kumar',
                    style: TextStyles.sp14(
                        fontWeight: FontWeight.w400,
                        color: Palette.colorTextLabel),
                  ),
                  leading: SizedBox(
                    height: 56,
                    width: 56,
                    child: Image.asset(
                      AssetConstants.myAccountProfileImage,
                      scale: 0.5,
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                Divider(),
                const SizedBox(height: 16),
                Text(
                  'Transaction ID:',
                  style: TextStyles.sp14(),
                ),
                const SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '87y3eoqqewqdhjhassd82',
                      style: TextStyles.sp16(),
                    ),
                    Text(
                      'Copy',
                      style: TextStyles.sp16(color: Palette.colorPrimary),
                    ),
                  ],
                ),
                const SizedBox(height: 16),
                Divider(),
                const SizedBox(height: 16),
                Text('Help'),
                const SizedBox(height: 16),
              ],
            )),
          ],
        ),
      ),
    );
  }
}
