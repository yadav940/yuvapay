import 'package:flutter_svg/flutter_svg.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/app_translation.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/app_text_field.dart';

import '../../../utils/enums.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/page.dart';
import '../onboarding/upi/transfer_to_upi/upi_pay_page.dart';

class RequestWalletPage extends AppPageWithAppBar {
  static const routeName = "/request_wallet_page";

  static void start({String? arg}) {
    Get.toNamed(RequestWalletPage.routeName,arguments: arg);
  }

  @override
  String get title => TranslationKeys.requestToWallet.tr;

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  @override
  Widget get body {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8),
      child: ListView(
        children: [
          AppCard(
            child: Row(
              children: [
                Expanded(
                  child: AppTextField(
                    textController: TextEditingController(),
                    hintText: TranslationKeys.enterPhoneNumberName.tr,
                    prefixIcon: Icon(Icons.search),
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                SvgPicture.asset(AssetConstants.icKeyboardWithAbc)
              ],
            ),
          ),
          const SizedBox(
            height: 12,
          ),
          AppCard(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  TranslationKeys.yuvaPayContacts.tr.toUpperCase(),
                  style: TextStyles.sp14(color: Palette.colorPrimaryTextG50),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (context, index) => _showSearchResultItem(index),
                  itemCount: 4,
                  physics: const NeverScrollableScrollPhysics(),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _showSearchResultItem(int index) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            UpiPayPage.start(page: FromUpiPayPage.requestToWallet);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      AssetConstants.icUpiUser,
                      height: 50,
                    ),
                    const SizedBox(width: 8,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Urvashi C',
                            style: TextStyles.sp16(fontWeight: FontWeight.w500)),
                        const SizedBox(height : 8,),
                        Text('+91 916777666',
                            style: TextStyles.sp14(
                                color: Palette.colorPrimaryTextG50)),
                      ],
                    )
                  ],
                ),
                /*SizedBox(
                    height: 30,
                    width: 30,
                    child: Image.asset(AssetConstants.yuvaPayLogo),
                  )*/
                SizedBox(
                  height: 32,
                  width: 44,
                  child:  SvgPicture.asset(AssetConstants.icLogo),
                )
              ],
            ),
          ),
        ),
        index != 2 ? Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: const Divider(),
        ) : const SizedBox(height: 0),
      ],
    );
  }
}
