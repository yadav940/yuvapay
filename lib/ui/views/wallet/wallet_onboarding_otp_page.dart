import 'package:flutter_svg/svg.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:yuvapay/ui/views/wallet/create_yuvapay_pin.page.dart';

import '../../resources/asset_ constants.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';
import '../my_account/my_account_page.dart';
import '../widgets/app_button.dart';
import '../widgets/popup_utils.dart';

class WalletOnboardingOtpPage extends AppPageWithAppBar {
  static const routeName = "/wallet-onboarding";
  TextEditingController _otpController = TextEditingController();

  @override
  // TODO: implement appBar
  PreferredSizeWidget? get appBar => null;

  @override
  Widget get body {
    return SafeArea(
      child: _showBodyContent(),
    );
  }

  Widget _showBodyContent() {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                TranslationKeys.oneTimePassword.tr,
                style: TextStyles.sp22(fontWeight: FontWeight.w700),
              ),
              const SizedBox(height: 4),
              Text(
                TranslationKeys.enterTheOtpWhichHasText.tr,
                style: TextStyles.sp16(),
              ),
              const SizedBox(height: 12),
              _showOtpField(),
            ],
          ),
          _showBottomSection(),
        ],
      ),
    );
  }

  Column _showBottomSection() {
    return Column(
          children: [
            Text(
              TranslationKeys.byClickingContinueText.tr,
              style: TextStyles.sp16(
                  color: Palette.buttonBackground,
                  fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 12),
            AppButton(
                text: TranslationKeys.continueB.tr,
                onPressed: () {
                  if(_otpController.text.length==6){
                    //HARDCODED CHECKING
                    //THIS WILL BE CHECKED VIA APIs
                    if(_otpController.text=='000000'){
                      showAppBottomSheet(
                        dragPickColor: Colors.transparent,
                        initialChildSize: 0.4,
                        child: Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: _showChangePinSuccess,
                        ),
                      );
                    }
                    else{
                      showAppBottomSheet(
                        dragPickColor: Colors.transparent,
                        initialChildSize: 0.4,
                        child: Padding(
                          padding: const EdgeInsets.all(18.0),
                          child: _showWrongPin,
                        ),
                      );
                    }
                  }
                }),
          ],
        );
  }

  Widget _showOtpField() {
    return PinCodeTextField(
              maxLength: 6,
              autofocus: true,
              highlightColor: Colors.red,
              defaultBorderColor: Palette.greyScaleDark6,
              pinBoxBorderWidth: 1,
              hasTextBorderColor: Palette.psMain,
              pinBoxRadius: 12,
              pinBoxHeight: 56,
              pinBoxWidth: 48,
              keyboardType: TextInputType.number,
              controller: _otpController,
              onTextChanged: (_) {
              },
            );
  }

  final Widget _showChangePinSuccess = Column(
    children: [
      const SizedBox(height: 24),
      Transform.scale(
          scale: 5, child: SvgPicture.asset(AssetConstants.successSign)),
      const SizedBox(height: 60),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: Text(
          TranslationKeys.congratulations.tr,
          textAlign: TextAlign.center,
          style: TextStyles.sp18(
              color: Palette.colorPrimaryTextG90, fontWeight: FontWeight.w500),
        ),
      ),
      const SizedBox(height: 12),
      Text(
        TranslationKeys.kycUpdatedSuccessfully.tr,
        textAlign: TextAlign.center,
        style: TextStyles.sp18(
            color: Palette.colorPrimaryTextG90, fontWeight: FontWeight.w400),
      ),
      const SizedBox(height: 24),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8),
        child: AppButton(
          text: TranslationKeys.createYuvaPayPin.tr,
          onPressed: () async {
            Get.back();
            Get.to(() => CreateYuvaPayPinPage());
          },
        ),
      ),
    ],
  );

  final Widget _showWrongPin = Column(
    children: [
      SvgPicture.asset(AssetConstants.errorWardingSign),
      const SizedBox(height: 30),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: Text(
          TranslationKeys.otpEnteredIsInvalid.tr,
          textAlign: TextAlign.center,
          style: TextStyles.sp18(
              color: Palette.colorPrimaryTextG90, fontWeight: FontWeight.w600),
        ),
      ),
      const SizedBox(height: 24),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: AppButtonWithBorder(
                text: TranslationKeys.back.tr,
                onPressed: () async {
                  Get.back();
                },
              ),
            ),
            const SizedBox(width: 12),
            Expanded(
              flex: 1,
              child: AppButton(
                text: TranslationKeys.retry.tr,
                onPressed: () {
                  Get.back();
                },
              ),
            ),
          ],
        ),
      ),
    ],
  );
}
