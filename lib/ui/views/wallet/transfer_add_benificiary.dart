import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/wallet/transfer_amount_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';
import '../../../../../controller/onboarding/upi/add_new_beneficiary_controller.dart';

class TransferAddBenificiary extends AppPageWithAppBar {
  static const routeName = "/add_new_beneficiary_page";

  static void start() {
    // Get.toNamed(AddNewBeneficiaryPage.routeName);
  }

  final controller = Get.put(AddNewBeneficiaryController());

  @override
  String get title => TranslationKeys.bankTransfer.tr;

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  @override
  Widget get body {
    return SafeArea(
      child: AppPadding(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                const SizedBox(
                  height: 16,
                ),
                AppCard(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getDetails(TranslationKeys.accountNumber.tr, '', '',
                          controller.accountNumberTextController,
                          keyboardType: TextInputType.number),
                      Obx(
                        () => controller.isError.value
                            ? Row(
                                children: [
                                  SvgPicture.asset(
                                    AssetConstants.icErrorAlert,
                                    height: 12,
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    TranslationKeys.invalidAccountNumber.tr,
                                    style: TextStyles.sp12(
                                        color: Palette.colorError),
                                  )
                                ],
                              )
                            : Container(),
                      ),
                      const SizedBox(
                        height: 17,
                      ),
                      getDetails(TranslationKeys.confirmAccountNumber.tr, '',
                          '', controller.confirmNumberTextController,
                          onSearch: controller.onSearch,
                          keyboardType: TextInputType.number),
                      Obx(
                        () => controller.isConfirmAccountError.value
                            ? Row(
                                children: [
                                  SvgPicture.asset(
                                    AssetConstants.icErrorAlert,
                                    height: 12,
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    TranslationKeys
                                        .accountNumberDoesNotMatch.tr,
                                    style: TextStyles.sp12(
                                        color: Palette.colorError),
                                  )
                                ],
                              )
                            : Container(),
                      ),
                      const SizedBox(
                        height: 17,
                      ),
                      getDetails(TranslationKeys.ifsc.tr, '', '',
                          controller.ifscTextController),
                      Obx(
                        () => controller.isIfscError.value
                            ? Row(
                                children: [
                                  SvgPicture.asset(
                                    AssetConstants.icErrorAlert,
                                    height: 12,
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    TranslationKeys.invalidIfscCode.tr,
                                    style: TextStyles.sp12(
                                        color: Palette.colorError),
                                  )
                                ],
                              )
                            : Container(),
                      ),
                      const SizedBox(
                        height: 17,
                      ),
                      getDetails('', TranslationKeys.nameOptional.tr, '',
                          controller.nickNameTextController),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 100,
                ),
              ],
            ),
            Column(
              children: [
                const PoweredByYesBankBhim(),
                const SizedBox(
                  height: 6,
                ),
                AppButton(
                  onPressed: () {
                    Get.to(TransferAmountPage());
                    // controller.onNext();
                  },
                  text: TranslationKeys.proceedToPay.tr,
                ),
                const SizedBox(
                  height: 16,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget getDetails(String lebalText, String hintText, String error,
      TextEditingController controllerText,
      {void Function(String text)? onSearch, TextInputType? keyboardType}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(lebalText),
        TextFormField(
          inputFormatters: [
            LengthLimitingTextInputFormatter(18),
          ],
          controller: controllerText,
          keyboardType: keyboardType,
          decoration: InputDecoration.collapsed(hintText: hintText),
          onChanged: onSearch,
        ),
        const SizedBox(
          height: 7,
        ),
      ],
    );
  }
}
