import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/wallet/transfer_amount_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';
import '../../../../../controller/onboarding/upi/add_new_beneficiary_controller.dart';

class OnBoardingInfoPage extends AppPageWithAppBar {
  static const routeName = "/add_new_beneficiary_page";

  static void start() {
    // Get.toNamed(AddNewBeneficiaryPage.routeName);
  }

  final controller = Get.put(AddNewBeneficiaryController());

  @override
  String get title => 'About Wallet (Prepaid card)';

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  @override
  Widget get body {
    return SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16,horizontal: 8),
          child: AppCard(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
            const Text('Welcome to Yuva Pay - YUVA PAY is a mobile payment platform for the last mile rural population who are Un-Connected and Un-banked. YUVA PAY is a pioneer in the mobile payments field and designed a new payment platform which can be used to make payments with or without an internet connection, supporting both smart and features phones. YUVA PAY’s goal it to enable its users/customer/merchants to send and receive money, anywhere and at any time seamlessly.'),
        AppButton(
              text: 'Done',
              onPressed:(){
            Get.back();
        }),
      ],
    ),
          ),
        ));
  }
}
