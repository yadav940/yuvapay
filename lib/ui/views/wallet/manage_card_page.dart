import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';

import '../../../controller/wallete/manage_card_controller.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';

class ManageCardPage extends AppPageWithAppBar {
  static const routeName = "/manageCardPage";

  static void start() {
    Get.toNamed(ManageCardPage.routeName);
  }

  final controller = Get.put(ManageCardController());

  @override
  String get title => TranslationKeys.manageCard.tr;

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Obx(()=>AppCard(
                  child: Column(
                    children: [
                      Text(
                        TranslationKeys.transactionLimit.tr,
                        style: TextStyles.sp14(),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        '₹ ${(controller.transactionLimit*15000).round()}',
                        style: TextStyles.pageTitle(),
                      ),
                      Slider(value: controller.transactionLimit, onChanged: (value) {
                        controller.transactionLimitSet(value);
                      })
                    ],
                  ),
                )),
                const SizedBox(
                  height: 12,
                ),
                AppCard(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          TranslationKeys.enableOnlineTransactions.tr,
                          style: TextStyles.sp16(),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          TranslationKeys
                              .disablingWillBlockOnlineTransactions.tr,
                          style: TextStyles.sp14(
                              color: Palette.colorPrimaryTextG50),
                        ),
                      ],
                    ),
                    Obx(()=>Switch(
                      value: controller.enableOnlineTransaction.value,
                      activeTrackColor: Palette.messageSuccess0,
                      activeColor: Palette.colorPageBg,
                      onChanged: (value) {controller.setEnableOnlineTransaction();},
                    ))
                  ],
                ))
              ],
            ),
            PoweredByYesBankBhim()
          ],
        ),
      ),
    );
  }
}
