import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/app_translation.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/onboarding/upi/transfer_to_bank/bank_search_page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/transfer_to_upi/upi_pay_page.dart';
import 'package:yuvapay/ui/views/wallet/manage_card_page.dart';
import 'package:yuvapay/ui/views/wallet/request_wallet_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_creadit_card.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';

import '../../../controller/wallete/wallete_prepaid_card_controller.dart';
import '../../../utils/enums.dart';
import '../../usefull/palette.dart';
import '../base/page.dart';

class WalletePrepaidCardPage extends AppPageWithAppBar {
  static const routeName = "/WalletePrepaidCardPage";

  static void start() {
    Get.toNamed(WalletePrepaidCardPage.routeName);
  }

  final controller = Get.put(WalletPrepaidCardController());

  @override
  String get title => TranslationKeys.yuvaPayWalletPrepaidCard.tr;

  @override
  Color get pageBackgroundColor => Palette.colorPageBgG07;

  @override
  Widget get body {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView(
        children: [
          Obx(()=>cardDetail),
          const SizedBox(
            height: 12,
          ),
          getWalletBalance,
          const SizedBox(
            height: 12,
          ),
          getRequestList,
          const SizedBox(
            height: 12,
          ),
          const PoweredByYesBank()
        ],
      ),
    );
  }

  Widget get cardDetail {
    return AppCard(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            /*Obx(()=>),*/
            controller.cardFreeze.value?Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                image: DecorationImage(
                  image: AssetImage(AssetConstants.icCardFreeze),
                  fit: BoxFit.cover,
                ),
              ),
              child: SvgPicture.asset(AssetConstants.icCardFreeze),
            ):AppCreaditCard(cardVisible: controller.cardVisible.value,),
            const SizedBox(
              height: 24,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: (){
                    controller.setCardVisible();
                  },
                  child: Obx(()=>controller.cardVisible.value?getCardFuntion(
                      assetPath: AssetConstants.icEyeOpen,
                      text: TranslationKeys.cardDetailsOn.tr):getCardFuntion(
                      assetPath: AssetConstants.icEyeClosed,
                      text: TranslationKeys.cardDetailsOff.tr)),
                ),
                InkWell(
                  onTap: (){
                    controller.setCardFreeze();
                  },
                  child: getCardFuntion(
                      assetPath: AssetConstants.icFreeze,
                      text: TranslationKeys.freezeCardOff.tr),
                ),
                InkWell(
                  onTap: (){
                    ManageCardPage.start();
                  },
                  child: getCardFuntion(
                      assetPath: AssetConstants.icSetting,
                      text: TranslationKeys.manageCard.tr),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget getCardFuntion({required String assetPath, required String text}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 40,
          width: 40,
          padding: const EdgeInsets.all(8.0),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            color: Color.fromRGBO(247, 247, 248, 1),
          ),
          child: SvgPicture.asset(assetPath),
        ),
        const SizedBox(
          height: 8,
        ),
        SizedBox(
            width: 80,
            child: Text(
              text,
              textAlign: TextAlign.center,
            )),
      ],
    );
  }

  Widget get getWalletBalance {
    return AppCard(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Text(
                TranslationKeys.walletBalance.tr,
                style: TextStyles.sp14(),
              ),
              const SizedBox(
                height: 12,
              ),
              Text(
                '₹ 10,000',
                style: TextStyles.sp28(
                    color: Palette.colorPrimaryTextG90,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
          SizedBox(
            width: 130,
            child: AppButtonWithBorder(
              onPressed: (){
                UpiPayPage.start(page: FromUpiPayPage.topUp);
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 12.0, bottom: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Icon(
                      Icons.add,
                      color: Palette.buttonBackground,
                    ),
                    Text(
                      TranslationKeys.topUp.tr,
                      style: TextStyles.sp16(
                          fontWeight: FontWeight.bold,
                          color: Palette.buttonBackground),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget get getRequestList {
    return AppCard(
      child: Column(
        children: [
          InkWell(
            onTap: (){
              //UpiPayPage.start(page: 'Request  to Wallet');
              RequestWalletPage.start();
            },
            child: requestIteam(
                assetPath: AssetConstants.icArrowLeftDown,
                title: TranslationKeys.requestToWallet.tr),
          ),
          InkWell(
            onTap: (){
              //UpiPayPage.start(page: 'Request  to Wallet');
              BankTransferSearchPage.start();
            },
            child: requestIteam(
                assetPath: AssetConstants.icBankTransfer,
                title: TranslationKeys.transferToBank.tr),
          ),
          InkWell(
            onTap: (){
              UpiPayPage.start(page: FromUpiPayPage.sendToWallet);
            },
            child: requestIteam(
                assetPath: AssetConstants.icWallet,
                title: TranslationKeys.sendToWallet.tr),
          ),
          InkWell(
            onTap: (){

              RequestWalletPage.start();
            },
            child: requestIteam(
                assetPath: AssetConstants.icTransaction,
                title: TranslationKeys.walletTransactions.tr),
          ),
        ],
      ),
    );
  }

  Widget requestIteam({required String assetPath, required String title}) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0, bottom: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                height: 40,
                width: 40,
                padding: const EdgeInsets.all(8.0),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Color.fromRGBO(247, 247, 248, 1),
                ),
                child: SvgPicture.asset(assetPath),
              ),
              const SizedBox(
                width: 16,
              ),
              Text(
                title,
                style: TextStyles.sp16(),
              ),
            ],
          ),
          const Icon(
            Icons.arrow_forward_ios_outlined,
            color: Palette.colorPrimaryTextG90,
            size: 15,
          )
        ],
      ),
    );
  }
}
