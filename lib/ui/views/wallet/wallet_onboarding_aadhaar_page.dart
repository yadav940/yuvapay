import 'package:yuvapay/ui/views/wallet/wallet_onboarding_otp_page.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';

class WalletOnboardingAadhaarPage extends AppPageWithAppBar {
  static const routeName = "/wallet-onboarding";
  final TextEditingController _emailController = TextEditingController();

  @override
  // TODO: implement title
  String get title => TranslationKeys.completeYourKyc.tr;

  @override
  Widget get body {
    return SafeArea(
      child: _showBodyContent(),
    );
  }

  Widget _showBodyContent() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 25),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              AppTextField(
                hintText: TranslationKeys.enterYourEmailId.tr,
                textController: _emailController,
                onChange: (value) {},
                autofocus: true,
                inputType: TextInputType.text,
                borderColor: Palette.greyScaleDark7,
              ),
              const SizedBox(height: 18),
              Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                  border: Border.all(
                    color: Palette.greyScaleDark7,
                    width: 1,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: _showDropDownMenu(),
                ),
              ),
              const SizedBox(height: 18),
              AppTextField(
                hintText: TranslationKeys.enterAlternatePhoneNumber.tr,
                textController: _emailController,
                onChange: (value) {},
                autofocus: true,
                inputType: TextInputType.text,
                borderColor: Palette.greyScaleDark7,
              ),
              const SizedBox(height: 18),
              AppTextField(
                hintText: TranslationKeys.selectAnswer.tr,
                textController: _emailController,
                onChange: (value) {},
                autofocus: true,
                inputType: TextInputType.text,
                borderColor: Palette.greyScaleDark7,
              ),
              const SizedBox(height: 18),
              AppTextField(
                hintText: TranslationKeys.enterAlternatePhoneNumberOtp.tr,
                textController: _emailController,
                onChange: (value) {},
                autofocus: true,
                inputType: TextInputType.text,
                borderColor: Palette.greyScaleDark7,
              ),
              const SizedBox(height: 18),
              AppTextField(
                hintText: TranslationKeys.enterAadhaarNUmber.tr,
                textController: _emailController,
                onChange: (value) {},
                autofocus: true,
                inputType: TextInputType.text,
                borderColor: Palette.greyScaleDark7,
              ),
            ],
          ),
          _showBottomPart(),
        ],
      ),
    );
  }

  DropdownButtonFormField<String> _showDropDownMenu() {
    return DropdownButtonFormField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: TranslationKeys.selectSecurityQuestion.tr,
                    hintStyle: TextStyles.sp16(color: Palette.greyScaleDark7),
                    // suffixIcon: Icon(Icons.arrow_drop_down),
                  ),
                  dropdownColor: Colors.white,
                  items: [
                    // HARDCODED OPTIONS.
                    // ACTUAL OPTIONS WILL COME FROM APIs.
                    DropdownMenuItem(
                        child: Text(
                          "What is your city of birth?",
                          style:
                              TextStyles.sp16(color: Palette.greyScaleDark7),
                        ),
                        value: "USA"),
                    DropdownMenuItem(
                        child: Text(
                          "What is your first school name?",
                          style:
                              TextStyles.sp16(color: Palette.greyScaleDark7),
                        ),
                        value: "Canada"),
                    DropdownMenuItem(
                        child: Text(
                          "What is your favorite color?",
                          style:
                              TextStyles.sp16(color: Palette.greyScaleDark7),
                        ),
                        value: "Brazil"),
                  ],
                  onChanged: (Object? value) {
                    print(value);
                  },
                );
  }

  Widget _showBottomPart() {
    return Column(
          children: [
            Text(
              TranslationKeys.byClickingContinueYouAreText.tr,
              style: TextStyles.sp16(
                  color: Palette.buttonBackground,
                  fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 12),
            AppButton(
                text: TranslationKeys.verifyWithAadhaar.tr,
                onPressed: () {
                  Get.to(WalletOnboardingOtpPage());
                }),
          ],
        );
  }
}
