import 'dart:ui';

import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/home/home_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_padding.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';

import '../../resources/asset_ constants.dart';
import '../../usefull/palette.dart';
import '../base/base.dart';
import '../base/page.dart';



class TransferSuccessPage extends AppPageWithAppBar {
  static const routeName = "/upi_transfer_success_page";

  static void start() {
    // Get.toNamed(UpiTransferSuccessPage.routeName);
  }

  @override
  double? get toolbarHeight => 0;

  //final controller = Get.put(UpiSearchController());

  @override
  Widget get body {
    return SafeArea(
      child: getBody,
    );
  }

  Widget get getBody {
    return AppPadding(
      child: Column(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Lottie.asset(AssetConstants.animPaymentSuccess,
                    height: 170, repeat: false),
                const SizedBox(
                  height: 2,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: Text(
                    'Successfully transffered ₹1000 to Bank Account.',
                    style: TextStyles.sp18(),
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(
                  height: 24,
                ),
                // getError,
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 100),
                  child: AppButtonWithBorder(
                    child: Text(TranslationKeys.viewDetails.tr,style: TextStyles.sp14(color: Palette.colorPrimaryTextG70,fontWeight: FontWeight.w500),),
                    radius: 40,
                    color: Palette.colorPrimaryTextG70,
                  ),
                ),
                const SizedBox(
                  height: 24,
                ),
                Row(
                  children: [],
                )
              ],
            ),
          ),
          Column(
            children: [
              const PoweredByYesBankBhim(),
              const SizedBox(
                height: 16,
              ),
              AppButton(
                onPressed: HomePage.start,
                text: TranslationKeys.done.tr,
              )
            ],
          )
        ],
      ),
    );
  }

  Widget get getError {
    return Padding(
      padding: const EdgeInsets.only(left: 30.0,right: 30),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: AppButtonWithBorder(
              child: Text(TranslationKeys.viewDetails.tr,style: TextStyles.sp14(color: Palette.colorPrimaryTextG70,fontWeight: FontWeight.w500),),
              radius: 40,
              color: Palette.colorPrimaryTextG70,
            ),
          ),
          const SizedBox(width: 8),
          Expanded(
            flex: 1,
            child: AppButtonWithBorder(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(AssetConstants.icShare),
                  const SizedBox(width: 10,),
                  Text(TranslationKeys.share.tr,style: TextStyles.sp14(color: Palette.colorPrimaryTextG70,fontWeight: FontWeight.w500),)
                ],
              ),
              radius: 40,
              color: Palette.colorPrimaryTextG70,
              onPressed: () async {
                //Get.back();
                // controller.onNext();
              },
            ),
          ),
        ],
      ),
    );
  }
}
