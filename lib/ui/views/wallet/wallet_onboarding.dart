import 'package:flutter_svg/flutter_svg.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/views/wallet/reset_yuvapay_pin_page.dart';
import 'package:yuvapay/ui/views/wallet/transaction_details_page.dart';
import 'package:yuvapay/ui/views/wallet/wallet_onboarding_aadhaar_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';
import 'onboarding_info_page.dart';

class WalletOnboardingPage extends AppPageWithAppBar {
  static const routeName = "/wallet-onboarding";

  @override
  // TODO: implement title
  String get title => TranslationKeys.yuvaPayWallet.tr;

  @override
  Widget get body {
    return SafeArea(
      child: _showBodyContent(),
    );
  }

  Widget _showBodyContent() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _showTopPart(),
          _showBottomPart(),
        ],
      ),
    );
  }

  Column _showTopPart() {
    return Column(
      children: [
        Row(
          children: [
            Text(TranslationKeys.completeYourKyc.tr,
                style: TextStyles.sp22(fontWeight: FontWeight.w700))
          ],
        ),
        const SizedBox(height: 40),
        InkWell(
            onTap: () {
              Get.to(ResetYuvaPayPinPage());
            },
            child: Image.asset(AssetConstants.kycImage)),
        const SizedBox(height: 45),
        InkWell(
          onTap: () {
            TransactionDetailsPage.start();
          },
          child: AppCard(
              child: Column(
            children: [
              _showTickTiles(TranslationKeys.asPerRbiText.tr),
              const SizedBox(height: 18),
              _showTickTiles(TranslationKeys.getMonthlyLimitText.tr),
              const SizedBox(height: 18),
              _showTickTiles(TranslationKeys.useYourEKycText.tr),
            ],
          )),
        ),
      ],
    );
  }

  Widget _showTickTiles(String text) {
    return Row(
      children: [
        SvgPicture.asset(AssetConstants.walletTick),
        const SizedBox(width: 18),
        Expanded(
            flex: 1,
            child: Text(
              text,
              style: TextStyles.sp16(),
            )),
      ],
    );
  }

  Widget _showBottomPart() {
    return Column(
      children: [
        Row(
          children: [
            SvgPicture.asset(AssetConstants.walletLock),
            const SizedBox(width: 18),
            _showTAndC(),
          ],
        ),
        const SizedBox(height: 12),
        AppButton(
            text: TranslationKeys.verifyWithAadhaar.tr,
            onPressed: () {
              Get.to(WalletOnboardingAadhaarPage());
            }),
      ],
    );
  }

  Widget _showTAndC() {
    return Expanded(
        flex: 1,
        child: InkWell(
          onTap: () {
            Get.to(OnBoardingInfoPage());
          },
          child: RichText(
            text: TextSpan(
                text: TranslationKeys.byClickingText.tr,
                style: TextStyles.sp16(),
                children: [
                  TextSpan(
                    text: TranslationKeys.termsAndConditions.tr,
                    style: TextStyles.sp16(
                        color: Palette.buttonBackground,
                        fontWeight: FontWeight.w500),
                  ),
                ]),
          ),
        ));
  }
}
