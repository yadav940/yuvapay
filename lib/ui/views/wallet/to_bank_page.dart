import 'package:flutter_svg/flutter_svg.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/views/wallet/reset_yuvapay_pin_page.dart';
import 'package:yuvapay/ui/views/wallet/transfer_add_benificiary.dart';
import 'package:yuvapay/ui/views/wallet/wallet_onboarding_aadhaar_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';

class ToBankPage extends AppPageWithAppBar {
  static const routeName = "/wallet-onboarding";

  @override
  // TODO: implement title
  String get title => 'Transfer To Bank';

  @override
  Widget get body {
    return SafeArea(
      child: _showBodyContent(),
    );
  }

  Widget _showBodyContent() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              itemCount: 2,
              itemBuilder: (BuildContext context, int index) {
                return _showAccountTile(index);
              }),
          AppCard(
            // decoration: _tileDecoration(),
            child: InkWell(
              onTap: () {
                Get.to(TransferAddBenificiary());
                // Get.toNamed(AccountDetailsPage.routeName);
              },
              child: ListTile(
                // contentPadding:
                //     const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
                title: Text(
                  'Add a new bank account',
                  style: TextStyles.sp14(
                      fontWeight: FontWeight.w400,
                      color: Palette.colorTextLabel),
                ),
                leading: SizedBox(
                  height: 48,
                  width: 48,
                  child: Image.asset(
                    AssetConstants.addNewBankAccount,
                  ),
                ),
                trailing: const Icon(
                  Icons.chevron_right,
                  color: Palette.greyScaleDark6,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _showAccountTile(int index) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: AppCard(
        // decoration: _tileDecoration(),
        child: InkWell(
          onTap: () {
            // Get.toNamed(AccountDetailsPage.routeName);
          },
          child: ListTile(
            // contentPadding:
            //     const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            title: Text('Axis Bank',
                style: TextStyles.sp16(fontWeight: FontWeight.w500)),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'XX 1188',
                  style: TextStyles.sp14(
                      fontWeight: FontWeight.w400,
                      color: Palette.colorTextLabel),
                ),
                index == 0
                    ? Column(
                  children: [
                    const SizedBox(height: 4 ),
                    Text(
                      TranslationKeys.defaultAccount.tr,
                      style: TextStyles.sp14(
                          fontWeight: FontWeight.w400,
                          color: Palette.colorTextLabel),
                    ),
                  ],
                )
                    : const SizedBox(height: 0),
              ],
            ),
            leading: SizedBox(
              height: 48,
              width: 48,
              child: Image.asset(
                '${AssetConstants.linkedBanksImageBase}$index${AssetConstants.pngIconExtension}',
              ),
            ),
            trailing: const Icon(
              Icons.chevron_right,
              color: Palette.greyScaleDark6,
            ),
          ),
        ),
      ),
    );
  }

  BoxDecoration _tileDecoration() {
    return const BoxDecoration(
        color: Palette.colorPageBg,
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }


}