import 'package:flutter_svg/svg.dart';

import '../../resources/asset_ constants.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';

class AppCreaditCard extends StatelessWidget {
  // AppCreaditCard({Key? key}) : super(key: key);
  bool? cardVisible = false;

  AppCreaditCard({this.cardVisible});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        image: DecorationImage(
          image: AssetImage(AssetConstants.walletCardBackground),
          fit: BoxFit.cover,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 22.0, right: 22.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 30,
                  width: 30,
                  child: Image.asset(
                    AssetConstants.yuvaPayLogo,
                  ),
                ),
                Text(
                  TranslationKeys.prepaidCard.tr,
                  style: TextStyles.sp18(color: Palette.colorPageBg),
                ),
                SizedBox(
                  height: 30,
                  width: 30,
                  child: SvgPicture.asset(
                    AssetConstants.icVisa,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 55,
            ),
            Text(
              cardVisible! ? '9999 8888 7777 6666' : '****  ****  ****  8888',
              style: TextStyles.sp18(
                  color: Palette.colorPageBg, fontWeight: FontWeight.w700),
            ),
            const SizedBox(
              height: 55,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  cardVisible! ? '333' : '****',
                  style: TextStyles.sp18(
                      color: Palette.colorPageBg, fontWeight: FontWeight.w700),
                ),
                Text(
                  '07/22',
                  style: TextStyles.sp15(
                      color: Palette.colorPageBg, fontWeight: FontWeight.w700),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}

class Singleton {
  static final Singleton _singleton = Singleton._internal();

  factory Singleton() {
    return _singleton;
  }

  Singleton._internal();
}
