

import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';

class PoweredByYesBankBhim extends StatelessWidget {
  const PoweredByYesBankBhim({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(AssetConstants.icPoweredByYesbankBhimUpi);
  }
}

class PoweredByYesBank extends StatelessWidget {
  const PoweredByYesBank({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(AssetConstants.icPoweredByYesbank);
  }
}
