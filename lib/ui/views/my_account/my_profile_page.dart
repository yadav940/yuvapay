import 'dart:ui';

import 'package:flutter_svg/svg.dart';
import 'package:yuvapay/controller/my_account/my_profile_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';

import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';

class MyProfilePage extends AppPageWithAppBar {
  // static const routeName = "/my-account-profile";

  final controller = Get.put(MyProfileController());

  static void start() {
    // navigateOffAll(routeName);
  }

  @override
  // TODO: implement title
  String get title => TranslationKeys.myProfile.tr;

  @override
  Color get pageBackgroundColor => Palette.colorCardBg;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: _showBody(),
      ),
    );
  }

  Widget _showBody() {
    return Column(
      children: [
        AppCard(
          // decoration: const BoxDecoration(
          //     color: Palette.colorPageBg,
          //     borderRadius: BorderRadius.all(Radius.circular(12))),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 24),
            child: ListView(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              children: [
                Obx(() {
                  return SizedBox(
                      height: 120,
                      width: 120,
                      child: Image.asset(controller.isKycNeeded.value
                          ? 'assets/images/png/dummy_profile_image.png'
                          : AssetConstants.myAccountProfileImage));
                }),
                const SizedBox(height: 10),
                Center(
                  child: Obx(() {
                    return Text(
                        controller.isKycNeeded.value
                            ? '--Stranger--'
                            : 'Prahlad kumar',
                        style: TextStyles.sp18(fontWeight: FontWeight.w500));
                  }),
                ),
                const SizedBox(height: 10),
                Center(
                    child: Text('9988383838',
                        style: TextStyles.sp16(fontWeight: FontWeight.w400))),
              ],
            ),
          ),
        ),
        const SizedBox(height: 12),
        AppCard(
          // decoration: const BoxDecoration(
          //     color: Palette.colorPageBg,
          //     borderRadius: BorderRadius.all(Radius.circular(12))),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 24),
            child: ListView(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              children: [
                Text(
                  TranslationKeys.emailAddress.tr,
                  style: TextStyles.sp16(fontWeight: FontWeight.w400),
                ),
                const SizedBox(height: 4),
                Container(
                  decoration: BoxDecoration(
                      border:
                          Border.all(color: Palette.colorTextLabel, width: 1),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(12))),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 14),
                  child: Text('contactprahlad@gmail.com',
                      style: TextStyles.sp16(fontWeight: FontWeight.w400)),
                ),
                const SizedBox(height: 22),
                Obx(() {
                  return controller.isKycNeeded.value
                      ? AppButton(
                          text: TranslationKeys.finishYourKyc.tr,
                          onPressed: () {
                            controller.isKycNeeded(false);
                          },
                        )
                      : Row(
                          children: [
                            SvgPicture.asset(AssetConstants.defaultTickImage),
                            const SizedBox(width: 6),
                            Text(TranslationKeys.yuvapayWalletActivated.tr,
                                style: TextStyles.sp18(
                                    fontWeight: FontWeight.w500)),
                          ],
                        );
                }),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
