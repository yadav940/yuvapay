import 'dart:ui';

import 'package:yuvapay/ui/views/widgets/app_card.dart';

import '../../../controller/my_account/yuvapay_policies_controller.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';

class YuvapayPoliciesPage extends AppPageWithAppBar {
  // static const routeName = "/my-account-profile";

  final controller = Get.put(YuvapayPoliciesController());

  static void start() {
    // navigateOffAll(routeName);
  }

  @override
  String get title => 'Yuvapay Policies';

  @override
  Color get pageBackgroundColor => Palette.colorCardBg;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: _showBody(),
      ),
    );
  }

  Widget _showBody() {
    return AppCard(
      // decoration: const BoxDecoration(
      //     color: Palette.colorPageBg,
      //     borderRadius: BorderRadius.all(Radius.circular(12))),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
        child: _showOptions(),
      ),
    );
  }

  Widget _showOptions() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const ClampingScrollPhysics(),
        itemCount: 13,
        itemBuilder: (BuildContext context, int index) {
          return _showListTile(index);
        });
  }

  Widget _showListTile(int index) {
    return InkWell(
      onTap: () {},
      child: ListTile(
        contentPadding: const EdgeInsets.all(0),

        title: Text(
          controller.yuvapayPoliciesOptions[index],
          style: TextStyles.sp16(fontWeight: FontWeight.w400),
        ),
        trailing:const Icon(Icons.chevron_right),
      ),
    );
  }
}
