import 'package:flutter/services.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';

import '../../../controller/my_account/reset_via_email_phone_controller.dart';
import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';

class ResetViaEmailPhonePage extends AppPageWithAppBar {
  static const routeName = "/reset-via-phone";
  final _isPhone = Get.arguments[0];

  static void start() {
    navigateOffAll(routeName);
    // var data=Get.arguments;
    // print(data);
  }

  final controller = Get.put(ResetViaEmailPhoneController());

  @override
  // TODO: implement title
  String get title => TranslationKeys.security.tr;

  @override
  Color get pageBackgroundColor => Palette.colorCardBg;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: _showBody(),
      ),
    );
  }

  Widget _showBody() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ListView(
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          children: [
            AppCard(
              // decoration: const BoxDecoration(
              //     color: Palette.colorPageBg,
              //     borderRadius: BorderRadius.all(Radius.circular(12))),
              padding: const EdgeInsets.all(16),
              child: ListView(
                shrinkWrap: true,
                physics: const ClampingScrollPhysics(),
                children: [
                  Text(
                    _isPhone ? 'Alternate Phone Number' : 'Email Addeess',
                    style: TextStyles.sp16(fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(height: 20),
                  Obx(() {
                    return AppTextField(
                      hintText: _isPhone ? "000 000 0000" : "abc@gmail.com",
                      textController: controller.textController,
                      onChange: (value) {},
                      autofocus: true,
                      inputFormatters:
                          _isPhone ? [LengthLimitingTextInputFormatter(10)] : [],
                      inputType: _isPhone
                          ? TextInputType.phone
                          : TextInputType.emailAddress,
                      borderColor: controller.isError.value
                          ? Palette.colorError
                          : Palette.greyScaleDark7,
                    );
                  }),
                  // const SizedBox(height: 8),
                  Obx(() {
                    return controller.isError.value
                        ? Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                              controller.errorMessage.value,
                              style: TextStyles.sp12(color: Palette.colorError),
                            ),
                        )
                        : const Text('');
                  }),
                ],
              ),
            ),
          ],
        ),
        AppButton(
          text: TranslationKeys.button_contnue.tr,
          onPressed: () {
            var data = Get.arguments;
            print(data);
            controller.next(_isPhone);
          },
        )
      ],
    );
  }
}
