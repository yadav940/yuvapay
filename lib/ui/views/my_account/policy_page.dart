import 'dart:ui';

import 'package:flutter_svg/svg.dart';
import 'package:yuvapay/controller/my_account/policy_controller.dart';
import 'package:yuvapay/ui/views/my_account/yuvapay_policies_page.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';

import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';

class PolicyPage extends AppPageWithAppBar {
  // static const routeName = "/my-account-profile";

  final controller = Get.put(PolicyController());

  static void start() {
    // navigateOffAll(routeName);
  }

  @override
  // TODO: implement title
  String get title => 'Policies';

  @override
  Color get pageBackgroundColor => Palette.colorCardBg;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: _showBody(),
      ),
    );
  }

  Widget _showBody() {
    return AppCard(
      // decoration: const BoxDecoration(
      //     color: Palette.colorPageBg,
      //     borderRadius: BorderRadius.all(Radius.circular(12))),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
        child: _showOptions(),
      ),
    );
  }

  Widget _showOptions() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const ClampingScrollPhysics(),
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return _showListTile(index);
        });
  }

  Widget _showListTile(int index) {
    return InkWell(
      onTap: () {
        if(index==0){
          Get.to(YuvapayPoliciesPage());
        }
      },
      child: ListTile(
        contentPadding: const EdgeInsets.all(0),
        leading: SizedBox(
          height: 40,
          width: 40,
          child: SvgPicture.asset(
              'assets/images/svg/my_account_module/policy$index.svg'),
        ),
        title: Text(
          controller.policyOptions[index],
          style: TextStyles.sp16(fontWeight: FontWeight.w400),
        ),
        trailing:const Icon(Icons.chevron_right),
      ),
    );
  }
}
