import 'package:yuvapay/controller/my_account/reset_via_security_question_controller.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';

import '../../resources/translations/translations_key.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';

class ResetViaSecurityQuestionPage extends AppPageWithAppBar {
  // static const routeName = "/reset-via-phone";

  static void start() {
    // navigateOffAll(routeName);
    // var data=Get.arguments;
    // print(data);
  }

  final controller = Get.put(ResetViaSecurityQuestionController());


  @override
  // TODO: implement title
  String get title => TranslationKeys.resetPinViaSecurityQuestions.tr;

  @override
  Color get pageBackgroundColor => Palette.colorCardBg;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: _showBody(),
      ),
    );
  }

  Widget _showBody() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ListView(
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          children: [
            AppCard(
              // decoration: const BoxDecoration(
              //     color: Palette.colorPageBg,
              //     borderRadius: BorderRadius.all(Radius.circular(12))),
              padding: const EdgeInsets.all(8),
              child: Row(
                children: [
                  Radio(
                    value: 1,
                    onChanged: (value) {
                    }, groupValue: 1,
                    activeColor: Colors.green,

                  ),
                  // const SizedBox(width: 8),
                  Text(
                    'What is the name of first school?',
                    style: TextStyles.sp16(fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 8),
            Container(
              decoration: const BoxDecoration(
                  color: Palette.colorPageBg,
                  borderRadius: BorderRadius.all(Radius.circular(12))),
              padding: const EdgeInsets.all(16),
              child: ListView(
                shrinkWrap: true,
physics: const ClampingScrollPhysics(),
// crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    TranslationKeys.yourAnswerCaps.tr,
                    style: TextStyles.sp16(fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(height: 20),
                  Text(
                    'What is the name of first school?',
                    style: TextStyles.sp12(fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(height: 4),
                  Obx(() {
                    return AppTextField(
                      hintText:TranslationKeys.yourAnswer.tr,
                      textController: controller.textController,
                      onChange: (value) {},
                      autofocus: true,
                      inputType:TextInputType.text,
                      borderColor: controller.isError.value
                          ? Palette.colorError
                          : Palette.greyScaleDark7,
                    );
                  }),
                  const SizedBox(height: 8),
                  Obx(() {
                    return controller.isError.value
                        ? Text(
                      controller.errorMessage.value,
                      style: TextStyles.sp12(color: Palette.colorError),
                    )
                        : const Text('');
                  }),
                ],
              ),
            ),
          ],
        ),
        AppButton(
          text: TranslationKeys.button_contnue.tr,
          onPressed: () {
            var data = Get.arguments;
            print(data);
            controller.next();
          },
        )
      ],
    );
  }

}