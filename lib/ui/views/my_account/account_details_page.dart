import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';
import 'package:yuvapay/controller/my_account/account_details_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import 'package:yuvapay/ui/views/widgets/app_text_field.dart';
import 'package:yuvapay/ui/views/widgets/app_title.dart';
import 'package:yuvapay/ui/views/widgets/popup_utils.dart';
import 'package:yuvapay/ui/views/widgets/powered_by_yesbank_bhim_upi.dart';

class AccountDetailsPage extends AppPageWithAppBar {
  static const routeName = "/account-details";

  static void start() {
    //navigateOffAll(routeName);
    Get.toNamed(AccountDetailsPage.routeName);
  }

  final controller = Get.put(AccountDetailsController());

  @override
  ConvexAppBar? get bottomNavigationBar => null;

  @override
  // TODO: implement title
  String get title => TranslationKeys.accountDetails.tr;

  @override
  // TODO: implement actions
  List<Widget>? get actions => [
        IconButton(
          visualDensity: VisualDensity.compact,
          splashRadius: 24,
          onPressed: Get.back,
          icon: const Icon(Icons.share),
        ),
      ];

  @override
  Color get pageBackgroundColor => Palette.colorCardBg;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: SingleChildScrollView(
          child: _showContent(),
        ),
      ),
    );
  }

  Widget _showContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _showAccountHeader(),
        const SizedBox(height: 12),
        _showAccountInfo(),
        const SizedBox(height: 12),
        _showUpiPinSection(),
        _showBalanceAndUpiSection(),
        const SizedBox(height: 18),
        _showUnlinkAccountSection()
      ],
    );
  }

  Widget _showUnlinkAccountSection() {
    return Text(
      TranslationKeys.unlinkBankAccount.tr,
      style: TextStyles.sp14(
          fontWeight: FontWeight.w500, color: Palette.buttonBackground),
    );
  }

  Widget _showBalanceAndUpiSection() {
    return Obx(() {
      return controller.isPinExist.value
          ? Column(
              children: [
                const SizedBox(height: 12),
                _showBalanceSection(),
                const SizedBox(height: 12),
                _showUpiIdSection(),
              ],
            )
          : const SizedBox(height: 0);
    });
  }

  Widget _showAccountHeader() {
    return AppCard(
      // width: MediaQuery.of(Get.context!).size.width,
      padding: const EdgeInsets.all(16),
      // decoration: _tileDecoration(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _showAccountHeaderContent(),
        ],
      ),
    );
  }

  Widget _showAccountHeaderContent() {
    return Column(
      children: [
        _showBankImage(),
        const SizedBox(height: 12),
        _showBankName(),
        const SizedBox(height: 4),
        _showAccountNumber(),
        const SizedBox(height: 6),
        Obx(() {
          return !controller.isDefault.value
              ? _showSetAsDefaultSection()
              : _showDefaultWIthTickSection();
        }),
        const SizedBox(height: 8),
        Obx(() {
          return controller.isPinExist.value
              ? _showQrCodeSection()
              : const SizedBox(height: 0);
        }),
      ],
    );
  }

  Widget _showQrCodeSection() {
    return Container(
      width: 150,
      decoration: const BoxDecoration(
          color: Palette.colorCardBg,
          borderRadius: BorderRadius.all(Radius.circular(20))),
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Icons.qr_code,
            size: 18,
            color: Palette.colorPrimaryTextG90,
          ),
          const SizedBox(width: 10),
          Text(
            TranslationKeys.viewQrCode.tr,
            style: TextStyles.sp14(fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }

  Widget _showDefaultWIthTickSection() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            TranslationKeys.defaultAccount.tr,
            style: TextStyles.sp14(fontWeight: FontWeight.w400),
          ),
          const SizedBox(width: 8),
          SvgPicture.asset(AssetConstants.defaultTickImage),
        ],
      ),
    );
  }

  Widget _showSetAsDefaultSection() {
    return InkWell(
      onTap: () {
        controller.isDefault(true);
      },
      child: Container(
        width: 150,
        decoration: const BoxDecoration(
            color: Palette.colorCardBg,
            borderRadius: BorderRadius.all(Radius.circular(20))),
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 8),
        child: Center(
          child: Text(
            TranslationKeys.setAsDefault.tr,
            style: TextStyles.sp14(fontWeight: FontWeight.w500),
          ),
        ),
      ),
    );
  }

  Widget _showAccountNumber() {
    return Text(
      TranslationKeys.xx1188.tr,
      style: TextStyles.sp14(fontWeight: FontWeight.w400),
    );
  }

  Widget _showBankName() {
    return Text(
      TranslationKeys.canaraBank.tr,
      style: TextStyles.sp18(fontWeight: FontWeight.w500),
    );
  }

  Widget _showBankImage() {
    return SizedBox(
      height: 48,
      width: 48,
      child: Image.asset(AssetConstants.canaraBankImage),
    );
  }

  Widget _showAccountInfo() {
    return AppCard(
      // width: MediaQuery.of(Get.context!).size.width,
      padding: const EdgeInsets.all(16),
      // decoration: _tileDecoration(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            TranslationKeys.accountInformation.tr,
            style: TextStyles.sp16(fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 4),
          Table(
            children: [
              _showInfoTable(
                  TranslationKeys.bankingName.tr, controller.dummyName),
              _showInfoTable(
                  TranslationKeys.phoneNumber.tr, controller.dummyPhone),
              _showInfoTable(
                  TranslationKeys.accountType.tr, controller.dummyAccountType),
              _showInfoTable(TranslationKeys.branch.tr, controller.dummyBranch),
              _showInfoTable(TranslationKeys.ifsc.tr, controller.dummyIfsc),
            ],
          ),
        ],
      ),
    );
  }

  TableRow _showInfoTable(String heading, String content) {
    return TableRow(children: [
      Text(heading, style: TextStyles.sp14(fontWeight: FontWeight.w400)),
      Text(controller.tableSeparator,
          style: TextStyles.sp14(fontWeight: FontWeight.w400)),
      Text(content, style: TextStyles.sp14(fontWeight: FontWeight.w400)),
    ]);
  }

  Widget _showUpiPinSection() {
    return AppCard(
      // width: MediaQuery.of(Get.context!).size.width,
      padding: const EdgeInsets.all(16),
      // decoration: _tileDecoration(),
      child: _showUpiPinSectionContent(),
    );
  }

  Widget _showUpiPinSectionContent() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _showUpiPinStatus(),
        _showUpiPinOptions(),
      ],
    );
  }

  Widget _showUpiPinOptions() {
    return Obx(() {
      return controller.isPinExist.value
          ? Row(
              children: [
                Container(
                  decoration: const BoxDecoration(
                      color: Palette.buttonBackground,
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    child: Text(
                      TranslationKeys.reset.tr,
                      style: TextStyles.sp14(
                          fontWeight: FontWeight.w500,
                          color: Palette.colorPageBg),
                    ),
                  ),
                ),
                const SizedBox(width: 16),
                Container(
                  decoration: const BoxDecoration(
                      color: Palette.buttonBackground,
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    child: Text(
                      TranslationKeys.change.tr,
                      style: TextStyles.sp14(
                          fontWeight: FontWeight.w500,
                          color: Palette.colorPageBg),
                    ),
                  ),
                ),
              ],
            )
          : _showSetPinButton();
    });
  }

  Widget _showSetPinButton() {
    return InkWell(
      onTap: onPinPopup,
      child: Container(
        decoration: const BoxDecoration(
            color: Palette.buttonBackground,
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Text(
            TranslationKeys.setPin.tr,
            style: TextStyles.sp14(
                fontWeight: FontWeight.w500, color: Palette.colorPageBg),
          ),
        ),
      ),
    );
  }

  Widget _showUpiPinStatus() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          TranslationKeys.upiPin.tr,
          style: TextStyles.sp16(fontWeight: FontWeight.w500),
        ),
        const SizedBox(height: 4),
        Text(
          TranslationKeys.upiPinExists.tr,
          style: TextStyles.sp14(fontWeight: FontWeight.w500),
        ),
      ],
    );
  }

  Widget _showBalanceSection() {
    return AppCard(
      padding: const EdgeInsets.all(16),
      // decoration: _tileDecoration(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            TranslationKeys.balance.tr,
            style: TextStyles.sp16(fontWeight: FontWeight.w500),
          ),
          Obx(() {
            return controller.isShowBalance.value
                ? Padding(
                    padding: const EdgeInsets.symmetric(vertical: 3.0),
                    child: Text(
                      controller.dummyBalance,
                      style: TextStyles.sp20(
                          fontWeight: FontWeight.w700,
                          color: Palette.colorPrimaryTextG90),
                    ),
                  )
                : InkWell(
                    onTap: () {
                      controller.isShowBalance(true);
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                          color: Palette.buttonBackground,
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 16),
                        child: Text(
                          TranslationKeys.checkBalance.tr,
                          style: TextStyles.sp14(
                              fontWeight: FontWeight.w500,
                              color: Palette.colorPageBg),
                        ),
                      ),
                    ),
                  );
          }),
        ],
      ),
    );
  }

  Widget _showUpiIdSection() {
    return AppCard(
      padding: const EdgeInsets.all(16),
      // decoration: _tileDecoration(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            TranslationKeys.upiId.tr,
            style: TextStyles.sp16(fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 4),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                controller.dummyUpiId,
                style: TextStyles.sp14(fontWeight: FontWeight.w500),
              ),
              Text(
                TranslationKeys.active.tr,
                style: TextStyles.sp14(fontWeight: FontWeight.w500),
              ),
            ],
          ),
        ],
      ),
    );
  }

  BoxDecoration _tileDecoration() {
    return const BoxDecoration(
        color: Palette.colorPageBg,
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }

  void onPinPopup() {
    Future.delayed(const Duration(milliseconds: 1200), () {
      controller.isPinExist(true);
    });
    showAppBottomSheet(
        initialChildSize: 0.6,
        child: getCardDetail,
        dragPickColor: Colors.transparent);
  }

  Widget get getCardDetail {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: const RotationTransition(
                  child: Icon(
                    Icons.add,
                    color: Palette.colorPrimaryTextG90,
                    size: 24,
                  ),
                  turns: AlwaysStoppedAnimation(45 / 360),
                ),
              ),
              AppTitle(titleText: TranslationKeys.debitCreditCardDetails.tr)
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              color: Palette.greyScaleDark8,
              border: Border.all(
                color: const Color.fromRGBO(229, 230, 231, 1),
                width: 1,
              ),
            ),
            height: 64,
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: Row(
              children: [
                Image.asset(AssetConstants.canaraBankImage),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      TranslationKeys.canaraBank.tr,
                      style: TextStyles.sp16(),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text(
                      TranslationKeys.xx1188.tr,
                      style: TextStyles.sp14(),
                    )
                  ],
                )
              ],
            ),
          ),
          const SizedBox(
            height: 24,
          ),
          Text(
            TranslationKeys.lastDigitsOfYourCardNumber.tr,
            style: TextStyles.sp12(),
          ),
          const SizedBox(
            height: 4,
          ),
          AppTextField(
            textController: controller.cardNumController,
            hintText: 'XXXXXX',
            autofocus: true,
            prefixText: 'XXXXXX  ',
            inputFormatters: [
              LengthLimitingTextInputFormatter(6),
            ],
            inputType: TextInputType.number,
              onChange: (text){

              }
          ),
          const SizedBox(
            height: 24,
          ),
          Text(
            TranslationKeys.expiryDate.tr,
            style: TextStyles.sp12(),
          ),
          const SizedBox(
            height: 4,
          ),
          AppTextField(
            textController: controller.expiryDateController,
            hintText: controller.mmyyHinteText,
            inputType: TextInputType.number,
            onChange: (text){
              int lengthText=controller.expiryDateController.text.length;
              String tempText='';
              // if(text.length==2){
              //   controller.expiryDateController.text=controller.expiryDateController.text+"/";
              //   controller.expiryDateController.selection=TextSelection.fromPosition(TextPosition(offset: controller.expiryDateController.text.length));
              // }
              if(lengthText==3 && controller.expiryDateController.text[lengthText-1]!='/'){
                var tempChar=controller.expiryDateController.text[2];
                tempText=controller.expiryDateController.text;
                tempText=tempText.substring(0, tempText.length - 1);
                tempText=tempText+"/";
                controller.expiryDateController.text=tempText;
                controller.expiryDateController.text=controller.expiryDateController.text+tempChar;
                controller.expiryDateController.selection=TextSelection.fromPosition(TextPosition(offset: controller.expiryDateController.text.length));

              }
            },
            inputFormatters: [
              LengthLimitingTextInputFormatter(5),
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          AppButton(
            onPressed: () {
              Get.back();
              // Navigator.pop(Get.context!);
              // errorSuccessPopup(isError: true);
            },
            text: TranslationKeys.proceed.tr,
          ),
          const SizedBox(
            height: 24,
          ),
          const PoweredByYesBankBhim()
        ],
      ),
    );
  }

  void errorSuccessPopup({required bool isError}) {
    showAppBottomSheet(
      initialChildSize: 0.4,
      child: Column(
        children: [
          Lottie.asset(AssetConstants.animUpiAddAankSuccess),
          const SizedBox(
            height: 24,
          ),
        ],
      ),
    );
  }
}
