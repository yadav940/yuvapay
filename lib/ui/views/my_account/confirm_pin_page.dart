import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/my_account/account_security_page.dart';
import 'package:yuvapay/ui/views/my_account/my_account_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/popup_utils.dart';

class ConfirmPinPage extends AppPageWithAppBar {
  static const routeName = "/change-pin";

  TextEditingController passTextController = TextEditingController();
  TextEditingController repassTextController = TextEditingController();
  String repass='';

  static void start() {}

  @override
  double? get toolbarHeight => 0;

  // TODO: implement appBar
  PreferredSizeWidget? get appBar => AppBar(
      backgroundColor: Palette.buttonBackground,
      leading: IconButton(
        visualDensity: VisualDensity.compact,
        splashRadius: 24,
        onPressed: Get.back,
        icon: const Icon(Icons.arrow_back_outlined),
      ),
      title: Text(TranslationKeys.confirmPin.tr));

  @override
  Widget get body {
    return SafeArea(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _showChangePinSection(),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
          child: AppButton(
            text: 'Change Pin',
            onPressed: () async {
              if (repass == passTextController.text) {
                showAppBottomSheet(
                  dragPickColor: Colors.transparent,
                  initialChildSize: 0.4,
                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: _showChangePinSuccess,
                  ),
                );
              } else {
                showAppBottomSheet(
                  dragPickColor: Colors.transparent,
                  initialChildSize: 0.4,

                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: _showWrongPin,
                  ),
                );
              }
            },
          ),
        ),
      ],
    ));
  }

  Widget _showChangePinSection() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 28, horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            TranslationKeys.newPin.tr,
            style: TextStyles.sp18(fontWeight: FontWeight.w400),
          ),
          const SizedBox(height: 18),
          PinCodeTextField(
            hideCharacter: true,
            maxLength: 6,
            autofocus: true,
            highlightColor: Colors.red,
            defaultBorderColor: Palette.greyScaleDark6,
            hasTextBorderColor: Palette.psMain,
            pinBoxRadius: 12,
            pinBoxHeight: 56,
            pinBoxWidth: 48,
            pinBoxBorderWidth: 0.000001,
            keyboardType: TextInputType.number,
            controller: passTextController,
            hasUnderline: true,
            onDone: (_) {},
            onTextChanged: (_) {},
          ),
          const SizedBox(height: 44),
          Text(
            TranslationKeys.confirmPin.tr,
            style: TextStyles.sp18(fontWeight: FontWeight.w400),
          ),
          const SizedBox(height: 18),
          PinCodeTextField(
            hideCharacter: true,
            maxLength: 6,
            autofocus: true,
            highlightColor: Colors.red,
            defaultBorderColor: Palette.greyScaleDark6,
            hasTextBorderColor: Palette.psMain,
            pinBoxRadius: 12,
            pinBoxHeight: 56,
            pinBoxWidth: 48,
            pinBoxBorderWidth: 0.001,
            keyboardType: TextInputType.number,
            controller: repassTextController,
            hasUnderline: true,
            onDone: (_) {
              repass=_;
              // if (_ == passTextController.text) {
              //   showAppBottomSheet(
              //     dragPickColor: Colors.transparent,
              //     initialChildSize: 0.4,
              //     child: Padding(
              //       padding: const EdgeInsets.all(18.0),
              //       child: _showChangePinSuccess,
              //     ),
              //   );
              // } else {
              //   showAppBottomSheet(
              //     dragPickColor: Colors.transparent,
              //     initialChildSize: 0.4,
              //
              //     child: Padding(
              //       padding: const EdgeInsets.all(18.0),
              //       child: _showWrongPin,
              //     ),
              //   );
              // }
            },
            onTextChanged: (_) {},
          ),
        ],
      ),
    );
  }

  final Widget _showWrongPin = Column(
    children: [
      SvgPicture.asset(AssetConstants.errorWardingSign),
      const SizedBox(height: 30),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: Text(
          TranslationKeys.passwordsDoNotMatch.tr,
          textAlign: TextAlign.center,
          style: TextStyles.sp18(
              color: Palette.colorPrimaryTextG90, fontWeight: FontWeight.w600),
        ),
      ),
      const SizedBox(height: 24),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8),
        child: AppButton(
          text: TranslationKeys.confirm.tr,
          onPressed: () async {
          },
        ),
      ),
      const SizedBox(height: 12),
      InkWell(
        onTap: () {
          Get.back();
        },
        child: Text(
          TranslationKeys.backToHome.tr,
          style: TextStyles.sp16(color: Palette.greyScaleDark6),
          textAlign: TextAlign.center,
        ),
      ),
    ],
  );

  final Widget _showChangePinSuccess = Column(
    children: [
      const SizedBox(height: 24),
      Transform.scale(
          scale: 5, child: SvgPicture.asset(AssetConstants.successSign)),
      const SizedBox(height: 60),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: Text(
          TranslationKeys.passwordsSuccessfullyReset.tr,
          textAlign: TextAlign.center,
          style: TextStyles.sp18(
              color: Palette.colorPrimaryTextG90, fontWeight: FontWeight.w600),
        ),
      ),
      const SizedBox(height: 24),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8),
        child: AppButton(
          text: TranslationKeys.okay.tr,
          onPressed: () async {
            Get.to(()=>MyAccountPage());
          },
        ),
      ),
    ],
  );
}
