import 'dart:ui';

import 'package:flutter_svg/svg.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';

import '../../../controller/my_account/help_and_support_controller.dart';
import '../../resources/asset_ constants.dart';
import '../../usefull/palette.dart';
import '../../usefull/styles/text_styles.dart';
import '../base/base.dart';
import '../base/page.dart';

class HelpAndSupportPage extends AppPageWithAppBar {
  // static const routeName = "/my-account-profile";

  final controller = Get.put(HelpAndSupportController());

  static void start() {
    // navigateOffAll(routeName);
  }

  @override
  String get title => TranslationKeys.helpAndSupport.tr;

  @override
  Color get pageBackgroundColor => Palette.colorCardBg;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: _showBody(),
      ),
    );
  }

  Widget _showBody() {
    return AppCard(
      // decoration: const BoxDecoration(
      //     color: Palette.colorPageBg,
      //     borderRadius: BorderRadius.all(Radius.circular(12))),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
        child: _showOptions(),
      ),
    );
  }

  Widget _showOptions() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const ClampingScrollPhysics(),
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return _showListTile(index);
        });
  }

  Widget _showListTile(int index) {
    return InkWell(
      onTap: () {},
      child: ListTile(
        contentPadding: const EdgeInsets.all(0),
        leading: SizedBox(
          height: 40,
          width: 40,
          child: SvgPicture.asset(
              'assets/images/svg/my_account_module/help_and_policies$index${AssetConstants.svgIconExtension}'),
        ),
        title: Text(
          controller.helpAndSupportOptions[index],
          style: TextStyles.sp16(fontWeight: FontWeight.w400),
        ),
        trailing:index==2?const Icon(Icons.call_outlined): const Icon(Icons.chevron_right),
        subtitle: index==2?Text(TranslationKeys.availableOnWorking.tr,
            style: TextStyles.sp14(
                fontWeight: FontWeight.w400, color: Palette.colorTextLabel)):null,
      ),
    );
  }
}
