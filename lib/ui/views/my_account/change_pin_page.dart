import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/my_account/confirm_pin_page.dart';
import 'package:yuvapay/ui/views/widgets/app_button.dart';
import 'package:yuvapay/ui/views/widgets/popup_utils.dart';

class ChangePinPage extends AppPageWithAppBar {
  static const routeName = "/change-pin";

  TextEditingController textController = TextEditingController();
  int _wrongPinCount = 0;

  static void start() {}

  @override
  double? get toolbarHeight => 0;

  @override
  Widget get body {
    return SafeArea(
        child: SizedBox(
            height: MediaQuery.of(Get.context!).size.height,
            width: MediaQuery.of(Get.context!).size.width,
            child: Stack(
              children: [
                _showChangePinSection(),
                Positioned(
                    top: 15,
                    left: 15,
                    child: InkWell(
                        onTap: () {
                          Get.back();
                        },
                        child: const Icon(Icons.close))),
              ],
            )));
  }

  Widget _showChangePinSection() {
    return SizedBox(
      height: MediaQuery.of(Get.context!).size.height,
      width: MediaQuery.of(Get.context!).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 140),
          SizedBox(
            height: 100,
            width: 70,
            child: Image.asset(AssetConstants.changePinIcon),
          ),
          const SizedBox(height: 32),
          Text(
            TranslationKeys.changeYuvaPayPin.tr,
            style: TextStyles.sp18(fontWeight: FontWeight.w400),
          ),
          Text(
            TranslationKeys.toResetPasswordConfirmYuvaPayPin.tr,
            style: TextStyles.sp14(
                fontWeight: FontWeight.w400,
                color: Palette.colorPrimaryTextG70),
          ),
          const SizedBox(height: 60),
          _showPinCodeTextField(),
        ],
      ),
    );
  }

  Widget _showPinCodeTextField() {
    return PinCodeTextField(
          hideCharacter: true,
          maxLength: 6,
          autofocus: true,
          highlightColor: Colors.red,
          defaultBorderColor: Palette.greyScaleDark6,
          hasTextBorderColor: Palette.psMain,
          pinBoxRadius: 12,
          pinBoxHeight: 56,
          pinBoxWidth: 48,
          pinBoxBorderWidth: 0.000001,
          keyboardType: TextInputType.number,
          controller: textController,
          hasUnderline: true,
          onDone: (_) {
            if (_ != '000000' && _wrongPinCount < 3) {
              _wrongPinCount = _wrongPinCount + 1;
              showAppBottomSheet(
                dragPickColor: Colors.transparent,
                initialChildSize: 0.4,
                maxHeight: 400,
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child:

                  // _showWrongPinSheet
                  Column(
                    children: [
                      SvgPicture.asset(AssetConstants.errorWardingSign),
                      const SizedBox(height: 30),
                      Text(
                        TranslationKeys.wrongPassword.tr,
                        style: TextStyles.sp18(
                            color: Palette.colorPrimaryTextG90, fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(height: 8),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 28),
                        child: Text(
                          TranslationKeys.youHaveTwoAttemptsLeft.tr,
                          textAlign: TextAlign.center,
                          style: TextStyles.sp16(
                              color: Palette.colorPrimaryTextG70, fontWeight: FontWeight.w400),
                        ),
                      ),
                      const SizedBox(height: 30),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: AppButton(
                              text: TranslationKeys.back.tr,
                              textStyle: TextStyles.sp16(
                                  color: Palette.buttonBackground, fontWeight: FontWeight.w700),
                              onPressed: () async {
                                Get.back();
                              },
                              style: ElevatedButton.styleFrom(
                                side: const BorderSide(
                                    width: 0.5, color: Palette.buttonBackground),
                                elevation: 0,
                                primary: Palette.colorPageBg,
                                // background
                                onPrimary: Palette.colorPageBg,
                                // foreground
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 8),
                          Expanded(
                            flex: 1,
                            child: AppButton(
                              text: TranslationKeys.retry.tr,
                              onPressed: () async {
                                // Get.back();
                                textController.clear();
                                Get.back();
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            } else if (_wrongPinCount > 2) {
              showAppBottomSheet(
                dragPickColor: Colors.transparent,
                initialChildSize: 0.4,
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: _showYuvaPayLocked,
                ),
              );
            } else if (_ == '000000') {
              Get.to(ConfirmPinPage());
            } else {
              debugPrint('Invalid Condition');
            }
          },
          onTextChanged: (_) {},
        );
  }

  final Widget _showWrongPinSheet = Column(
    children: [
      SvgPicture.asset(AssetConstants.errorWardingSign),
      const SizedBox(height: 30),
      Text(
        TranslationKeys.wrongPassword.tr,
        style: TextStyles.sp18(
            color: Palette.colorPrimaryTextG90, fontWeight: FontWeight.w600),
      ),
      const SizedBox(height: 8),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: Text(
          TranslationKeys.youHaveTwoAttemptsLeft.tr,
          textAlign: TextAlign.center,
          style: TextStyles.sp16(
              color: Palette.colorPrimaryTextG70, fontWeight: FontWeight.w400),
        ),
      ),
      const SizedBox(height: 30),
      Row(
        children: [
          Expanded(
            flex: 1,
            child: AppButton(
              text: TranslationKeys.back.tr,
              textStyle: TextStyles.sp16(
                  color: Palette.buttonBackground, fontWeight: FontWeight.w700),
              onPressed: () async {
                Get.back();
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(
                    width: 0.5, color: Palette.buttonBackground),
                elevation: 0,
                primary: Palette.colorPageBg,
                // background
                onPrimary: Palette.colorPageBg,
                // foreground
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
              ),
            ),
          ),
          const SizedBox(width: 8),
          Expanded(
            flex: 1,
            child: AppButton(
              text: TranslationKeys.retry.tr,
              onPressed: () async {
                Get.back();
              },
            ),
          ),
        ],
      ),
    ],
  );

  final Widget _showYuvaPayLocked = Column(
    children: [
      SvgPicture.asset(AssetConstants.errorWardingSign),
      const SizedBox(height: 30),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: Text(
          TranslationKeys.tooManyUnsuccessfulAttempts.tr,
          textAlign: TextAlign.center,
          style: TextStyles.sp18(
              color: Palette.colorPrimaryTextG90, fontWeight: FontWeight.w600),
        ),
      ),
      const SizedBox(height: 24),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: Text(
          TranslationKeys.closingAppInFiveSeconds.tr,
          textAlign: TextAlign.center,
          style: TextStyles.sp18(
              color: Palette.colorPrimaryTextG90, fontWeight: FontWeight.w600),
        ),
      ),
    ],
  );
}
