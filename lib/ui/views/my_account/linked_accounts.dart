import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:yuvapay/controller/my_account/linked_accounts_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/my_account/account_details_page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/addbank/addbank_no_account.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';

import '../onboarding/upi/addbank/addbank_page.dart';
import 'add_bank.dart';

class LinkedAccountsPage extends AppPageWithAppAndBottomBar {
  static const routeName = "/linked-accounts";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(LinkedAccountsController());

  @override
  double? get toolbarHeight => 0;

  @override
  ConvexAppBar? get bottomNavigationBar => null;

  @override
  PreferredSizeWidget? get appBar => AppBar(
      backgroundColor: Palette.buttonBackground,
      leading: IconButton(
        visualDensity: VisualDensity.compact,
        splashRadius: 24,
        onPressed: Get.back,
        icon: const Icon(Icons.arrow_back_outlined),
      ),
      title:  Text(TranslationKeys.linkedBankAccounts.tr));

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
        child: SingleChildScrollView(
          child: _showContent(),
        ),
      ),
    );
  }

  Widget _showContent() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              itemCount: 2,
              itemBuilder: (BuildContext context, int index) {
                return _showAccountTile(index);
              }),
          AppCard(
            // decoration: _tileDecoration(),
            child: InkWell(
              onTap: () {


                Get.to(AddbankAccountPage());
              },
              child: ListTile(
                // contentPadding:
                //     const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
                title: Text(
                  controller.linkedAccountsHeader[2],
                  style: TextStyles.sp14(
                      fontWeight: FontWeight.w400,
                      color: Palette.colorTextLabel),
                ),
                leading: SizedBox(
                  height: 48,
                  width: 48,
                  child: Image.asset(
                    AssetConstants.addNewBankAccount,
                  ),
                ),
                trailing: const Icon(
                  Icons.chevron_right,
                  color: Palette.greyScaleDark6,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _showAccountTile(int index) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: AppCard(
        // decoration: _tileDecoration(),
        child: InkWell(
          onTap: () {
            Get.toNamed(AccountDetailsPage.routeName);
          },
          child: ListTile(
            // contentPadding:
            //     const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            title: Text(controller.linkedAccountsHeader[index],
                style: TextStyles.sp16(fontWeight: FontWeight.w500)),
            subtitle: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  controller.linkedAccountsSubHeader[index],
                  style: TextStyles.sp14(
                      fontWeight: FontWeight.w400,
                      color: Palette.colorTextLabel),
                ),
                index == 0
                    ? Column(
                        children: [
                          const SizedBox(height: 4),
                          Text(
                           TranslationKeys.defaultAccount.tr,
                            style: TextStyles.sp14(
                                fontWeight: FontWeight.w400,
                                color: Palette.colorTextLabel),
                          ),
                        ],
                      )
                    : const SizedBox(height: 0),
              ],
            ),
            leading: SizedBox(
              height: 48,
              width: 48,
              child: Image.asset(
                '${AssetConstants.linkedBanksImageBase}$index${AssetConstants.pngIconExtension}',
              ),
            ),
            trailing: const Icon(
              Icons.chevron_right,
              color: Palette.greyScaleDark6,
            ),
          ),
        ),
      ),
    );
  }

  BoxDecoration _tileDecoration() {
    return const BoxDecoration(
        color: Palette.colorPageBg,
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }
}
