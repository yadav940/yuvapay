import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:yuvapay/controller/my_account/account_security_controllers.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/my_account/change_pin_page.dart';
import 'package:yuvapay/ui/views/my_account/reset_via_email_phone_page.dart';
import 'package:yuvapay/ui/views/my_account/reset_via_security_question.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';
import '../widgets/popup_utils.dart';

class AccountSecurityPage extends AppPageWithAppBar {
  static const routeName = "/account-security";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(AccountSecurityController());

  @override
  String get title => TranslationKeys.security.tr;

  @override
  Color get pageBackgroundColor => Palette.colorCardBg;

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
        child: _showBody(),
      ),
    );
  }

  Widget _showBody() {
    return AppCard(
      // color: Palette.colorPageBg,

      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
        child: _showOptions(),
      ),
    );
  }

  Widget _showOptions() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const ClampingScrollPhysics(),
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return _showListTile(index);
        });
  }

  Widget _showListTile(int index) {
    return InkWell(
      onTap: () {
        if (index == 1) {
          Get.to(ChangePinPage());
        } else if (index == 2) {
          showAppBottomSheet(
            dragPickColor: Colors.transparent,
            initialChildSize: 0.4,
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: _showWrongPinSheet,
            ),
          );
        }
      },
      child: ListTile(
        contentPadding: const EdgeInsets.all(0),
        leading: SizedBox(
          height: 40,
          width: 40,
          child: Image.asset(
              '${AssetConstants.accountSecurityIconBase}$index${AssetConstants.pngIconExtension}'),
        ),
        title: Text(
          controller.accountSecurityOptions[index],
          style: TextStyles.sp16(fontWeight: FontWeight.w400),
        ),
        subtitle: index == 0
            ? Text(TranslationKeys.recommendedToBeOn.tr,
                style: TextStyles.sp14(
                    fontWeight: FontWeight.w400, color: Palette.colorTextLabel))
            : null,
        trailing: index == 0
            ? Obx(() {
                return _showSecuritySwitch();
              })
            : const Icon(Icons.chevron_right),
      ),
    );
  }

  Widget _showSecuritySwitch() {
    return Switch.adaptive(
        activeColor: Colors.green,
        value: controller.isLockSecurity.value,
        onChanged: (value) {
          controller.isLockSecurity(value);
        });
  }

  final Widget _showWrongPinSheet = Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () {
              Get.back();
            },
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
          Text(
            TranslationKeys.recoverPinUsing.tr,
            style: TextStyles.sp18(fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 0),
        ],
      ),
      const SizedBox(height: 30),
      InkWell(
        onTap: () {
          Get.back();
          Get.to(() => ResetViaSecurityQuestionPage());
        },
        child: Text(
          TranslationKeys.securityQuestions.tr,
          style: TextStyles.sp16(fontWeight: FontWeight.w400),
        ),
      ),
      const SizedBox(height: 20),
      const Divider(thickness: 0.5),
      const SizedBox(height: 20),
      InkWell(
        onTap: () {
          Get.back();
          Get.to(() => ResetViaEmailPhonePage(), arguments: [true]);
        },
        child: Text(
          TranslationKeys.alternatePhoneNumber.tr,
          style: TextStyles.sp16(fontWeight: FontWeight.w400),
        ),
      ),
      const SizedBox(height: 20),
      const Divider(thickness: 0.5),
      const SizedBox(height: 20),
      InkWell(
        onTap: () {
          Get.back();
          Get.to(() => ResetViaEmailPhonePage(), arguments: [false]);
        },
        child: Text(
          TranslationKeys.emailId.tr,
          style: TextStyles.sp16(fontWeight: FontWeight.w400),
        ),
      ),
    ],
  );
}
