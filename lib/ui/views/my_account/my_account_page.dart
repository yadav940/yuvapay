import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:yuvapay/controller/my_account/my_account_controller.dart';
import 'package:yuvapay/ui/resources/asset_%20constants.dart';
import 'package:yuvapay/ui/resources/translations/translations_key.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/usefull/styles/text_styles.dart';
import 'package:yuvapay/ui/views/base/base.dart';
import 'package:yuvapay/ui/views/base/page.dart';
import 'package:yuvapay/ui/views/my_account/help_and_support_page.dart';
import 'package:yuvapay/ui/views/my_account/language_settings_page.dart';
import 'package:yuvapay/ui/views/my_account/linked_accounts.dart';
import 'package:yuvapay/ui/views/my_account/policy_page.dart';
import 'package:yuvapay/ui/views/onboarding/language_page.dart';
import 'package:yuvapay/ui/views/widgets/app_card.dart';

import 'account_security_page.dart';
import 'my_profile_page.dart';

class MyAccountPage extends AppPageWithAppAndBottomBar {
  static const routeName = "/my-account";

  static void start() {
    navigateOffAll(routeName);
  }

  final controller = Get.put(MyAccountController());

  @override
  double? get toolbarHeight => 0;

  @override
  ConvexAppBar? get bottomNavigationBar => null;

  @override
  PreferredSizeWidget? get appBar => AppBar(
      backgroundColor: Palette.buttonBackground,
      leading: IconButton(
        visualDensity: VisualDensity.compact,
        splashRadius: 24,
        onPressed: Get.back,
        icon: const Icon(Icons.arrow_back_outlined),
      ),
      title: Text(TranslationKeys.myAccount.tr));

  @override
  Widget get body {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
        child: SingleChildScrollView(
          child: _showContent(),
        ),
      ),
    );
  }

  Widget _showContent() {
    return SingleChildScrollView(
      child: Column(
        children: [
          InkWell(
              onTap: () {
                Get.to(() => MyProfilePage());
              },
              child: _showProfileTile()),
          const SizedBox(height: 12),
          _showPrimaryAccountOptions(),
          const SizedBox(height: 12),
          _showSecondaryAccountOptions(),
        ],
      ),
    );
  }

  void displayDialog() {
    showDialog(
      context: Get.context!,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text("Log Out?"),
        content: new Text("Are you sure?"),
        actions: [
          CupertinoDialogAction(
              onPressed: () {
                Get.back();
              },
              isDefaultAction: true,
              child: new Text("No")),
          CupertinoDialogAction(isDefaultAction: true, child: new Text("Yes"))
        ],
      ),
    );
  }

  Widget _showSecondaryAccountOptions() {
    return AppCard(
      // decoration: _tileDecoration(),
      child: ListView.builder(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
                onTap: () {
                  if (index == 0) {
                    Get.toNamed(AccountSecurityPage.routeName);
                  }
                  if (index == 1) {
                    Get.to(() => PolicyPage());
                  }
                  if (index == 2) {
                    Get.to(() => HelpAndSupportPage());
                  }
                  if (index == 3) {
                    Get.to(() => LanguageSettingsPage());
                  }
                  if (index == 4) {
                    displayDialog();
                  }
                },
                child: _showAccountExtraTile(index));
          }),
    );
  }

  Widget _showPrimaryAccountOptions() {
    return AppCard(
      // decoration: _tileDecoration(),
      child: ListView.builder(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          itemCount: 1,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
                onTap: () {
                  Get.toNamed(LinkedAccountsPage.routeName);
                },
                child: _showAccountTile(index));
          }),
    );
  }

  Widget _showAccountTile(int index) {
    return ListTile(
      contentPadding: const EdgeInsets.all(0),
      title: Text(controller.myAccountOptions[index],
          style: TextStyles.sp16(fontWeight: FontWeight.w400)),
      leading: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(40)),
          color: Palette.colorCardBg,
        ),
        height: 40,
        width: 40,
        child: Transform.scale(
            scale: 0.5,
            child: SvgPicture.asset(
              '${AssetConstants.myAccountIconBase}$index${AssetConstants.svgIconExtension}',
            )),
      ),
      trailing: const Icon(
        Icons.chevron_right,
        color: Palette.greyScaleDark6,
      ),
    );
  }

  Widget _showAccountExtraTile(int index) {
    return InkWell(
      child: ListTile(
        contentPadding: const EdgeInsets.all(0),
        title: Text(controller.myAccountExtraOptions[index],
            style: TextStyles.sp16(fontWeight: FontWeight.w400)),
        leading: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(40)),
            color: Palette.colorCardBg,
          ),
          height: 40,
          width: 40,
          child: Transform.scale(
              scale: 0.5,
              child: SvgPicture.asset(
                '${AssetConstants.myAccountPartIconBase}$index${AssetConstants.svgIconExtension}',
              )),
        ),
        trailing: const Icon(
          Icons.chevron_right,
          color: Palette.greyScaleDark6,
        ),
      ),
    );
  }

  Widget _showProfileTile() {
    return AppCard(
      // decoration: _tileDecoration(),
      child: _showBottomContent(),
    );
  }

  Widget _showBottomContent() {
    return ListView(
      shrinkWrap: true,
      physics: const ClampingScrollPhysics(),
      children: [
        _showCustomerSection(),
      ],
    );
  }

  Widget _showCustomerSection() {
    return ListTile(
      contentPadding: const EdgeInsets.all(16),
      title: Text(controller.dummyName,
          style: TextStyles.sp18(fontWeight: FontWeight.w500)),
      subtitle: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            controller.dummyPhone,
            style: TextStyles.sp14(
                fontWeight: FontWeight.w400, color: Palette.colorTextLabel),
          ),
          const SizedBox(height: 4),
          Text(
            controller.dummyEmail,
            style: TextStyles.sp14(
                fontWeight: FontWeight.w400, color: Palette.colorTextLabel),
          ),
        ],
      ),
      leading: SizedBox(
        height: 56,
        width: 56,
        child: Image.asset(
          AssetConstants.myAccountProfileImage,
          scale: 0.5,
        ),
      ),
      trailing: const Icon(
        Icons.chevron_right,
        color: Palette.greyScaleDark6,
      ),
    );
  }

  BoxDecoration _tileDecoration() {
    return const BoxDecoration(
        color: Palette.colorPageBg,
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }
}
