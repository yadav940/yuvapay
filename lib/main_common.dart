import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:yuvapay/routes.dart';
import 'package:yuvapay/ui/resources/translations/app_translation.dart';
import 'package:yuvapay/ui/usefull/palette.dart';
import 'package:yuvapay/ui/views/home/home_page.dart';
import 'package:yuvapay/ui/views/onboarding/language_page.dart';
import 'package:yuvapay/ui/views/onboarding/upi/upi_activate.dart';
import 'package:yuvapay/utils/common.dart';

Future<void> mainCommon() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(_getMaterialApp);
}

GetMaterialApp get _getMaterialApp => GetMaterialApp(
      title: 'YuvaPay',
      debugShowCheckedModeBanner: false,
      locale: Get.deviceLocale,
      fallbackLocale: const Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      theme: _themeData,
      getPages: Routes.get(),
      defaultTransition: Transition.rightToLeftWithFade,
      translations: AppTranslations.get(),
      //translationsKeys: ,
      initialRoute: HomePage.routeName,
       //initialRoute: LanguagePage.routeName,
    );

ThemeData get _themeData => ThemeData(
      visualDensity: VisualDensity.adaptivePlatformDensity,
      primarySwatch: Common.createMaterialColor(Palette.colorPrimary),
      canvasColor: Colors.transparent,
      fontFamily: 'Rubik',
      scaffoldBackgroundColor: Colors.white,
      inputDecorationTheme: inputDecorationTheme,
      iconTheme: IconThemeData(color: Palette.psMain),
      textTheme: textTheme,
    );

InputDecorationTheme get inputDecorationTheme => InputDecorationTheme(
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Palette.greyScaleLight0.withOpacity(1),
          width: 1,
        ),
      ),
      border: const UnderlineInputBorder(
        borderSide: BorderSide(color: Palette.colorBorders, width: 1),
      ),
      errorBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red, width: 1),
      ),
      focusedErrorBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red, width: 1),
      ),
      errorStyle: const TextStyle(
        color: Palette.colorError,
        fontSize: 11,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
        letterSpacing: 0.8,
      ),
      disabledBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: Palette.colorBorders, width: 1),
      ),
      counterStyle: TextStyle(
        height: 1,
        fontSize: 10,
        color: Palette.greyScaleDark2.withOpacity(0.5),
      ),
    );

/// [caption] for form label,
/// [bodyText1] for form text, i.e. inputText, dropdown text etc,
TextTheme get textTheme => const TextTheme(
      // Used for Form Labels
      caption: TextStyle(
        height: 1.1,
        fontSize: 10,
        letterSpacing: 0.5,
        fontWeight: FontWeight.w600,
        color: Palette.greyScaleDark3,
      ),

      // Used for emphasizing text which would otherwise be bodyText2
      bodyText1: TextStyle(
        fontSize: 14,
        height: 17 / 14,
        color: Palette.greyScaleDark0,
        fontWeight: FontWeight.bold,
      ),

      // General Default Text (Used for Form Answers)
      bodyText2: TextStyle(
        fontSize: 14,
        height: 17 / 14,
        color: Palette.greyScaleDark0,
        fontWeight: FontWeight.w400,
      ),
    );
