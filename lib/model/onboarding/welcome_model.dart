
import 'package:yuvapay/ui/views/base/base.dart';

class WelcomeModel{
  String asset;
  Color assetBackgroundColor;
  String title;
  String bodyMessage;

  WelcomeModel({required this.asset, required this.assetBackgroundColor, required this.title,required  this.bodyMessage});
  
}