class PermissionModel {
  String asset;
  String title;
  String bodyMessage;

  PermissionModel(
      {required this.asset, required this.title, required this.bodyMessage});
}
