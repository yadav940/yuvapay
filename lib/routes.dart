import 'package:get/get.dart';
import 'package:yuvapay/routes/initial_routes.dart';
import 'package:yuvapay/routes/neo_bank_routes.dart';
import 'package:yuvapay/routes/wallete_routes.dart';

import 'routes/upi_routes.dart';

class Routes {
  Routes._();

  static List<GetPage> get() {
    final moduleRoutes = <GetPage>[];
    moduleRoutes.addAll(InitialRoutes.routes);
    moduleRoutes.addAll(NeoBankRoutes.routes);
    moduleRoutes.addAll(UpiRoutes.routes);
    moduleRoutes.addAll(WalleteRoutes.routes);
    return moduleRoutes;
  }
}
